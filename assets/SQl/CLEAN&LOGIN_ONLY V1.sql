-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 20, 2014 at 08:43 AM
-- Server version: 5.5.27
-- PHP Version: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `kwitansi`
--
CREATE DATABASE `kwitansi` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `kwitansi`;

-- --------------------------------------------------------

--
-- Table structure for table `barang`
--

CREATE TABLE IF NOT EXISTS `barang` (
  `kd_barang` varchar(10) NOT NULL,
  `nm_barang` varchar(50) NOT NULL,
  `harga` int(11) NOT NULL,
  PRIMARY KEY (`kd_barang`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `biaya`
--

CREATE TABLE IF NOT EXISTS `biaya` (
  `kd_biaya` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `jenis_biaya` varchar(15) NOT NULL,
  `kd_pelajaran` varchar(3) DEFAULT NULL,
  `kd_program` varchar(3) DEFAULT NULL,
  `kd_tingkat` varchar(5) DEFAULT NULL,
  `kd_jenis` varchar(10) DEFAULT NULL,
  `nm_biaya` varchar(50) NOT NULL,
  `biaya` int(11) NOT NULL,
  PRIMARY KEY (`kd_biaya`),
  UNIQUE KEY `jenis_biaya` (`jenis_biaya`,`kd_pelajaran`,`kd_program`,`kd_tingkat`,`kd_jenis`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `diskon`
--

CREATE TABLE IF NOT EXISTS `diskon` (
  `kd_diskon` varchar(2) NOT NULL,
  `nm_diskon` varchar(35) NOT NULL,
  `persen` float NOT NULL,
  `ket_diskon` varchar(200) NOT NULL,
  PRIMARY KEY (`kd_diskon`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `diskon_murid`
--

CREATE TABLE IF NOT EXISTS `diskon_murid` (
  `nim` varchar(10) NOT NULL,
  `kd_diskon` varchar(2) NOT NULL,
  PRIMARY KEY (`nim`,`kd_diskon`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jenis`
--

CREATE TABLE IF NOT EXISTS `jenis` (
  `kd_jenis` varchar(3) NOT NULL,
  `nm_jenis` varchar(35) NOT NULL,
  PRIMARY KEY (`kd_jenis`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE IF NOT EXISTS `login` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(15) NOT NULL,
  `pass` varchar(40) NOT NULL,
  `level` varchar(5) NOT NULL,
  `stat` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`id`, `name`, `pass`, `level`, `stat`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'admin', 0),
(2, 'user', 'ee11cbb19052e40b07aac0ca060c23ee', 'user', 0);

-- --------------------------------------------------------

--
-- Table structure for table `m_kwitansibrg`
--

CREATE TABLE IF NOT EXISTS `m_kwitansibrg` (
  `no_kwitansi` varchar(11) NOT NULL DEFAULT '',
  `tgl_kwitansi` date NOT NULL,
  `nim` varchar(10) DEFAULT NULL,
  `nama` varchar(35) NOT NULL,
  `total` int(11) NOT NULL,
  `keterangan` varchar(50) NOT NULL,
  PRIMARY KEY (`no_kwitansi`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `m_kwitansipljr`
--

CREATE TABLE IF NOT EXISTS `m_kwitansipljr` (
  `no_kwitansi` varchar(11) NOT NULL,
  `tgl_kwitansi` date NOT NULL,
  `nim` varchar(10) NOT NULL,
  `subtotal` int(11) NOT NULL,
  `total` int(11) NOT NULL,
  `keterangan` varchar(50) NOT NULL,
  `nilai_diskon` int(11) NOT NULL,
  PRIMARY KEY (`no_kwitansi`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `murid`
--

CREATE TABLE IF NOT EXISTS `murid` (
  `nim` varchar(10) NOT NULL,
  `nm_murid` varchar(50) NOT NULL,
  `ket` varchar(100) NOT NULL,
  PRIMARY KEY (`nim`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `p_kwitansibrg`
--

CREATE TABLE IF NOT EXISTS `p_kwitansibrg` (
  `no_kwitansi` varchar(11) NOT NULL,
  `tgl_kwitansi` date NOT NULL,
  `kd_barang` varchar(10) NOT NULL,
  `harga` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL,
  PRIMARY KEY (`no_kwitansi`,`kd_barang`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `p_kwitansipljr`
--

CREATE TABLE IF NOT EXISTS `p_kwitansipljr` (
  `no_kwitansi` varchar(11) NOT NULL,
  `tgl_kwitansi` date NOT NULL,
  `kd_biaya` int(10) unsigned NOT NULL,
  `bulan` varchar(25) NOT NULL,
  `jlh_bln` int(11) NOT NULL,
  `tahun` year(4) NOT NULL,
  `biaya` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pelajaran`
--

CREATE TABLE IF NOT EXISTS `pelajaran` (
  `kd_pelajaran` varchar(3) NOT NULL,
  `nm_pelajaran` varchar(35) NOT NULL,
  `kd_jenis` varchar(10) NOT NULL,
  `kd_tingkat` varchar(10) NOT NULL,
  `kd_program` varchar(10) NOT NULL,
  `kd_subprogram` varchar(10) NOT NULL,
  `ket` varchar(100) NOT NULL,
  PRIMARY KEY (`kd_pelajaran`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `program`
--

CREATE TABLE IF NOT EXISTS `program` (
  `kd_program` varchar(3) NOT NULL,
  `nm_program` varchar(35) NOT NULL,
  PRIMARY KEY (`kd_program`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `subprogram`
--

CREATE TABLE IF NOT EXISTS `subprogram` (
  `kd_subprogram` varchar(3) NOT NULL,
  `nm_subprogram` varchar(35) NOT NULL,
  PRIMARY KEY (`kd_subprogram`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tingkat`
--

CREATE TABLE IF NOT EXISTS `tingkat` (
  `kd_tingkat` varchar(5) NOT NULL,
  `nm_tingkat` varchar(35) NOT NULL,
  PRIMARY KEY (`kd_tingkat`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
