-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 20, 2014 at 08:39 AM
-- Server version: 5.5.27
-- PHP Version: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `kwitansi`
--
CREATE DATABASE `kwitansi` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `kwitansi`;

-- --------------------------------------------------------

--
-- Table structure for table `barang`
--

CREATE TABLE IF NOT EXISTS `barang` (
  `kd_barang` varchar(10) NOT NULL,
  `nm_barang` varchar(50) NOT NULL,
  `harga` int(11) NOT NULL,
  PRIMARY KEY (`kd_barang`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `barang`
--

INSERT INTO `barang` (`kd_barang`, `nm_barang`, `harga`) VALUES
('002', 'barang1', 21000),
('441', 'barang 2', 62500),
('ABC123', 'barang 3', 1500);

-- --------------------------------------------------------

--
-- Table structure for table `biaya`
--

CREATE TABLE IF NOT EXISTS `biaya` (
  `kd_biaya` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `jenis_biaya` varchar(15) NOT NULL,
  `kd_pelajaran` varchar(3) DEFAULT NULL,
  `kd_program` varchar(3) DEFAULT NULL,
  `kd_tingkat` varchar(5) DEFAULT NULL,
  `kd_jenis` varchar(10) DEFAULT NULL,
  `nm_biaya` varchar(50) NOT NULL,
  `biaya` int(11) NOT NULL,
  PRIMARY KEY (`kd_biaya`),
  UNIQUE KEY `jenis_biaya` (`jenis_biaya`,`kd_pelajaran`,`kd_program`,`kd_tingkat`,`kd_jenis`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ;

--
-- Dumping data for table `biaya`
--

INSERT INTO `biaya` (`kd_biaya`, `jenis_biaya`, `kd_pelajaran`, `kd_program`, `kd_tingkat`, `kd_jenis`, `nm_biaya`, `biaya`) VALUES
(1, 'USS', 'P', 'AK', 'PD', 'P', 'US Piano', 210000),
(2, 'USK', 'P', 'AK', 'PD', 'P', 'USK Piano', 200000),
(3, 'Sem1', 'P', 'AK', 'PD', 'P', 'UU Piano Semester 1', 50000),
(4, 'Sem2', 'P', 'AK', 'PD', 'P', 'UU Piano Semester 2', 60000),
(5, 'USS', 'P2', 'AK', 'LA 2', 'P', 'US Piano', 358000),
(6, 'USK', 'P2', 'AK', 'LA 2', 'P', 'USK Piano', 400000),
(7, 'Sem1', 'P2', 'AK', 'LA 2', 'P', 'UU Piano Semester 1', 36000),
(8, 'Sem2', 'P2', 'AK', 'LA 2', 'P', 'UU Piano Semester 2', 40000),
(9, 'USS', 'B', 'AK', 'LA 2', 'B', 'US Biola', 220000),
(10, 'USK', 'B', 'AK', 'LA 2', 'B', 'USK Biola', 250000),
(11, 'Sem1', 'B', 'AK', 'LA 2', 'B', 'UU Biola Semester 1', 50000),
(12, 'Sem2', 'B', 'AK', 'LA 2', 'B', 'UU Biola Semester 2', 60000),
(13, 'USS', 'M', 'AV', 'LA 2', 'M', 'US Melukis', 120000),
(14, 'USK', 'M', 'AV', 'LA 2', 'M', 'USK Melukis', 150000),
(15, 'Sem1', 'M', 'AV', 'LA 2', 'M', 'UU Melukis Semester 1', 30000),
(16, 'Sem2', 'M', 'AV', 'LA 2', 'M', 'UU Melukis Semester 2', 35000),
(17, 'test', NULL, NULL, NULL, 'P', 'Uang Testing Piano', 600000),
(18, 'test', NULL, NULL, NULL, 'M', 'Uang Testing Melukis', 75000),
(19, 'adm', NULL, 'NA', NULL, NULL, 'Uang Adm 2014', 350000),
(20, 'daftar', NULL, 'NA', NULL, NULL, 'Pendaftaran 2014', 350000);

-- --------------------------------------------------------

--
-- Table structure for table `diskon`
--

CREATE TABLE IF NOT EXISTS `diskon` (
  `kd_diskon` varchar(2) NOT NULL,
  `nm_diskon` varchar(35) NOT NULL,
  `persen` float NOT NULL,
  `ket_diskon` varchar(200) NOT NULL,
  PRIMARY KEY (`kd_diskon`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `diskon`
--

INSERT INTO `diskon` (`kd_diskon`, `nm_diskon`, `persen`, `ket_diskon`) VALUES
('56', 'Promosi Pembukaan', 0.45, 'Diskon Promosi Grand Opening tgl xx-xx-xxxx sampai tgl xx-xx-xxxx'),
('DP', 'Diskon Prestasi', 0.15, 'Diskon Prestasi'),
('DS', 'Diskon Saudara', 0.4, 'Diskon Murid SC yang memiliki saudara di SC');

-- --------------------------------------------------------

--
-- Table structure for table `diskon_murid`
--

CREATE TABLE IF NOT EXISTS `diskon_murid` (
  `nim` varchar(10) NOT NULL,
  `kd_diskon` varchar(2) NOT NULL,
  PRIMARY KEY (`nim`,`kd_diskon`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `diskon_murid`
--

INSERT INTO `diskon_murid` (`nim`, `kd_diskon`) VALUES
('1123', 'DP'),
('123', '56'),
('421', 'DP'),
('421', 'DS');

-- --------------------------------------------------------

--
-- Table structure for table `jenis`
--

CREATE TABLE IF NOT EXISTS `jenis` (
  `kd_jenis` varchar(3) NOT NULL,
  `nm_jenis` varchar(35) NOT NULL,
  PRIMARY KEY (`kd_jenis`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jenis`
--

INSERT INTO `jenis` (`kd_jenis`, `nm_jenis`) VALUES
('B', 'Biola'),
('G', 'Gitar'),
('M', 'Melukis'),
('P', 'Piano'),
('SF', 'Solfeggio'),
('V', 'Vokal');

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE IF NOT EXISTS `login` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(15) NOT NULL,
  `pass` varchar(40) NOT NULL,
  `level` varchar(5) NOT NULL,
  `stat` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`id`, `name`, `pass`, `level`, `stat`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'admin', 0),
(2, 'user', 'ee11cbb19052e40b07aac0ca060c23ee', 'user', 0);

-- --------------------------------------------------------

--
-- Table structure for table `m_kwitansibrg`
--

CREATE TABLE IF NOT EXISTS `m_kwitansibrg` (
  `no_kwitansi` varchar(11) NOT NULL DEFAULT '',
  `tgl_kwitansi` date NOT NULL,
  `nim` varchar(10) DEFAULT NULL,
  `nama` varchar(35) NOT NULL,
  `total` int(11) NOT NULL,
  `keterangan` varchar(50) NOT NULL,
  PRIMARY KEY (`no_kwitansi`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_kwitansibrg`
--

INSERT INTO `m_kwitansibrg` (`no_kwitansi`, `tgl_kwitansi`, `nim`, `nama`, `total`, `keterangan`) VALUES
('B00001/2014', '2014-04-30', '7676', 'wawewoiwoi', 130500, ''),
('B00002/2014', '2014-04-30', NULL, 'Jimmy', 188000, ''),
('B00003/2014', '2014-04-30', '7676', 'wawewoiwoi', 167000, '');

-- --------------------------------------------------------

--
-- Table structure for table `m_kwitansipljr`
--

CREATE TABLE IF NOT EXISTS `m_kwitansipljr` (
  `no_kwitansi` varchar(11) NOT NULL,
  `tgl_kwitansi` date NOT NULL,
  `nim` varchar(10) NOT NULL,
  `subtotal` int(11) NOT NULL,
  `total` int(11) NOT NULL,
  `keterangan` varchar(50) NOT NULL,
  `nilai_diskon` int(11) NOT NULL,
  PRIMARY KEY (`no_kwitansi`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_kwitansipljr`
--

INSERT INTO `m_kwitansipljr` (`no_kwitansi`, `tgl_kwitansi`, `nim`, `subtotal`, `total`, `keterangan`, `nilai_diskon`) VALUES
('U00001/2014', '2014-05-17', '421', 890000, 400500, '', 489500),
('U00002/2014', '2014-05-19', '7676', 400000, 400000, '', 0),
('U00003/2014', '2014-05-19', '421', 358000, 161100, '', 196900),
('U00004/2014', '2014-05-19', '1123', 200000, 170000, '', 30000),
('U00005/2014', '2014-05-19', '7676', 210000, 210000, '', 0),
('U00006/2014', '2014-05-19', '7676', 120000, 120000, '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `murid`
--

CREATE TABLE IF NOT EXISTS `murid` (
  `nim` varchar(10) NOT NULL,
  `nm_murid` varchar(50) NOT NULL,
  `ket` varchar(100) NOT NULL,
  PRIMARY KEY (`nim`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `murid`
--

INSERT INTO `murid` (`nim`, `nm_murid`, `ket`) VALUES
('1123', 'Andrea Tanu Wijaya', 'Pianist? nah. dunno ''bout dat'),
('421', 'Silvia Yang', 'Namanya Astaga. Orangnya Astaga. Prestasinya, Wow astaga'),
('7676', 'wawewoiwoi', 'anak jelek');

-- --------------------------------------------------------

--
-- Table structure for table `p_kwitansibrg`
--

CREATE TABLE IF NOT EXISTS `p_kwitansibrg` (
  `no_kwitansi` varchar(11) NOT NULL,
  `tgl_kwitansi` date NOT NULL,
  `kd_barang` varchar(10) NOT NULL,
  `harga` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL,
  PRIMARY KEY (`no_kwitansi`,`kd_barang`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `p_kwitansibrg`
--

INSERT INTO `p_kwitansibrg` (`no_kwitansi`, `tgl_kwitansi`, `kd_barang`, `harga`, `qty`, `jumlah`) VALUES
('B00001/2014', '2014-04-30', '002', 21000, 6, 126000),
('B00001/2014', '2014-04-30', 'ABC123', 1500, 3, 4500),
('B00002/2014', '2014-04-30', '002', 21000, 3, 63000),
('B00002/2014', '2014-04-30', '441', 62500, 2, 125000),
('B00003/2014', '2014-04-30', '002', 21000, 2, 42000),
('B00003/2014', '2014-04-30', '441', 62500, 2, 125000);

-- --------------------------------------------------------

--
-- Table structure for table `p_kwitansipljr`
--

CREATE TABLE IF NOT EXISTS `p_kwitansipljr` (
  `no_kwitansi` varchar(11) NOT NULL,
  `tgl_kwitansi` date NOT NULL,
  `kd_biaya` int(10) unsigned NOT NULL,
  `bulan` varchar(25) NOT NULL,
  `jlh_bln` int(11) NOT NULL,
  `tahun` year(4) NOT NULL,
  `biaya` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `p_kwitansipljr`
--

INSERT INTO `p_kwitansipljr` (`no_kwitansi`, `tgl_kwitansi`, `kd_biaya`, `bulan`, `jlh_bln`, `tahun`, `biaya`, `jumlah`) VALUES
('U00001/2014', '2014-05-17', 1, '1,2,3,4', 4, 2014, 210000, 840000),
('U00001/2014', '2014-05-17', 3, '', 1, 0000, 50000, 50000),
('U00002/2014', '2014-05-19', 2, '1,2', 2, 2014, 200000, 400000),
('U00003/2014', '2014-05-19', 5, '1', 1, 2014, 358000, 358000),
('U00004/2014', '2014-05-19', 9, '1', 1, 2014, 200000, 200000),
('U00005/2014', '2014-05-19', 1, '1', 1, 2014, 210000, 210000),
('U00006/2014', '2014-05-19', 13, '1', 1, 2014, 120000, 120000);

-- --------------------------------------------------------

--
-- Table structure for table `pelajaran`
--

CREATE TABLE IF NOT EXISTS `pelajaran` (
  `kd_pelajaran` varchar(3) NOT NULL,
  `nm_pelajaran` varchar(35) NOT NULL,
  `kd_jenis` varchar(10) NOT NULL,
  `kd_tingkat` varchar(10) NOT NULL,
  `kd_program` varchar(10) NOT NULL,
  `kd_subprogram` varchar(10) NOT NULL,
  `ket` varchar(100) NOT NULL,
  PRIMARY KEY (`kd_pelajaran`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pelajaran`
--

INSERT INTO `pelajaran` (`kd_pelajaran`, `nm_pelajaran`, `kd_jenis`, `kd_tingkat`, `kd_program`, `kd_subprogram`, `ket`) VALUES
('B', 'Biola', 'B', 'LA 2', 'AK', 'CL', '0'),
('M', 'Melukis', 'M', 'LA 2', 'AV', 'AV', '0'),
('P', 'Piano', 'P', 'PD', 'AK', 'CL', '0'),
('P2', 'Piano', 'P', 'LA 2', 'AK', 'CL', '0');

-- --------------------------------------------------------

--
-- Table structure for table `program`
--

CREATE TABLE IF NOT EXISTS `program` (
  `kd_program` varchar(3) NOT NULL,
  `nm_program` varchar(35) NOT NULL,
  PRIMARY KEY (`kd_program`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `program`
--

INSERT INTO `program` (`kd_program`, `nm_program`) VALUES
('AB', 'ABRSM'),
('AK', 'Academic'),
('AV', 'Avocation'),
('NA', 'Non-academic');

-- --------------------------------------------------------

--
-- Table structure for table `subprogram`
--

CREATE TABLE IF NOT EXISTS `subprogram` (
  `kd_subprogram` varchar(3) NOT NULL,
  `nm_subprogram` varchar(35) NOT NULL,
  PRIMARY KEY (`kd_subprogram`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subprogram`
--

INSERT INTO `subprogram` (`kd_subprogram`, `nm_subprogram`) VALUES
('ALT', 'Alternative'),
('AV', 'Avocation'),
('CL', 'Classica'),
('SM', 'Suzuki Method');

-- --------------------------------------------------------

--
-- Table structure for table `tingkat`
--

CREATE TABLE IF NOT EXISTS `tingkat` (
  `kd_tingkat` varchar(5) NOT NULL,
  `nm_tingkat` varchar(35) NOT NULL,
  PRIMARY KEY (`kd_tingkat`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tingkat`
--

INSERT INTO `tingkat` (`kd_tingkat`, `nm_tingkat`) VALUES
('LA 2', 'Lanjutan 2'),
('PD', 'Piano Dasar');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
