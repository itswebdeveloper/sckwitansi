-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 26, 2014 at 08:56 AM
-- Server version: 5.5.27
-- PHP Version: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `kwitansi`
--
CREATE DATABASE `kwitansi` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `kwitansi`;

-- --------------------------------------------------------

--
-- Table structure for table `barang`
--

CREATE TABLE IF NOT EXISTS `barang` (
  `kd_barang` varchar(10) NOT NULL,
  `nm_barang` varchar(50) NOT NULL,
  `harga` int(11) NOT NULL,
  PRIMARY KEY (`kd_barang`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `barang`
--

INSERT INTO `barang` (`kd_barang`, `nm_barang`, `harga`) VALUES
('002', 'barang1', 21000),
('441', 'barang 2', 62500),
('ABC123', 'barang 3', 1500);

-- --------------------------------------------------------

--
-- Table structure for table `biaya`
--

CREATE TABLE IF NOT EXISTS `biaya` (
  `kd_biaya` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `jenis_biaya` varchar(15) NOT NULL,
  `kd_pelajaran` varchar(15) DEFAULT NULL,
  `kd_program` varchar(3) DEFAULT NULL,
  `kd_tingkat` varchar(5) DEFAULT NULL,
  `kd_jenis` varchar(10) DEFAULT NULL,
  `nm_biaya` varchar(50) NOT NULL,
  `biaya` int(11) NOT NULL,
  PRIMARY KEY (`kd_biaya`),
  UNIQUE KEY `jenis_biaya` (`jenis_biaya`,`kd_pelajaran`,`kd_program`,`kd_tingkat`,`kd_jenis`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=214 ;

--
-- Dumping data for table `biaya`
--

INSERT INTO `biaya` (`kd_biaya`, `jenis_biaya`, `kd_pelajaran`, `kd_program`, `kd_tingkat`, `kd_jenis`, `nm_biaya`, `biaya`) VALUES
(9, 'USS', 'AK-CL-SF-PD1', 'AK', 'PD1', 'SF', 'US Solfeggio', 250000),
(10, 'USK', 'AK-CL-SF-PD1', 'AK', 'PD1', 'SF', 'USK Solfeggio', 270000),
(11, 'Sem1', 'AK-CL-SF-PD1', 'AK', 'PD1', 'SF', 'UU Solfeggio Semester 1', 0),
(12, 'Sem2', 'AK-CL-SF-PD1', 'AK', 'PD1', 'SF', 'UU Solfeggio Semester 1', 0),
(13, 'USS', 'AK-CL-SF-PD2', 'AK', 'PD2', 'SF', 'US Solfeggio', 260000),
(14, 'USK', 'AK-CL-SF-PD2', 'AK', 'PD2', 'SF', 'USK Solfeggio', 280000),
(15, 'Sem1', 'AK-CL-SF-PD2', 'AK', 'PD2', 'SF', 'UU Solfeggio Semester 1', 0),
(16, 'Sem2', 'AK-CL-SF-PD2', 'AK', 'PD2', 'SF', 'UU Solfeggio Semester 1', 150000),
(17, 'USS', 'AK-CL-SF-DS1', 'AK', 'DS1', 'SF', 'US Solfeggio', 280000),
(18, 'USK', 'AK-CL-SF-DS1', 'AK', 'DS1', 'SF', 'USK Solfeggio', 300000),
(19, 'Sem1', 'AK-CL-SF-DS1', 'AK', 'DS1', 'SF', 'UU Solfeggio Semester 1', 170000),
(20, 'Sem2', 'AK-CL-SF-DS1', 'AK', 'DS1', 'SF', 'UU Solfeggio Semester 1', 170000),
(21, 'USS', 'AK-CL-SF-DS2', 'AK', 'DS2', 'SF', 'US Solfeggio', 310000),
(22, 'USK', 'AK-CL-SF-DS2', 'AK', 'DS2', 'SF', 'USK Solfeggio', 330000),
(23, 'Sem1', 'AK-CL-SF-DS2', 'AK', 'DS2', 'SF', 'UU Solfeggio Semester 1', 180000),
(24, 'Sem2', 'AK-CL-SF-DS2', 'AK', 'DS2', 'SF', 'UU Solfeggio Semester 1', 190000),
(25, 'USS', 'AK-CL-SF-MN1', 'AK', 'MN1', 'SF', 'US Solfeggio', 320000),
(26, 'USK', 'AK-CL-SF-MN1', 'AK', 'MN1', 'SF', 'USK Solfeggio', 340000),
(27, 'Sem1', 'AK-CL-SF-MN1', 'AK', 'MN1', 'SF', 'UU Solfeggio Semester 1', 190000),
(28, 'Sem2', 'AK-CL-SF-MN1', 'AK', 'MN1', 'SF', 'UU Solfeggio Semester 1', 190000),
(29, 'USS', 'AK-CL-SF-MN2', 'AK', 'MN2', 'SF', 'US Solfeggio', 330000),
(30, 'USK', 'AK-CL-SF-MN2', 'AK', 'MN2', 'SF', 'USK Solfeggio', 350000),
(31, 'Sem1', 'AK-CL-SF-MN2', 'AK', 'MN2', 'SF', 'UU Solfeggio Semester 1', 200000),
(32, 'Sem2', 'AK-CL-SF-MN2', 'AK', 'MN2', 'SF', 'UU Solfeggio Semester 1', 200000),
(33, 'USS', 'AK-CL-SF-MN3', 'AK', 'MN3', 'SF', 'US Solfeggio', 360000),
(34, 'USK', 'AK-CL-SF-MN3', 'AK', 'MN3', 'SF', 'USK Solfeggio', 380000),
(35, 'Sem1', 'AK-CL-SF-MN3', 'AK', 'MN3', 'SF', 'UU Solfeggio Semester 1', 220000),
(36, 'Sem2', 'AK-CL-SF-MN3', 'AK', 'MN3', 'SF', 'UU Solfeggio Semester 1', 230000),
(37, 'USS', 'AK-CL-SF-LA1', 'AK', 'LA1', 'SF', 'US Solfeggio', 370000),
(38, 'USK', 'AK-CL-SF-LA1', 'AK', 'LA1', 'SF', 'USK Solfeggio', 390000),
(39, 'Sem1', 'AK-CL-SF-LA1', 'AK', 'LA1', 'SF', 'UU Solfeggio Semester 1', 240000),
(40, 'Sem2', 'AK-CL-SF-LA1', 'AK', 'LA1', 'SF', 'UU Solfeggio Semester 1', 240000),
(41, 'USS', 'AK-CL-SF-LA2', 'AK', 'LA2', 'SF', 'US Solfeggio', 410000),
(42, 'USK', 'AK-CL-SF-LA2', 'AK', 'LA2', 'SF', 'USK Solfeggio', 430000),
(43, 'Sem1', 'AK-CL-SF-LA2', 'AK', 'LA2', 'SF', 'UU Solfeggio Semester 1', 270000),
(44, 'Sem2', 'AK-CL-SF-LA2', 'AK', 'LA2', 'SF', 'UU Solfeggio Semester 1', 300000),
(45, 'USS', 'AK-CL-HR-HR0', 'AK', 'HR0', 'HR', 'US Harmoni', 350000),
(46, 'USK', 'AK-CL-HR-HR0', 'AK', 'HR0', 'HR', 'USK Harmoni', 370000),
(47, 'Sem1', 'AK-CL-HR-HR0', 'AK', 'HR0', 'HR', 'UU Harmoni Semester 1', 250000),
(48, 'Sem2', 'AK-CL-HR-HR0', 'AK', 'HR0', 'HR', 'UU Harmoni Semester 1', 250000),
(49, 'USS', 'AK-CL-SJ-SJ0', 'AK', 'SJ0', 'SJ', 'US Sejarah', 350000),
(50, 'USK', 'AK-CL-SJ-SJ0', 'AK', 'SJ0', 'SJ', 'USK Sejarah', 370000),
(51, 'Sem1', 'AK-CL-SJ-SJ0', 'AK', 'SJ0', 'SJ', 'UU Sejarah Semester 1', 260000),
(52, 'Sem2', 'AK-CL-SJ-SJ0', 'AK', 'SJ0', 'SJ', 'UU Sejarah Semester 1', 260000),
(53, 'USS', 'AK-CL-AM-AM0', 'AK', 'AM0', 'AM', 'US Analisa Musik', 350000),
(54, 'USK', 'AK-CL-AM-AM0', 'AK', 'AM0', 'AM', 'USK Analisa Musik', 370000),
(55, 'Sem1', 'AK-CL-AM-AM0', 'AK', 'AM0', 'AM', 'UU Analisa Musik Semester 1', 280000),
(56, 'Sem2', 'AK-CL-AM-AM0', 'AK', 'AM0', 'AM', 'UU Analisa Musik Semester 1', 280000),
(57, 'USS', 'AK-CL-PN-PD0', 'AK', 'PD0', 'PN', 'US Piano', 350000),
(58, 'USK', 'AK-CL-PN-PD0', 'AK', 'PD0', 'PN', 'USK Piano', 370000),
(59, 'Sem1', 'AK-CL-PN-PD0', 'AK', 'PD0', 'PN', 'UU Piano Semester 1', 150000),
(60, 'Sem2', 'AK-CL-PN-PD0', 'AK', 'PD0', 'PN', 'UU Piano Semester 1', 150000),
(61, 'USS', 'AK-CL-PN-DS1', 'AK', 'DS1', 'PN', 'US Piano', 370000),
(62, 'USK', 'AK-CL-PN-DS1', 'AK', 'DS1', 'PN', 'USK Piano', 390000),
(63, 'Sem1', 'AK-CL-PN-DS1', 'AK', 'DS1', 'PN', 'UU Piano Semester 1', 160000),
(64, 'Sem2', 'AK-CL-PN-DS1', 'AK', 'DS1', 'PN', 'UU Piano Semester 1', 160000),
(65, 'USS', 'AK-CL-PN-DS2', 'AK', 'DS2', 'PN', 'US Piano', 400000),
(66, 'USK', 'AK-CL-PN-DS2', 'AK', 'DS2', 'PN', 'USK Piano', 420000),
(67, 'Sem1', 'AK-CL-PN-DS2', 'AK', 'DS2', 'PN', 'UU Piano Semester 1', 170000),
(68, 'Sem2', 'AK-CL-PN-DS2', 'AK', 'DS2', 'PN', 'UU Piano Semester 1', 170000),
(69, 'USS', 'AK-CL-PN-DS3', 'AK', 'DS3', 'PN', 'US Piano', 420000),
(70, 'USK', 'AK-CL-PN-DS3', 'AK', 'DS3', 'PN', 'USK Piano', 440000),
(71, 'Sem1', 'AK-CL-PN-DS3', 'AK', 'DS3', 'PN', 'UU Piano Semester 1', 190000),
(72, 'Sem2', 'AK-CL-PN-DS3', 'AK', 'DS3', 'PN', 'UU Piano Semester 1', 190000),
(73, 'USS', 'AK-CL-PN-DS4', 'AK', 'DS4', 'PN', 'US Piano', 440000),
(74, 'USK', 'AK-CL-PN-DS4', 'AK', 'DS4', 'PN', 'USK Piano', 460000),
(75, 'Sem1', 'AK-CL-PN-DS4', 'AK', 'DS4', 'PN', 'UU Piano Semester 1', 210000),
(76, 'Sem2', 'AK-CL-PN-DS4', 'AK', 'DS4', 'PN', 'UU Piano Semester 1', 210000),
(77, 'USS', 'AK-CL-PN-DS5', 'AK', 'DS5', 'PN', 'US Piano', 510000),
(78, 'USK', 'AK-CL-PN-DS5', 'AK', 'DS5', 'PN', 'USK Piano', 530000),
(79, 'Sem1', 'AK-CL-PN-DS5', 'AK', 'DS5', 'PN', 'UU Piano Semester 1', 230000),
(80, 'Sem2', 'AK-CL-PN-DS5', 'AK', 'DS5', 'PN', 'UU Piano Semester 1', 280000),
(81, 'USS', 'AK-CL-PN-MN1', 'AK', 'MN1', 'PN', 'US Piano', 530000),
(82, 'USK', 'AK-CL-PN-MN1', 'AK', 'MN1', 'PN', 'USK Piano', 550000),
(83, 'Sem1', 'AK-CL-PN-MN1', 'AK', 'MN1', 'PN', 'UU Piano Semester 1', 240000),
(84, 'Sem2', 'AK-CL-PN-MN1', 'AK', 'MN1', 'PN', 'UU Piano Semester 1', 250000),
(85, 'USS', 'AK-CL-PN-MN2', 'AK', 'MN2', 'PN', 'US Piano', 560000),
(86, 'USK', 'AK-CL-PN-MN2', 'AK', 'MN2', 'PN', 'USK Piano', 580000),
(87, 'Sem1', 'AK-CL-PN-MN2', 'AK', 'MN2', 'PN', 'UU Piano Semester 1', 260000),
(88, 'Sem2', 'AK-CL-PN-MN2', 'AK', 'MN2', 'PN', 'UU Piano Semester 1', 270000),
(89, 'USS', 'AK-CL-PN-MN3', 'AK', 'MN3', 'PN', 'US Piano', 650000),
(90, 'USK', 'AK-CL-PN-MN3', 'AK', 'MN3', 'PN', 'USK Piano', 670000),
(91, 'Sem1', 'AK-CL-PN-MN3', 'AK', 'MN3', 'PN', 'UU Piano Semester 1', 300000),
(92, 'Sem2', 'AK-CL-PN-MN3', 'AK', 'MN3', 'PN', 'UU Piano Semester 1', 350000),
(93, 'USS', 'AK-CL-PN-LA1', 'AK', 'LA1', 'PN', 'US Piano', 700000),
(94, 'USK', 'AK-CL-PN-LA1', 'AK', 'LA1', 'PN', 'USK Piano', 720000),
(95, 'Sem1', 'AK-CL-PN-LA1', 'AK', 'LA1', 'PN', 'UU Piano Semester 1', 310000),
(96, 'Sem2', 'AK-CL-PN-LA1', 'AK', 'LA1', 'PN', 'UU Piano Semester 1', 330000),
(97, 'USS', 'AK-CL-PN-LA2', 'AK', 'LA2', 'PN', 'US Piano', 800000),
(98, 'USK', 'AK-CL-PN-LA2', 'AK', 'LA2', 'PN', 'USK Piano', 820000),
(99, 'Sem1', 'AK-CL-PN-LA2', 'AK', 'LA2', 'PN', 'UU Piano Semester 1', 360000),
(100, 'Sem2', 'AK-CL-PN-LA2', 'AK', 'LA2', 'PN', 'UU Piano Semester 1', 450000),
(101, 'USS', 'AK-CL-BL-PD1', 'AK', 'PD1', 'BL', 'US Biola', 350000),
(102, 'USK', 'AK-CL-BL-PD1', 'AK', 'PD1', 'BL', 'USK Biola', 370000),
(103, 'Sem1', 'AK-CL-BL-PD1', 'AK', 'PD1', 'BL', 'UU Biola Semester 1', 150000),
(104, 'Sem2', 'AK-CL-BL-PD1', 'AK', 'PD1', 'BL', 'UU Biola Semester 1', 150000),
(105, 'USS', 'AK-CL-BL-DS1', 'AK', 'DS1', 'BL', 'US Biola', 370000),
(106, 'USK', 'AK-CL-BL-DS1', 'AK', 'DS1', 'BL', 'USK Biola', 390000),
(107, 'Sem1', 'AK-CL-BL-DS1', 'AK', 'DS1', 'BL', 'UU Biola Semester 1', 160000),
(108, 'Sem2', 'AK-CL-BL-DS1', 'AK', 'DS1', 'BL', 'UU Biola Semester 1', 160000),
(109, 'USS', 'AK-CL-BL-DS2', 'AK', 'DS2', 'BL', 'US Biola', 400000),
(110, 'USK', 'AK-CL-BL-DS2', 'AK', 'DS2', 'BL', 'USK Biola', 420000),
(111, 'Sem1', 'AK-CL-BL-DS2', 'AK', 'DS2', 'BL', 'UU Biola Semester 1', 170000),
(112, 'Sem2', 'AK-CL-BL-DS2', 'AK', 'DS2', 'BL', 'UU Biola Semester 1', 170000),
(113, 'USS', 'AK-CL-BL-DS3', 'AK', 'DS3', 'BL', 'US Biola', 420000),
(114, 'USK', 'AK-CL-BL-DS3', 'AK', 'DS3', 'BL', 'USK Biola', 440000),
(115, 'Sem1', 'AK-CL-BL-DS3', 'AK', 'DS3', 'BL', 'UU Biola Semester 1', 190000),
(116, 'Sem2', 'AK-CL-BL-DS3', 'AK', 'DS3', 'BL', 'UU Biola Semester 1', 190000),
(117, 'USS', 'AK-CL-BL-DS4', 'AK', 'DS4', 'BL', 'US Biola', 440000),
(118, 'USK', 'AK-CL-BL-DS4', 'AK', 'DS4', 'BL', 'USK Biola', 460000),
(119, 'Sem1', 'AK-CL-BL-DS4', 'AK', 'DS4', 'BL', 'UU Biola Semester 1', 210000),
(120, 'Sem2', 'AK-CL-BL-DS4', 'AK', 'DS4', 'BL', 'UU Biola Semester 1', 210000),
(121, 'USS', 'AK-CL-BL-DS5', 'AK', 'DS5', 'BL', 'US Biola', 510000),
(122, 'USK', 'AK-CL-BL-DS5', 'AK', 'DS5', 'BL', 'USK Biola', 530000),
(123, 'Sem1', 'AK-CL-BL-DS5', 'AK', 'DS5', 'BL', 'UU Biola Semester 1', 230000),
(124, 'Sem2', 'AK-CL-BL-DS5', 'AK', 'DS5', 'BL', 'UU Biola Semester 1', 280000),
(125, 'USS', 'AK-CL-BL-MN1', 'AK', 'MN1', 'BL', 'US Biola', 530000),
(126, 'USK', 'AK-CL-BL-MN1', 'AK', 'MN1', 'BL', 'USK Biola', 550000),
(127, 'Sem1', 'AK-CL-BL-MN1', 'AK', 'MN1', 'BL', 'UU Biola Semester 1', 240000),
(128, 'Sem2', 'AK-CL-BL-MN1', 'AK', 'MN1', 'BL', 'UU Biola Semester 1', 250000),
(129, 'USS', 'AK-CL-BL-MN2', 'AK', 'MN2', 'BL', 'US Biola', 560000),
(130, 'USK', 'AK-CL-BL-MN2', 'AK', 'MN2', 'BL', 'USK Biola', 580000),
(131, 'Sem1', 'AK-CL-BL-MN2', 'AK', 'MN2', 'BL', 'UU Biola Semester 1', 260000),
(132, 'Sem2', 'AK-CL-BL-MN2', 'AK', 'MN2', 'BL', 'UU Biola Semester 1', 270000),
(133, 'USS', 'AK-CL-BL-MN3', 'AK', 'MN3', 'BL', 'US Biola', 650000),
(134, 'USK', 'AK-CL-BL-MN3', 'AK', 'MN3', 'BL', 'USK Biola', 670000),
(135, 'Sem1', 'AK-CL-BL-MN3', 'AK', 'MN3', 'BL', 'UU Biola Semester 1', 300000),
(136, 'Sem2', 'AK-CL-BL-MN3', 'AK', 'MN3', 'BL', 'UU Biola Semester 1', 350000),
(137, 'USS', 'AK-CL-BL-LA1', 'AK', 'LA1', 'BL', 'US Biola', 700000),
(138, 'USK', 'AK-CL-BL-LA1', 'AK', 'LA1', 'BL', 'USK Biola', 720000),
(139, 'Sem1', 'AK-CL-BL-LA1', 'AK', 'LA1', 'BL', 'UU Biola Semester 1', 310000),
(140, 'Sem2', 'AK-CL-BL-LA1', 'AK', 'LA1', 'BL', 'UU Biola Semester 1', 330000),
(141, 'USS', 'AK-CL-BL-LA2', 'AK', 'LA2', 'BL', 'US Biola', 800000),
(142, 'USK', 'AK-CL-BL-LA2', 'AK', 'LA2', 'BL', 'USK Biola', 820000),
(143, 'Sem1', 'AK-CL-BL-LA2', 'AK', 'LA2', 'BL', 'UU Biola Semester 1', 360000),
(144, 'Sem2', 'AK-CL-BL-LA2', 'AK', 'LA2', 'BL', 'UU Biola Semester 1', 450000),
(145, 'USS', 'AK-CL-OF-PK1', 'AK', 'PK1', 'OF', 'US Orff', 150000),
(146, 'USK', 'AK-CL-OF-PK1', 'AK', 'PK1', 'OF', 'USK Orff', 170000),
(147, 'Sem1', 'AK-CL-OF-PK1', 'AK', 'PK1', 'OF', 'UU Orff Semester 1', 150000),
(148, 'Sem2', 'AK-CL-OF-PK1', 'AK', 'PK1', 'OF', 'UU Orff Semester 1', 150000),
(149, 'USS', 'AK-CL-OF-PK2', 'AK', 'PK2', 'OF', 'US Orff', 180000),
(150, 'USK', 'AK-CL-OF-PK2', 'AK', 'PK2', 'OF', 'USK Orff', 200000),
(151, 'Sem1', 'AK-CL-OF-PK2', 'AK', 'PK2', 'OF', 'UU Orff Semester 1', 180000),
(152, 'Sem2', 'AK-CL-OF-PK2', 'AK', 'PK2', 'OF', 'UU Orff Semester 1', 180000),
(153, 'USS', 'AK-CL-CM-CM0', 'AK', 'CM0', 'CM', 'US Chamber Musik', 400000),
(154, 'USK', 'AK-CL-CM-CM0', 'AK', 'CM0', 'CM', 'USK Chamber Musik', 420000),
(155, 'Sem1', 'AK-CL-CM-CM0', 'AK', 'CM0', 'CM', 'UU Chamber Music Semester 1', 200000),
(156, 'Sem2', 'AK-CL-CM-CM0', 'AK', 'CM0', 'CM', 'UU Chamber Music Semester 1', 200000),
(157, 'USS', 'AV-PP-PN-PD0', 'AV', 'PD0', 'PN', 'US Piano', 330000),
(158, 'USK', 'AV-PP-PN-PD0', 'AV', 'PD0', 'PN', 'USK Piano', 350000),
(159, 'Sem1', 'AV-PP-PN-PD0', 'AV', 'PD0', 'PN', 'UU Piano Semester 1', 0),
(160, 'Sem2', 'AV-PP-PN-PD0', 'AV', 'PD0', 'PN', 'UU Piano Semester 1', 0),
(161, 'USS', 'AV-PP-VC-PD0', 'AV', 'PD0', 'VC', 'US Vocal', 280000),
(162, 'USK', 'AV-PP-VC-PD0', 'AV', 'PD0', 'VC', 'USK Vocal', 300000),
(163, 'Sem1', 'AV-PP-VC-PD0', 'AV', 'PD0', 'VC', 'UU Vocal Semester 1', 0),
(164, 'Sem2', 'AV-PP-VC-PD0', 'AV', 'PD0', 'VC', 'UU Vocal Semester 1', 0),
(165, 'USS', 'AV-PP-KB-PD0', 'AV', 'PD0', 'KB', 'US Keyboard', 330000),
(166, 'USK', 'AV-PP-KB-PD0', 'AV', 'PD0', 'KB', 'USK Keyboard', 350000),
(167, 'Sem1', 'AV-PP-KB-PD0', 'AV', 'PD0', 'KB', 'UU Keyboard Semester 1', 0),
(168, 'Sem2', 'AV-PP-KB-PD0', 'AV', 'PD0', 'KB', 'UU Keyboard Semester 1', 0),
(169, 'USS', 'AV-PP-GT-PD0', 'AV', 'PD0', 'GT', 'US Gitar', 280000),
(170, 'USK', 'AV-PP-GT-PD0', 'AV', 'PD0', 'GT', 'USK Gitar', 300000),
(171, 'Sem1', 'AV-PP-GT-PD0', 'AV', 'PD0', 'GT', 'UU Gitar Semester 1', 0),
(172, 'Sem2', 'AV-PP-GT-PD0', 'AV', 'PD0', 'GT', 'UU Gitar Semester 1', 0),
(173, 'USS', 'AV-AT-MK-CR0', 'AV', 'CR0', 'MK', 'US Melukis', 120000),
(174, 'USK', 'AV-AT-MK-CR0', 'AV', 'CR0', 'MK', 'USK Melukis', 140000),
(175, 'Sem1', 'AV-AT-MK-CR0', 'AV', 'CR0', 'MK', 'UU Melukis Semester 1', 0),
(176, 'Sem2', 'AV-AT-MK-CR0', 'AV', 'CR0', 'MK', 'UU Melukis Semester 1', 0),
(177, 'USS', 'AV-AT-MK-DW0', 'AV', 'DW0', 'MK', 'US Melukis', 160000),
(178, 'USK', 'AV-AT-MK-DW0', 'AV', 'DW0', 'MK', 'USK Melukis', 180000),
(179, 'Sem1', 'AV-AT-MK-DW0', 'AV', 'DW0', 'MK', 'UU Melukis Semester 1', 0),
(180, 'Sem2', 'AV-AT-MK-DW0', 'AV', 'DW0', 'MK', 'UU Melukis Semester 1', 0),
(181, 'USS', 'AV-AT-MK-SK0', 'AV', 'SK0', 'MK', 'US Melukis', 160000),
(182, 'USK', 'AV-AT-MK-SK0', 'AV', 'SK0', 'MK', 'USK Melukis', 180000),
(183, 'Sem1', 'AV-AT-MK-SK0', 'AV', 'SK0', 'MK', 'UU Melukis Semester 1', 0),
(184, 'Sem2', 'AV-AT-MK-SK0', 'AV', 'SK0', 'MK', 'UU Melukis Semester 1', 0),
(185, 'USS', 'AV-AT-MK-PT0', 'AV', 'PT0', 'MK', 'US Melukis', 250000),
(186, 'USK', 'AV-AT-MK-PT0', 'AV', 'PT0', 'MK', 'USK Melukis', 270000),
(187, 'Sem1', 'AV-AT-MK-PT0', 'AV', 'PT0', 'MK', 'UU Melukis Semester 1', 0),
(188, 'Sem2', 'AV-AT-MK-PT0', 'AV', 'PT0', 'MK', 'UU Melukis Semester 1', 0),
(189, 'USS', 'AV-DC-MD-PD0', 'AV', 'PD0', 'MD', 'US Modern', 220000),
(190, 'USK', 'AV-DC-MD-PD0', 'AV', 'PD0', 'MD', 'USK Modern', 240000),
(191, 'Sem1', 'AV-DC-MD-PD0', 'AV', 'PD0', 'MD', 'UU Modern Semester 1', 0),
(192, 'Sem2', 'AV-DC-MD-PD0', 'AV', 'PD0', 'MD', 'UU Modern Semester 1', 0),
(193, 'USS', 'AV-DC-TD-PD0', 'AV', 'PD0', 'TD', 'US Tradisional', 220000),
(194, 'USK', 'AV-DC-TD-PD0', 'AV', 'PD0', 'TD', 'USK Tradisional', 240000),
(195, 'Sem1', 'AV-DC-TD-PD0', 'AV', 'PD0', 'TD', 'UU Tradisional Semester 1', 0),
(196, 'Sem2', 'AV-DC-TD-PD0', 'AV', 'PD0', 'TD', 'UU Tradisional Semester 1', 0),
(197, 'USS', 'AV-GM-JW-PD0', 'AV', 'PD0', 'JW', 'US Jawa', 220000),
(198, 'USK', 'AV-GM-JW-PD0', 'AV', 'PD0', 'JW', 'USK Jawa', 240000),
(199, 'Sem1', 'AV-GM-JW-PD0', 'AV', 'PD0', 'JW', 'UU Jawa Semester 1', 0),
(200, 'Sem2', 'AV-GM-JW-PD0', 'AV', 'PD0', 'JW', 'UU Jawa Semester 1', 0),
(201, 'USS', 'AB-RY-PN-GR1', 'AB', 'GR1', 'PN', 'US Piano', 420000),
(202, 'USK', 'AB-RY-PN-GR1', 'AB', 'GR1', 'PN', 'USK Piano', 440000),
(203, 'Sem1', 'AB-RY-PN-GR1', 'AB', 'GR1', 'PN', 'UU Piano Semester 1', 0),
(204, 'Sem2', 'AB-RY-PN-GR1', 'AB', 'GR1', 'PN', 'UU Piano Semester 1', 0),
(205, 'adm', NULL, 'AK', NULL, NULL, 'ADM AKADEMIS', 200000),
(206, 'adm', NULL, 'NA', NULL, NULL, 'ADM NON AKADEMIS', 200000),
(207, 'adm', NULL, 'AV', NULL, NULL, 'ADM AVOKASI', 50000),
(208, 'adm', NULL, 'AB', NULL, NULL, 'ADM ABRSM', 50000),
(209, 'daftar', NULL, 'NA', NULL, NULL, 'UP NON AKADEMIS', 350000),
(210, 'daftar', NULL, 'AK', NULL, NULL, 'UP AKADEMIS', 350000),
(211, 'daftar', NULL, 'AV', NULL, NULL, 'UP AVOKASI', 100000),
(212, 'daftar', NULL, 'AB', NULL, NULL, 'UP ABRSM', 100000),
(213, 'test', NULL, NULL, NULL, 'PN', 'UT PIANO', 75000);

-- --------------------------------------------------------

--
-- Table structure for table `denda`
--

CREATE TABLE IF NOT EXISTS `denda` (
  `kd_denda` varchar(5) NOT NULL,
  `nm_denda` varchar(35) NOT NULL,
  `status` varchar(15) NOT NULL,
  `harga` int(11) NOT NULL,
  `ket_denda` text NOT NULL,
  PRIMARY KEY (`kd_denda`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `denda`
--

INSERT INTO `denda` (`kd_denda`, `nm_denda`, `status`, `harga`, `ket_denda`) VALUES
('DD1', 'Denda Keterlambatan Bulanan', 'bulanan', 10000, '');

-- --------------------------------------------------------

--
-- Table structure for table `diskon`
--

CREATE TABLE IF NOT EXISTS `diskon` (
  `kd_diskon` varchar(5) NOT NULL,
  `nm_diskon` varchar(35) NOT NULL,
  `persen` float NOT NULL,
  `ket_diskon` varchar(200) NOT NULL,
  PRIMARY KEY (`kd_diskon`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `diskon`
--

INSERT INTO `diskon` (`kd_diskon`, `nm_diskon`, `persen`, `ket_diskon`) VALUES
('DK1', 'Diskon Anak Guru', 50, ''),
('DK2', 'Diskon Sttc Group', 50, '');

-- --------------------------------------------------------

--
-- Table structure for table `diskon_p_kwitansipljr`
--

CREATE TABLE IF NOT EXISTS `diskon_p_kwitansipljr` (
  `no_kwitansi` varchar(11) NOT NULL,
  `kd_diskon` varchar(5) NOT NULL,
  `kd_biaya` int(10) NOT NULL,
  `persen_diskon` float NOT NULL,
  `ket` varchar(200) NOT NULL,
  PRIMARY KEY (`no_kwitansi`,`kd_diskon`,`kd_biaya`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `diskon_p_kwitansipljr`
--

INSERT INTO `diskon_p_kwitansipljr` (`no_kwitansi`, `kd_diskon`, `kd_biaya`, `persen_diskon`, `ket`) VALUES
('U00001/2014', 'DK2', 9, 50, 'Diskon Sttc Group');

-- --------------------------------------------------------

--
-- Table structure for table `jenis`
--

CREATE TABLE IF NOT EXISTS `jenis` (
  `kd_jenis` varchar(3) NOT NULL,
  `nm_jenis` varchar(35) NOT NULL,
  PRIMARY KEY (`kd_jenis`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jenis`
--

INSERT INTO `jenis` (`kd_jenis`, `nm_jenis`) VALUES
('AM', 'Analisa Musik'),
('BA', 'Biola Alto'),
('BL', 'Biola'),
('CL', 'Cello'),
('CM', 'Chamber Music'),
('CT', 'Clarinet'),
('DR', 'Drum'),
('GT', 'Gitar'),
('HN', 'Horn'),
('HR', 'Harmoni'),
('JW', 'Jawa'),
('KB', 'Keyboard'),
('KT', 'Kontra Bass'),
('MD', 'Modern'),
('MK', 'Melukis'),
('OF', 'Orff'),
('PN', 'Piano'),
('SF', 'Solfeggio'),
('SJ', 'Sejarah'),
('TD', 'Tradisional'),
('TR', 'Teori'),
('VC', 'Vocal');

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE IF NOT EXISTS `login` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(15) NOT NULL,
  `pass` varchar(40) NOT NULL,
  `level` varchar(5) NOT NULL,
  `stat` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`id`, `name`, `pass`, `level`, `stat`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'admin', 0),
(2, 'user', 'ee11cbb19052e40b07aac0ca060c23ee', 'user', 0);

-- --------------------------------------------------------

--
-- Table structure for table `m_kwitansibrg`
--

CREATE TABLE IF NOT EXISTS `m_kwitansibrg` (
  `no_kwitansi` varchar(11) NOT NULL DEFAULT '',
  `tgl_kwitansi` date NOT NULL,
  `nim` varchar(10) DEFAULT NULL,
  `nama` varchar(35) NOT NULL,
  `total` int(11) NOT NULL,
  `keterangan` varchar(50) NOT NULL,
  PRIMARY KEY (`no_kwitansi`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_kwitansibrg`
--

INSERT INTO `m_kwitansibrg` (`no_kwitansi`, `tgl_kwitansi`, `nim`, `nama`, `total`, `keterangan`) VALUES
('B00001/2014', '2014-04-30', '7676', 'wawewoiwoi', 130500, ''),
('B00002/2014', '2014-04-30', NULL, 'Jimmy', 188000, ''),
('B00003/2014', '2014-04-30', '7676', 'wawewoiwoi', 167000, ''),
('B00004/2014', '2014-06-25', NULL, 'tester', 42000, '');

-- --------------------------------------------------------

--
-- Table structure for table `m_kwitansipljr`
--

CREATE TABLE IF NOT EXISTS `m_kwitansipljr` (
  `no_kwitansi` varchar(11) NOT NULL,
  `tgl_kwitansi` date NOT NULL,
  `nim` varchar(10) NOT NULL,
  `subtotal` int(11) NOT NULL,
  `nilai_diskon` int(11) NOT NULL,
  `nilai_denda` int(11) NOT NULL,
  `kurang_bayar` int(11) NOT NULL,
  `lebih_bayar` int(11) NOT NULL,
  `total` int(11) NOT NULL,
  `keterangan` varchar(50) NOT NULL,
  PRIMARY KEY (`no_kwitansi`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_kwitansipljr`
--

INSERT INTO `m_kwitansipljr` (`no_kwitansi`, `tgl_kwitansi`, `nim`, `subtotal`, `nilai_diskon`, `nilai_denda`, `kurang_bayar`, `lebih_bayar`, `total`, `keterangan`) VALUES
('U00001/2014', '2014-06-24', '421', 250000, 125000, 0, 0, 20000, 105000, ''),
('U00002/2014', '2014-06-25', '7676', 420000, 0, 0, 0, 0, 420000, '');

-- --------------------------------------------------------

--
-- Table structure for table `murid`
--

CREATE TABLE IF NOT EXISTS `murid` (
  `nim` varchar(10) NOT NULL,
  `nm_murid` varchar(50) NOT NULL,
  `ket` varchar(100) NOT NULL,
  PRIMARY KEY (`nim`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `murid`
--

INSERT INTO `murid` (`nim`, `nm_murid`, `ket`) VALUES
('1123', 'Andrea Tanu Wijaya', 'Pianist? nah. dunno ''bout dat'),
('3321', '123', 'aasdasd'),
('421', 'Silvia Yang', 'The Perfect match'),
('7676', 'wawewoiwoi', 'anak jelek');

-- --------------------------------------------------------

--
-- Table structure for table `p_dendakwipljr`
--

CREATE TABLE IF NOT EXISTS `p_dendakwipljr` (
  `no_kwitansi` varchar(11) NOT NULL,
  `kd_denda` varchar(5) NOT NULL,
  `periode` int(11) NOT NULL,
  `bulan` varchar(25) NOT NULL,
  `status` varchar(15) NOT NULL,
  `harga` int(11) NOT NULL,
  `ket` varchar(200) NOT NULL,
  PRIMARY KEY (`no_kwitansi`,`kd_denda`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `p_kwitansibrg`
--

CREATE TABLE IF NOT EXISTS `p_kwitansibrg` (
  `no_kwitansi` varchar(11) NOT NULL,
  `tgl_kwitansi` date NOT NULL,
  `kd_barang` varchar(10) NOT NULL,
  `harga` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL,
  PRIMARY KEY (`no_kwitansi`,`kd_barang`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `p_kwitansibrg`
--

INSERT INTO `p_kwitansibrg` (`no_kwitansi`, `tgl_kwitansi`, `kd_barang`, `harga`, `qty`, `jumlah`) VALUES
('B00001/2014', '2014-04-30', '002', 21000, 6, 126000),
('B00001/2014', '2014-04-30', 'ABC123', 1500, 3, 4500),
('B00002/2014', '2014-04-30', '002', 21000, 3, 63000),
('B00002/2014', '2014-04-30', '441', 62500, 2, 125000),
('B00003/2014', '2014-04-30', '002', 21000, 2, 42000),
('B00003/2014', '2014-04-30', '441', 62500, 2, 125000),
('B00004/2014', '2014-06-25', '002', 21000, 2, 42000);

-- --------------------------------------------------------

--
-- Table structure for table `p_kwitansipljr`
--

CREATE TABLE IF NOT EXISTS `p_kwitansipljr` (
  `no_kwitansi` varchar(11) NOT NULL,
  `tgl_kwitansi` date NOT NULL,
  `kd_biaya` int(10) unsigned NOT NULL,
  `bulan` varchar(25) NOT NULL,
  `jlh_bln` int(11) NOT NULL,
  `tahun` year(4) NOT NULL,
  `biaya` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL,
  PRIMARY KEY (`no_kwitansi`,`kd_biaya`,`tahun`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `p_kwitansipljr`
--

INSERT INTO `p_kwitansipljr` (`no_kwitansi`, `tgl_kwitansi`, `kd_biaya`, `bulan`, `jlh_bln`, `tahun`, `biaya`, `jumlah`) VALUES
('U00001/2014', '2014-06-24', 9, '1', 1, 2014, 250000, 250000),
('U00002/2014', '2014-06-25', 201, '1', 1, 2014, 420000, 420000);

-- --------------------------------------------------------

--
-- Table structure for table `pelajaran`
--

CREATE TABLE IF NOT EXISTS `pelajaran` (
  `kd_pelajaran` varchar(15) NOT NULL,
  `nm_pelajaran` varchar(70) NOT NULL,
  `kd_jenis` varchar(10) NOT NULL,
  `kd_tingkat` varchar(10) NOT NULL,
  `kd_program` varchar(10) NOT NULL,
  `kd_subprogram` varchar(10) NOT NULL,
  `ket` varchar(100) NOT NULL,
  PRIMARY KEY (`kd_pelajaran`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pelajaran`
--

INSERT INTO `pelajaran` (`kd_pelajaran`, `nm_pelajaran`, `kd_jenis`, `kd_tingkat`, `kd_program`, `kd_subprogram`, `ket`) VALUES
('AB-RY-PN-GR1', 'ABRSM-Royal-Piano-Grade 1', 'PN', 'GR1', 'AB', 'RY', '0'),
('AK-CL-AM-AM0', 'AKADEMIS-Classic-Analisa Musik-Analisa Musik', 'AM', 'AM0', 'AK', 'CL', '0'),
('AK-CL-BL-DS1', 'AKADEMIS-Classic-Biola-Dasar 1', 'BL', 'DS1', 'AK', 'CL', '0'),
('AK-CL-BL-DS2', 'AKADEMIS-Classic-Biola-Dasar 2', 'BL', 'DS2', 'AK', 'CL', '0'),
('AK-CL-BL-DS3', 'AKADEMIS-Classic-Biola-Dasar 3', 'BL', 'DS3', 'AK', 'CL', '0'),
('AK-CL-BL-DS4', 'AKADEMIS-Classic-Biola-Dasar 4', 'BL', 'DS4', 'AK', 'CL', '0'),
('AK-CL-BL-DS5', 'AKADEMIS-Classic-Biola-Dasar 5', 'BL', 'DS5', 'AK', 'CL', '0'),
('AK-CL-BL-LA1', 'AKADEMIS-Classic-Biola-Lanjutan Atas 1', 'BL', 'LA1', 'AK', 'CL', '0'),
('AK-CL-BL-LA2', 'AKADEMIS-Classic-Biola-Lanjutan Atas 2', 'BL', 'LA2', 'AK', 'CL', '0'),
('AK-CL-BL-MN1', 'AKADEMIS-Classic-Biola-Menengah 1', 'BL', 'MN1', 'AK', 'CL', '0'),
('AK-CL-BL-MN2', 'AKADEMIS-Classic-Biola-Menengah 2', 'BL', 'MN2', 'AK', 'CL', '0'),
('AK-CL-BL-MN3', 'AKADEMIS-Classic-Biola-Menengah 3', 'BL', 'MN3', 'AK', 'CL', '0'),
('AK-CL-BL-PD1', 'AKADEMIS-Classic-Biola-Pra Dasar 1', 'BL', 'PD1', 'AK', 'CL', '0'),
('AK-CL-CM-CM0', 'AKADEMIS-Classic-Chamber Music-Chamber Music', 'CM', 'CM0', 'AK', 'CL', '0'),
('AK-CL-HR-HR0', 'AKADEMIS-Classic-Harmoni-Harmoni', 'HR', 'HR0', 'AK', 'CL', '0'),
('AK-CL-OF-PK1', 'AKADEMIS-Classic-Orff-Perkusi 1', 'OF', 'PK1', 'AK', 'CL', '0'),
('AK-CL-OF-PK2', 'AKADEMIS-Classic-Orff-Perkusi 2', 'OF', 'PK2', 'AK', 'CL', '0'),
('AK-CL-PN-DS1', 'AKADEMIS-Classic-Piano-Dasar 1', 'PN', 'DS1', 'AK', 'CL', '0'),
('AK-CL-PN-DS2', 'AKADEMIS-Classic-Piano-Dasar 2', 'PN', 'DS2', 'AK', 'CL', '0'),
('AK-CL-PN-DS3', 'AKADEMIS-Classic-Piano-Dasar 3', 'PN', 'DS3', 'AK', 'CL', '0'),
('AK-CL-PN-DS4', 'AKADEMIS-Classic-Piano-Dasar 4', 'PN', 'DS4', 'AK', 'CL', '0'),
('AK-CL-PN-DS5', 'AKADEMIS-Classic-Piano-Dasar 5', 'PN', 'DS5', 'AK', 'CL', '0'),
('AK-CL-PN-LA1', 'AKADEMIS-Classic-Piano-Lanjutan Atas 1', 'PN', 'LA1', 'AK', 'CL', '0'),
('AK-CL-PN-LA2', 'AKADEMIS-Classic-Piano-Lanjutan Atas 2', 'PN', 'LA2', 'AK', 'CL', '0'),
('AK-CL-PN-MN1', 'AKADEMIS-Classic-Piano-Menengah 1', 'PN', 'MN1', 'AK', 'CL', '0'),
('AK-CL-PN-MN2', 'AKADEMIS-Classic-Piano-Menengah 2', 'PN', 'MN2', 'AK', 'CL', '0'),
('AK-CL-PN-MN3', 'AKADEMIS-Classic-Piano-Menengah 3', 'PN', 'MN3', 'AK', 'CL', '0'),
('AK-CL-PN-PD0', 'AKADEMIS-Classic-Piano-Pra Dasar', 'PN', 'PD0', 'AK', 'CL', '0'),
('AK-CL-SF-DS1', 'AKADEMIS-Classic-Solfeggio-Dasar 1', 'SF', 'DS1', 'AK', 'CL', '0'),
('AK-CL-SF-DS2', 'AKADEMIS-Classic-Solfeggio-Dasar 2', 'SF', 'DS2', 'AK', 'CL', '0'),
('AK-CL-SF-LA1', 'AKADEMIS-Classic-Solfeggio-Lanjutan Atas 1', 'SF', 'LA1', 'AK', 'CL', '0'),
('AK-CL-SF-LA2', 'AKADEMIS-Classic-Solfeggio-Lanjutan Atas 2', 'SF', 'LA2', 'AK', 'CL', '0'),
('AK-CL-SF-MN1', 'AKADEMIS-Classic-Solfeggio-Menengah 1', 'SF', 'MN1', 'AK', 'CL', '0'),
('AK-CL-SF-MN2', 'AKADEMIS-Classic-Solfeggio-Menengah 2', 'SF', 'MN2', 'AK', 'CL', '0'),
('AK-CL-SF-MN3', 'AKADEMIS-Classic-Solfeggio-Menengah 3', 'SF', 'MN3', 'AK', 'CL', '0'),
('AK-CL-SF-PD1', 'AKADEMIS-Classic-Solfeggio-Pra Dasar 1', 'SF', 'PD1', 'AK', 'CL', '0'),
('AK-CL-SF-PD2', 'AKADEMIS-Classic-Solfeggio-Pra Dasar 2', 'SF', 'PD2', 'AK', 'CL', '0'),
('AK-CL-SJ-SJ0', 'AKADEMIS-Classic-Sejarah-Sejarah', 'SJ', 'SJ0', 'AK', 'CL', '0'),
('AV-AT-MK-CR0', 'AVOCATION-Art-Melukis-Colouring', 'MK', 'CR0', 'AV', 'AT', '0'),
('AV-AT-MK-DW0', 'AVOCATION-Art-Melukis-Drawing', 'MK', 'DW0', 'AV', 'AT', '0'),
('AV-AT-MK-PT0', 'AVOCATION-Art-Melukis-Painting', 'MK', 'PT0', 'AV', 'AT', '0'),
('AV-AT-MK-SK0', 'AVOCATION-Art-Melukis-Sketsa', 'MK', 'SK0', 'AV', 'AT', '0'),
('AV-DC-MD-PD0', 'AVOCATION-Dance-Modern-Pra Dasar', 'MD', 'PD0', 'AV', 'DC', '0'),
('AV-DC-TD-PD0', 'AVOCATION-Dance-Tradisional-Pra Dasar', 'TD', 'PD0', 'AV', 'DC', '0'),
('AV-GM-JW-PD0', 'AVOCATION-Gamelan-Jawa-Pra Dasar', 'JW', 'PD0', 'AV', 'GM', '0'),
('AV-PP-GT-PD0', 'AVOCATION-Pop-Gitar-Pra Dasar', 'GT', 'PD0', 'AV', 'PP', '0'),
('AV-PP-KB-PD0', 'AVOCATION-Pop-Keyboard-Pra Dasar', 'KB', 'PD0', 'AV', 'PP', '0'),
('AV-PP-PN-PD0', 'AVOCATION-Pop-Piano-Pra Dasar', 'PN', 'PD0', 'AV', 'PP', '0'),
('AV-PP-VC-PD0', 'AVOCATION-Pop-Vocal-Pra Dasar', 'VC', 'PD0', 'AV', 'PP', '0');

-- --------------------------------------------------------

--
-- Table structure for table `program`
--

CREATE TABLE IF NOT EXISTS `program` (
  `kd_program` varchar(3) NOT NULL,
  `nm_program` varchar(35) NOT NULL,
  PRIMARY KEY (`kd_program`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `program`
--

INSERT INTO `program` (`kd_program`, `nm_program`) VALUES
('AB', 'ABRSM'),
('AK', 'AKADEMIS'),
('AV', 'AVOCATION'),
('NA', 'NON AKADEMIS');

-- --------------------------------------------------------

--
-- Table structure for table `subprogram`
--

CREATE TABLE IF NOT EXISTS `subprogram` (
  `kd_subprogram` varchar(3) NOT NULL,
  `nm_subprogram` varchar(35) NOT NULL,
  PRIMARY KEY (`kd_subprogram`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subprogram`
--

INSERT INTO `subprogram` (`kd_subprogram`, `nm_subprogram`) VALUES
('AB', 'Alternative Blues'),
('AJ', 'Alternative Jazz'),
('AP', 'Alternative Pop'),
('AR', 'Alternative Rock'),
('AT', 'Art'),
('CL', 'Classic'),
('DC', 'Dance'),
('GM', 'Gamelan'),
('PP', 'Pop'),
('RY', 'Royal'),
('SI', 'Suzuki Individu'),
('SK', 'Suzuki Kelompok');

-- --------------------------------------------------------

--
-- Table structure for table `tingkat`
--

CREATE TABLE IF NOT EXISTS `tingkat` (
  `kd_tingkat` varchar(5) NOT NULL,
  `nm_tingkat` varchar(35) NOT NULL,
  PRIMARY KEY (`kd_tingkat`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tingkat`
--

INSERT INTO `tingkat` (`kd_tingkat`, `nm_tingkat`) VALUES
('AM0', 'Analisa Musik'),
('BK 7', 'Book 7'),
('BK1', 'Book 1'),
('BK10', 'Book 10'),
('BK2', 'Book 2'),
('BK3', 'Book 3'),
('BK4', 'Book 4'),
('BK5', 'Book 5'),
('BK6', 'Book 6'),
('BK8', 'Book 8'),
('BK9', 'Book 9'),
('CM0', 'Chamber Music'),
('CR0', 'Colouring'),
('DS1', 'Dasar 1'),
('DS2', 'Dasar 2'),
('DS3', 'Dasar 3'),
('DS4', 'Dasar 4'),
('DS5', 'Dasar 5'),
('DW0', 'Drawing'),
('GR1', 'Grade 1'),
('GR2', 'Grade 2'),
('GR3', 'Grade 3'),
('GR4', 'Grade 4'),
('GR5', 'Grade 5'),
('GR6', 'Grade 6'),
('GR7', 'Grade 7'),
('GR8', 'Grade 8'),
('HR0', 'Harmoni'),
('LA1', 'Lanjutan Atas 1'),
('LA2', 'Lanjutan Atas 2'),
('MN1', 'Menengah 1'),
('MN2', 'Menengah 2'),
('MN3', 'Menengah 3'),
('PD0', 'Pra Dasar'),
('PD1', 'Pra Dasar 1'),
('PD2', 'Pra Dasar 2'),
('PK1', 'Perkusi 1'),
('PK2', 'Perkusi 2'),
('PT0', 'Painting'),
('SJ0', 'Sejarah'),
('SK0', 'Sketsa');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
