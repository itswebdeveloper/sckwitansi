function check_pelajaran(){
	if($("#pTotal").text() == 0 && $("#dTotal").text() == 0){
		alert('Please Fill in the Pelajaran List');
	}else{
		frpelajaran.submit();
	}
}

//****  LIST PEMBAYARAN  ****
function addPembayaran(mtable){
	var bln=[];
	var valid = true;
	var periode = 1;
	var jb = ['USS','USK','Sem1','Sem2','adm','daftar','test'];	
	var pb = $("#pilih-biaya").val();

	if(pb == jb[0] || pb == jb[1]){
		var bln_from = parseInt($("#from").val());
		var bln_to = parseInt($("#to").val());
		var thn_from = parseInt($("#tahun_from").val());
		var thn_to = parseInt($("#tahun_to").val());

		if(thn_to == '' || bln_to == ''){
			bulan_from = bln_from;
			bulan_to = bln_from;
			tahun_from = thn_from;
			tahun_to = thn_from;
		}else if(thn_from > thn_to){
			valid = false;
		}else if(thn_from == thn_to){				
			if(bln_from > bln_to)
				valid = false;		
			else{
				tahun_from = thn_from;
				tahun_to = thn_to;
				bulan_from = bln_from;
				bulan_to = bln_to;
			}
		}else if(thn_from < thn_to){
			tahun_from = thn_from;
			tahun_to = thn_to;
			bulan_from = bln_from;
			bulan_to = bln_to;
		}
		if(valid){
			for(var i = tahun_from; i<=tahun_to; i++){				
				bln=[];
				var jlh_tahun = tahun_to - tahun_from;

				if(jlh_tahun == 0){
					for (var k = bulan_from; k<= bulan_to; k++){
						bln.push(k);
					}
				}else if(jlh_tahun > 0){					
					if(i==tahun_from){
						for (var j=bulan_from; j<=12; j++){
							bln.push(j);
						}
					}else if(i == tahun_to){
						for (var j=1;j<=bln_to; j++){
							bln.push(j);
						}
					}else{
						for(var j= 1;j<=12;j++){
							bln.push(j);
						}
					}				
				}																
				addList(mtable,bln,i,bln.length, parseInt((new Date).getFullYear()));				
			}
		}
	}else{
		addList(mtable,bln,'',periode);
	}
}

function addList(mtable,bln,thn,periode,cyear=null){
	var jb = ['USS','USK','Sem1','Sem2','adm','daftar','test'];
	var table = document.getElementById(mtable);
	var pb = $("#pilih-biaya").val();
	var pdb = $("#pilih-detail-biaya").val().split("|");
	var pembayaran = pdb[1];
	var ba = (pdb[2]).split("-");
	var biaya = ba[0];
	if(cyear != null)
		if(thn < cyear)
			biaya = ba[1];	

	var program = pdb[3];
	var tingkat = pdb[4];
	var subprogram = pdb[5];

	var rowCount = table.rows.length;
	var kd = pdb[0];
	var exist = false;	

	var subtotal =  biaya*(periode);	
	for(var i = 1; i < rowCount; i++){
		var row = table.rows[i];
		var rec_kd = row.cells[0].childNodes[0].value;
		var rec_thn = row.cells[0].childNodes[2].value;		
		if(rec_kd == kd && rec_thn == thn){			
			exist = true;
			row.cells[0].childNodes[1].value = bln;
			row.cells[0].childNodes[2].value = thn;
			row.cells[0].childNodes[3].value = subtotal;
			row.cells[0].childNodes[5].value = periode;

			row.cells[4].childNodes[0].value = '('+bln[0]+' - '+bln[periode-1]+') '+thn;
			row.cells[5].childNodes[0].value = periode;			
			row.cells[7].childNodes[0].value  = addCommas(biaya*(periode));
			
			total(mtable);
		}else if(rowCount > 4){
			exist = true;
		}
	}
	if(exist === true || $("#pilih-detail-biaya").val() == ''){
			
	}else{
		var row = table.insertRow(rowCount);				
		
		var kd_biaya = document.createElement("input");
		kd_biaya.type ="hidden";
		kd_biaya.name ="kd_biaya[]";
		kd_biaya.value = kd;
		var bulan = document.createElement("input");
		bulan.type ="hidden";
		bulan.name ="bulan[]";
		bulan.value = bln;		
		var tahun = document.createElement("input");
		tahun.type = "hidden";
		tahun.name = "tahun[]";
		tahun.value = thn;
		var stotal = document.createElement("input");
		stotal.type = "hidden";
		stotal.name = "stotal[]";
		stotal.value = subtotal;
		var jlh = document.createElement("input");
		jlh.type ="hidden";
		jlh.name ="periode[]";
		jlh.value = periode;
		

		var cell1 = row.insertCell(0);							//cell 1
		var element1 = document.createElement("input");
		element1.type = "button";
		element1.readOnly = "true";
		element1.style.width = "100%";
		element1.className = "btn btn-primary";
		element1.style.textAlign ="left";
		element1.value = pembayaran;		
		cell1.appendChild(kd_biaya);
		cell1.appendChild(bulan);
		cell1.appendChild(tahun);
		cell1.appendChild(stotal);
		cell1.appendChild(element1);
		cell1.appendChild(jlh);

		var cell2 = row.insertCell(1);							//cell 2
		var element2 = document.createElement("input");
		element2.type = "button";
		element2.readOnly = "true";
		element2.style.width = "100%";
		element2.className = "btn btn-primary";
		element2.style.textAlign ="center";
		element2.value = program;
		cell2.appendChild(element2);

		var cellsubprogam = row.insertCell(2);							//cell 3
		var elementsubprogam = document.createElement("input");
		elementsubprogam.type = "button";
		elementsubprogam.readOnly = "true";
		elementsubprogam.style.width = "100%";
		elementsubprogam.className = "btn btn-primary";
		elementsubprogam.style.textAlign ="center";
		elementsubprogam.value = subprogram;
		cellsubprogam.appendChild(elementsubprogam);

		var cell3 = row.insertCell(3);							//cell 3
		var element3 = document.createElement("input");
		element3.type = "button";
		element3.readOnly = "true";
		element3.style.width = "100%";
		element3.className = "btn btn-primary";
		element3.style.textAlign ="center";
		element3.value = tingkat;
		cell3.appendChild(element3);
		
		var cell4 = row.insertCell(4);							//cell 4
		var element4 = document.createElement("input");
		element4.type = "button";
		element4.readOnly = "true";
		element4.style.width = "100%";
		element4.className = "btn btn-primary";
		element4.style.textAlign ="center";
		element4.value = '('+bln[0]+' - '+bln[periode-1]+') '+thn;
		cell4.appendChild(element4);

		var cell5 = row.insertCell(5);							//cell 5
		var element5 = document.createElement("input");
		element5.type = "button";
		element5.readOnly = "true";
		element5.style.width = "100%";
		element5.className = "btn btn-primary";
		element5.style.textAlign ="center";
		element5.value = periode;
		cell5.appendChild(element5);

		var cell6 = row.insertCell(6);							//cell 6
		var element6 = document.createElement("input");
		element6.type = "button";
		element6.readOnly = "true";
		element6.style.width = "100%";
		element6.className = "btn btn-primary";
		element6.style.textAlign ="right";
		element6.value = addCommas(biaya);
		cell6.appendChild(element6);

		var cell7 = row.insertCell(7);							//cell 7
		var element7 = document.createElement("input");
		element7.type = "button";
		element7.readOnly = "true";
		element7.style.width = "100%";
		element7.className = "btn btn-primary";
		element7.style.textAlign ="right";
		element7.value = addCommas(biaya*(periode));
		cell7.appendChild(element7);

		var cell8 = row.insertCell(8);							//cell 8
		var element8 = document.createElement("input");
		element8.type = "button";
		element8.className = "btn btn-danger";
		element8.value = "Del.";
		element8.setAttribute("onClick","deleteRow('"+kd+"','ListTable')");

		var d_kd_pembayaran = "["+program+"] "+pembayaran+" "+subprogram+" "+tingkat;
		var format_nmpembayaran = "["+program+"] "+pembayaran+" "+subprogram+" "+tingkat;
		var addDiskon = document.createElement("input");
		addDiskon.type = "button";
		addDiskon.className = "btn btn-warning";
		addDiskon.value = "Dis.";
		addDiskon.setAttribute("onClick","filldBiaya('"+kd+"','"+format_nmpembayaran+"','"+d_kd_pembayaran+"')");
		addDiskon.setAttribute("href","#diskon_modal");
		addDiskon.setAttribute("data-toggle","modal");
		cell8.appendChild(addDiskon);
		cell8.appendChild(element8);
	
		$("#pTotal").html(rowCount);
		total(mtable);
	}
}

function filldBiaya(kode, format_nmpembayaran, d_kd_pembayaran){
	$("#d_kd_biaya").val(kode);
	$("#d_nm_pembayaran").val(format_nmpembayaran);
	$("#d_kd_pembayaran").val(d_kd_pembayaran);
}

function total(mtable){
	var table = document.getElementById(mtable);
	var rowCount = table.rows.length;
	var total=0;
	for(var i = 1; i < rowCount; i++){
		var row = table.rows[i];
		var stotal = parseInt(row.cells[0].childNodes[3].value);
		total += stotal;
	}
	$('#total').html("Total : Rp. "+addCommas(total));
}

function deleteRow(kode,mtable){
	var table = document.getElementById(mtable);
	var rowCount = table.rows.length;
	for(var i = 1; i < rowCount; i++){
		var row = table.rows[i];
		var rec_kd = row.cells[0].childNodes[0].value;
		if(rec_kd == kode){			
			table.deleteRow(i);
			rowCount--;
			i--;
			
			document.getElementById('pTotal').innerHTML = rowCount-1;
			
			var table2 = document.getElementById("DiskonListTable");			
			var rowCount2 = table2.rows.length;			
			for(var j = 1; j < rowCount2; j++){				
				var row2 = table2.rows[j];
				var rec_kd2 = row2.cells[0].childNodes[1].value;				
				if(rec_kd2 == kode){
					table2.deleteRow(j);
					rowCount2--;
					j--;
				}
			}
		}

	}	
	total(mtable);
}

function addCommas(nStr){
	nStr += '';
	x = nStr.split('.');
	x1 = x[0];
	x2 = x.length > 1 ? '.' + x[1] : '';
	var rgx = /(\d+)(\d{3})/;
	while (rgx.test(x1)){
		x1 = x1.replace(rgx, '$1' + ',' + '$2');
	}
	return x1 + x2;
}


//****  LIST DISKON  ****

function addDiskon(mtable){	
	var table = document.getElementById(mtable);
	var dsk = $("#pilih-diskon").val();
	var kd_biaya = $("#d_kd_biaya").val();
	var nm_pembayaran = $("#d_nm_pembayaran").val();
	var nm_diskon = '';
	var kd_diskon = '';
	var persen = 0;
	var exist = false;
	var d_kd_pembayaran = $("#d_kd_pembayaran").val();

	var rowCount = table.rows.length;
	if(rowCount >= 6){
		exist = true;
	}
	else if(rowCount == 1){			
		if(dsk == 'lain'){
			nm_diskon = $("#ket_diskon").val();
			persen = $("#p_diskon").val();			
			kd_diskon = 'LN0';			
		}else{
			split_dsk = dsk.split('|');
			kd_diskon = split_dsk[0];
			nm_diskon = split_dsk[1];
			persen = split_dsk[2];
		}		
	}else{				
		for(var i = 1; i < rowCount; i++){
			var row = table.rows[i];
			var rec_kd = row.cells[0].childNodes[0].value;
			var rec_kd_diskon = row.cells[0].childNodes[2].value;
			var last_ln = '';

			if(dsk == 'lain'){				
				nm_diskon = $("#ket_diskon").val();
				persen = $("#p_diskon").val();			
				if(rec_kd.substring(0,2) == "LN"){
					last_ln = rec_kd_diskon;				
				}				
			}else{
				split_dsk = dsk.split('|');
				kd_diskon = split_dsk[0];
				nm_diskon = split_dsk[1];
				persen = split_dsk[2];

				if(rec_kd == (kd_diskon+''+kd_biaya)){
					exist = true;			
				}				
			}	
		}

		if(dsk == "lain"){
			if(last_ln == '')
				kd_diskon = 'LN0';
			else{
				var newn = parseInt(last_ln.substring(2,10))+1;
				kd_diskon = 'LN'+newn;				
			}
		}		
	}
	
	if(exist === true || $("#pilih-diskon").val() == '' || nmdiskon == '' || persen>100 || persen<=0){
	}else{		
		var row = table.insertRow(rowCount);				
		
		var kddisba = document.createElement("input");
		kddisba.type ="hidden";
		kddisba.name ="kd_disba[]";
		kddisba.value = kd_diskon+''+kd_biaya;
		var kdbiaya = document.createElement("input");
		kdbiaya.type ="hidden";
		kdbiaya.name ="kd_biaya_diskon[]";
		kdbiaya.value = kd_biaya;
		var kddiskon = document.createElement("input");
		kddiskon.type ="hidden";
		kddiskon.name ="kd_diskon[]";
		kddiskon.value = kd_diskon;		
		var nmdiskon = document.createElement("input");
		nmdiskon.type ="hidden";
		nmdiskon.name ="ket_diskon[]";
		nmdiskon.value = nm_diskon;				
		var prsn = document.createElement("input");
		prsn.type = "hidden";
		prsn.name = "p_diskon[]";
		prsn.value = persen;
		var kd_pembayaran = document.createElement("input");
		kd_pembayaran.type = "hidden";
		kd_pembayaran.name = "kd_pembayaran[]";
		kd_pembayaran.value = d_kd_pembayaran;
		var cell1 = row.insertCell(0);							//cell 1
		var element1 = document.createElement("input");
		element1.type = "button";
		element1.readOnly = "true";
		element1.style.width = "100%";
		element1.className = "btn btn-primary";
		element1.style.textAlign ="left";
		element1.value = nm_pembayaran;
		cell1.appendChild(kddisba);
		cell1.appendChild(kdbiaya);
		cell1.appendChild(kddiskon);
		cell1.appendChild(nmdiskon);
		cell1.appendChild(prsn);
		cell1.appendChild(kd_pembayaran);
		cell1.appendChild(element1);

		var cell2 = row.insertCell(1);							//cell 2
		var element2 = document.createElement("input");
		element2.type = "button";
		element2.readOnly = "true";
		element2.style.width = "100%";
		element2.className = "btn btn-primary";
		element2.style.textAlign ="center";
		element2.value = nm_diskon;
		cell2.appendChild(element2);

		var cell3 = row.insertCell(2);							//cell 3
		var element3 = document.createElement("input");
		element3.type = "button";
		element3.readOnly = "true";
		element3.style.width = "100%";
		element3.className = "btn btn-primary";
		element3.style.textAlign ="center";
		element3.value = persen+' %';
		cell3.appendChild(element3);
		
		var cell3 = row.insertCell(3);							//cell 8
		var element3 = document.createElement("input");
		element3.type = "button";
		element3.className = "btn btn-danger fa fa-times";
		element3.value = "delete";
		element3.setAttribute("onClick","deleteDiskon('"+kd_diskon+''+kd_biaya+"','DiskonListTable')");
		cell3.appendChild(element3);					
	}
}
function deleteDiskon(kode,mtable){
	var table = document.getElementById(mtable);
	var rowCount = table.rows.length;	
	for(var i = 0; i < rowCount; i++){
		var row = table.rows[i];
		var rec_kd = row.cells[0].childNodes[0].value;
		if(rec_kd == (kode)){		
			table.deleteRow(i);
			rowCount--;
			i--;
		}
	}
}


//****  LIST DENDA  ****

function addDenda(mtable){	
	var table = document.getElementById(mtable);
	var dnd = $("#pilih-denda").val();
	ket_denda = $("#ket_denda").val();
	var kd_denda;
	var harga = 0;
	var status = '';
	var exist = false;
	var valid = true;
	var bulan_from;
	var tahun_from;
	var bulan_to;
	var tahun_to;
	var periode = 0;
	var total = 0;

	bln_from = parseInt($("#d_from").val());
	bln_to = parseInt($("#d_to").val());
	thn_from = parseInt($("#d_tahun_from").val());
	thn_to = parseInt($("#d_tahun_to").val());	

	if(thn_to == '' || bln_to == ''){
		bulan_from = bln_from;
		bulan_to = bln_from;
		tahun_from = thn_from;
		tahun_to = thn_from;
	}else if(thn_from > thn_to){
		valid = false;
	}else if(thn_from == thn_to){				
		if(bln_from > bln_to)
			valid = false;		
		else{
			tahun_from = thn_from;
			tahun_to = thn_to;
			bulan_from = bln_from;
			bulan_to = bln_to;
		}
	}else if(thn_from < thn_to){
		tahun_from = thn_from;
		tahun_to = thn_to;
		bulan_from = bln_from;
		bulan_to = bln_to;
	}

	var format_bulan = bulan_from+'-'+tahun_from+'|'+bulan_to+'-'+tahun_to;	
	var periode = count_periode(bulan_from,tahun_from,bulan_to,tahun_to);

	var rowCount = table.rows.length;
	if(rowCount > 4){
			exist = true;
	}else if(rowCount == 1){		
		if(dnd == 'lain'){			
			harga = $("#n_denda").val();			
			kd_denda = 'LN0';	
			status = $("#s_denda").val();
		}else{
			split_dnd = dnd.split('|');
			kd_denda = split_dnd[0];
			//ket_denda = split_dnd[1];
			harga = split_dnd[2];
			status = split_dnd[3];
		}		
	}else{		
		for(var i = 1; i < rowCount; i++){
			var row = table.rows[i];
			var rec_kd = row.cells[0].childNodes[0].value;
			var last_ln = '';

			if(dnd == 'lain'){	
				ket_denda = $("#ket_denda").val();
				harga = $("#n_denda").val();
				status = $("#s_denda").val();
				if(rec_kd.substring(0,2) == "LN"){
					last_ln = rec_kd;				
				}	
			}else{
				split_dnd = dnd.split('|');
				kd_denda = split_dnd[0];
				//ket_denda = split_dnd[1];
				harga = split_dnd[2];
				status = split_dnd[3];

				/*if(rec_kd == kd_denda){
					exist = true;
				}*/				
			}	
		}

		if(dnd == "lain"){
			if(last_ln == '')
				kd_denda = 'LN0';
			else{
				var newn = parseInt(last_ln.substring(2,10))+1;
				kd_denda = 'LN'+newn;				
			}
		}		
	}
	total = total_denda(status,harga,periode);
	
	if(exist === true || $("#pilih-denda").val() == '' || ket_denda == '' || harga <= 0 || valid == false){
	}else{		
		var row = table.insertRow(rowCount);				
		
		var kddenda = document.createElement("input");
		kddenda.type ="hidden";
		kddenda.name ="kd_denda[]";
		kddenda.value = kd_denda;
		var bulan = document.createElement("input");
		bulan.type = "hidden";
		bulan.name = "bulan_denda[]";
		bulan.value = format_bulan;
		var priode = document.createElement("input");
		priode.type = "hidden";
		priode. name = "periode_denda[]";
		priode.value = periode;
		var stat = document.createElement("input");
		stat.type = "hidden";
		stat. name = "status[]";
		stat.value = status;	
		var nmdenda = document.createElement("input");
		nmdenda.type ="hidden";
		nmdenda.name ="ket_denda[]";
		nmdenda.value = ket_denda;				
		var hrga = document.createElement("input");
		hrga.type = "hidden";
		hrga.name = "n_denda[]";
		hrga.value = harga;		
		var cell1 = row.insertCell(0);							//cell 1
		var element1 = document.createElement("input");
		element1.type = "button";
		element1.readOnly = "true";
		element1.style.width = "100%";
		element1.className = "btn btn-primary";
		element1.style.textAlign ="left";
		element1.value = ket_denda;		
		cell1.appendChild(kddenda);
		cell1.appendChild(bulan);
		cell1.appendChild(priode);
		cell1.appendChild(stat);
		cell1.appendChild(nmdenda);
		cell1.appendChild(hrga);
		cell1.appendChild(element1);

		var cell2 = row.insertCell(1);							//cell 2
		var element2 = document.createElement("input");
		element2.type = "button";
		element2.readOnly = "true";
		element2.style.width = "100%";
		element2.className = "btn btn-primary";
		element2.style.textAlign ="center";
		element2.value = status;
		cell2.appendChild(element2);

		var cell3 = row.insertCell(2);							//cell 3
		var element3 = document.createElement("input");
		element3.type = "button";
		element3.readOnly = "true";
		element3.style.width = "100%";
		element3.className = "btn btn-primary";
		element3.style.textAlign ="center";
		element3.value = format_bulan;
		cell3.appendChild(element3);

		var cell4 = row.insertCell(3);							//cell 4
		var element4 = document.createElement("input");
		element4.type = "button";
		element4.readOnly = "true";
		element4.style.width = "100%";
		element4.className = "btn btn-primary";
		element4.style.textAlign ="center";
		element4.value = periode;
		cell4.appendChild(element4);

		var cell5 = row.insertCell(4);							//cell 5
		var element5 = document.createElement("input");
		element5.type = "button";
		element5.readOnly = "true";
		element5.style.width = "100%";
		element5.className = "btn btn-primary";
		element5.style.textAlign ="center";
		element5.value = harga;
		cell5.appendChild(element5);
		
		var cell6 = row.insertCell(5);							//cell 5
		var element6 = document.createElement("input");
		element6.type = "button";
		element6.readOnly = "true";
		element6.style.width = "100%";
		element6.className = "btn btn-primary";
		element6.style.textAlign ="center";
		element6.value = total;
		cell6.appendChild(element6);
		
		var cell7 = row.insertCell(6);							//cell 8
		var element7 = document.createElement("input");
		element7.type = "button";
		element7.className = "btn btn-danger fa fa-times";
		element7.value = "delete";
		element7.setAttribute("onClick","deleteDenda('"+kd_denda+"','DendaListTable')");
		cell7.appendChild(element7);					
		$("#dTotal").html(rowCount);
	}
}
function deleteDenda(kode,mtable){
	var table = document.getElementById(mtable);
	var rowCount = table.rows.length;	
	for(var i = 0; i < rowCount; i++){
		var row = table.rows[i];
		var rec_kd = row.cells[0].childNodes[0].value;
		if(rec_kd == (kode)){		
			table.deleteRow(i);
			rowCount--;
			i--;

			document.getElementById('dTotal').innerHTML = rowCount-1;
		}
	}
}

function total_denda(status, harga, periode){
	var total = 0;

	if(status == "semester"){
		total = harga * periode;
	}else{
		for(var i=1; i<= periode; i++){
			total+=((harga*periode)-((i-1)*harga));
		}
	}

	return total;
}

function count_periode(bulan_from,tahun_from,bulan_to,tahun_to){
	var periode = 0;
	var jlh_tahun = tahun_to - tahun_from;

	if(jlh_tahun == 0){
		for (var i = bulan_from; i<= bulan_to; i++){
			periode++;
		}
	}else if(jlh_tahun > 0){
		for (var i = 0; i<=jlh_tahun;i++){
			if(i==0){
				for (var j=bulan_from; j<=12; j++)
					periode++;
			}else if(i == jlh_tahun){
				for (var j=1;j<=bln_to; j++)
					periode++;
			}else{
				periode+=12;
			}
		}
	}

	return periode;
}