<?php
class navigation{
	function show_menu(){
		$obj=&get_instance();
		$obj->load->helper('url');
		
		$menu = '<nav class="navbar navbar-inverse" role="navigation">
				  <div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse-01">
					  <span class="sr-only">Toggle navigation</span>
					</button>
					<a class="navbar-brand" href="'.base_url().'index.php/master/main">Sumatra Conservatoire <sub>master</sub></a>
				  </div>
				  <div class="collapse navbar-collapse" id="navbar-collapse-01">
					<ul class="nav navbar-nav navbar-right">           					  
					  <li class="dropdown">
						  <a class="dropdown-toggle label-success" data-toggle="dropdown">Master<span class="caret"></span></a>
						  <span class="dropdown-arrow dropdown-arrow-inverse"></span>
						  <ul class="dropdown-menu dropdown-inverse">
							<li><a href="'.base_url().'index.php/master/barang">Barang</a></li>
							<li><a href="'.base_url().'index.php/master/murid">Murid</a></li>
							<li><a href="'.base_url().'index.php/master/program">Program</a></li>
							<li><a href="'.base_url().'index.php/master/jenis">Jenis</a></li>
							<li><a href="'.base_url().'index.php/master/tingkat">Tingkat</a></li>
							<li><a href="'.base_url().'index.php/master/diskon">Diskon</a></li>
							<li><a href="'.base_url().'index.php/master/pelajaran">Pelajaran</a></li>
							<li><a href="'.base_url().'index.php/master/biaya">Biaya</a></li>
						  </ul>
					  </li>
					  <li><a href="'.base_url().'index.php/master/user" class="label-info">User</a></li>
					  <li><a href="'.base_url().'index.php/master/main/logout" class="label-danger">Keluar</a></li>
					</ul>
					
				  </div><!-- /.navbar-collapse -->
				</nav><!-- /navbar -->';				
		
		return $menu;
	}
}
?>