<!doctype html>
<html>
<head>
<?php $this->load->view('template/head_link'); ?>
</head>

<body>
<div id="add_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="MyModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
        	<form method="post" action="<?php echo base_url().'index.php/master/biaya/add';?>">
            <div class="modal-header label-default">
                <a type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</a>
                <h4 class="modal-title"><span class="fa fa-user fa-3x"></span> Biaya baru</h4>
            </div>
            <div class="modal-body">
            		<div class="row">
                      <div class="col-md-5"><label for="jenis_biaya">Jenis Biaya</label></div>
                      <div class="col-md-7">
                          <select required class="select-block jenis_biaya" name="jenis_biaya">
                          	<option value="">Pilih salah Satu</option>                          	
                            <option value="adm">Administrasi</option>
                            <option value="daftar">Pendaftaran</option>
                            <option value="test">Test</option>
                            <option value="lain">Lainnya</option>
                          </select>
                      </div>
                    </div>
                	<!--<div class="row">
                      <div class="col-md-5"><label for="kd_biaya">Kode biaya</label></div>
                      <div class="col-md-7">
                          <input autofocus type="text" class="form-control" name="kd_biaya" required data-toggle='tooltip'>
                      </div>
                    </div>-->
                    <div class="row">
                      <div class="col-md-5"><label for="nm_biaya">Nama biaya</label></div>
                      <div class="col-md-7">
                          <input type="text" required class="form-control" name="nm_biaya">                          
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-5"><label for="kd_jenis">Jenis Pelajaran</label></div>
                      <div class="col-md-7">
                          <select class="select-block kd_jenis" name="kd_jenis">
                          	<option></option>
                            <?php 
							foreach($pelajaran->result_array() as $row){							
								print"<option value='$row[kd_jenis]'>$row[nm_jenis]</option>";								
							}?>
                          </select>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-5"><label for="kd_program">Program</label></div>
                      <div class="col-md-7">
                          <select class="select-block kd_program" name="kd_program">
                          	<option></option>
                            <?php foreach($program->result_array() as $row){
								print"<option value='$row[kd_program]'>$row[nm_program]</option>";
							}?>
                          </select>
                      </div>
                    </div>                    
                    <div class="row">
                      <div class="col-md-5"><label for="biaya">Biaya</label></div>
                      <div class="col-md-7">
                          <input autofocus type="number" required class="form-control" name="biaya">
                      </div>
                    </div>          
            </div>
            <div class="modal-footer">
            	<input type="submit" class="btn btn-success" value="Simpan">
                <input type="reset" class="btn btn-danger" data-dismiss="modal" value="Batal">
            </div>
            </form>
        </div>
    </div>
</div>
<div id="edit_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="MyModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">          
        </div>
    </div>
</div>

<div id="wrap">
	<?php echo $navigation; ?>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h1>Biaya <small>master biaya</small></h1>
                <ol class="breadcrumb">
                	<li><a href="<?php echo base_url().'index.php/master/main';?>">Beranda</a></li>
                    <li class="active">Biaya</li>
                </ol>             
            </div>
        </div>
        <div class="row">
            <?php
                if(validation_errors()){                        
                    echo validation_errors('<div class="alert alert-danger">Save Failed!<br>', '</div>');
                }
                if(isset($m_success)){
                    echo "<div class='alert alert-success'>$m_success</div>";
                }
            ?>            
        </div>
        <a href="#add_modal" style="margin-bottom: 20px;" id="masukkan" data-toggle="modal" class="btn btn-embossed btn-info btn-lg btn-block"><i class="fa fa-plus fa-1x"></i> Masukkan Data Baru <i class="fa fa-download fa-1x"></i></a>
        <?php $this->load->view('master/biaya_view'); ?>  
    </div>
</div>

<div id="footer">
	<div class="container">
    	<p class="credit">Copyright&copy;2014.MasterPanel by ITS STTC.All right Reserved</p>
    </div>
</div>

<?php $this->load->view('template/javascript_link'); ?>
<script language="javascript">
$(document).ready(function() {
    $("select").selectpicker({style: 'btn btn-primary', menuStyle: 'dropdown-inverse'});
	var jenis_biaya = ["adm","daftar","test"];
	var jb = $(".jenis_biaya");
	var kj = $("div.kd_jenis");
	var kpr = $("div.kd_program");
	
	if(jb.val() == jenis_biaya[0] || jb.val() == jenis_biaya[1]){
			kj.hide();
			kpr.show();
	}else if(jb.val() == jenis_biaya[2]){
			kj.show();
			kpr.hide();
	}else{
			kj.hide();
			kpr.hide();
	}
	
	jb.on("change",function(){
		if(jb.val() == jenis_biaya[0] || jb.val() == jenis_biaya[1]){
				kj.hide();
				kpr.show();
				kj.val();
		}else if(jb.val() == jenis_biaya[2]){
				kj.show();
				kpr.hide();
		}else{
				kj.hide();
				kpr.hide();
		}
	});

  $(".modal").on('shown.bs.modal',function(){
      $(this).find("[autofocus]:first").focus();
  });
  $("body").on('hidden.bs.modal','.modal',function(){
        $(this).removeData('bs.modal');
    });
    var oTable = $("#DataTable").DataTable({          
          "bProcessing" : true,
          "bServerSide" : true,          
          "sAjaxSource" : '<?php echo base_url();?>index.php/master/biaya/biaya_datatable',          
          "sPaginationType": "full_numbers",
          'fnServerData' : function(sSource, aoData, fnCallback){
            $.ajax({
              'dataType' : 'json',
              'type' : 'POST',
              'url' : sSource,
              'data' : aoData,
              'success' : fnCallback
            });
          },
          "oLanguage": {
            "sSearch": "Search all columns:"
                },          
          "bStateSave" : true,
          'iCookieDuration':60*60,          
          "columns" : [                         
                        {"data" : "jenis_biaya"},
                        {"data" : "kd_pelajaran"},
                        {"data" : "nm_biaya"},
                        {"data" : "kd_program"},
                        {"data" : "kd_tingkat"},
                        {"data" : "kd_jenis"},
                        {"data" : "biaya"},
                        {"bSearchable":false, "data" : "edit"}
                      ],
          /*"fnInitComplete" : function(oSettings, json){
            var table = $('#DataTable').DataTable();
              $("#DataTable tfoot th:first-child").each( function(i){
                var select = $('<select><option value="")Filter on: </option></select>').appendTo( this ).on('change', function(){
                  table.column( i ).search( $(this).val()).draw();
                });

                table.column( i ).data().unique().sort().each( function( d, j){
                  select.append('<option value="'+d+'">'+d+'</option>')
                });
              });
          }*/
          
    });
});
</script>

</body>
</html>