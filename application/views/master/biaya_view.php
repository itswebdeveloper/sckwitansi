      <div class="row tile">
        <i class="label label-info">USS = Uang Sekolah Standard</i>
        <i class="label label-info">USK = Uang Sekolah Khusus</i>
        <i class="label label-info">Sem1 = Uang Ujian Semester 1</i>
        <i class="label label-info">Sem2 = Uang Ujian Semester 2</i>
        <i class="label label-info">ADM = Uang Administrasi</i>
        <i class="label label-info">Daftar = Uang Pendaftaran</i>
        <i class="label label-info">Test = Uang Testing</i>
          <table id="DataTable" class="display compact table table-striped table-responsive cell-border table-hovered">
            <thead class="label-default">
              <tr>                
                <th>Jenis Biaya</th>
                <th>Kode Pelajaran</th>
                <th>Nama Biaya</th>
                <th>Kode Program</th>
                <th>Kode Tingkat</th>
                <th>Kode Jenis</th>
                <th>Biaya</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td colspan="4" class="dataTables_empty"> Loading Biaya</td>
              </tr>
            </tbody>
            <tfoot>
              <th>Jenis Biaya</th>
              <th>Kode Pelajaran</th>
              <th>Nama Biaya</th>
              <th>Kode Program</th>
              <th>Kode Tingkat</th>
              <th>Kode Jenis</th>
              <th>Biaya</th>
            </tfoot>
          </table>
        </div>