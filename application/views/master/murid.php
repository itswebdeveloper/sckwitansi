<!doctype html>
<html>
<head>
<?php $this->load->view('template/head_link'); ?>
</head>

<body>
<div id="add_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="MyModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
        	<form method="post" action="<?php echo base_url().'index.php/master/murid/add';?>">
            <div class="modal-header label-default">
                <a type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</a>
                <h4 class="modal-title"><span class="fa fa-user fa-3x"></span> Murid baru</h4>
            </div>
            <div class="modal-body">
            	<div class="row">
                  <div class="col-md-5"><label for="nim">NIM</label></div>
                  <div class="col-md-7">
                      <input autofocus type="text" class="form-control" maxlength="10" name="nim" required data-toggle='tooltip'>                          
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-5"><label for="nm_murid">Nama murid</label></div>
                  <div class="col-md-7">
                      <input type="text" required maxlength="50" class="form-control" name="nm_murid">                          
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-5"><label for="ket">Keterangan</label></div>
                  <div class="col-md-7">
                      <textarea maxlength="100" class="form-control" name="ket"></textarea>
                  </div>
                </div>
            </div>
            <div class="modal-footer">
            	<input type="submit" class="btn btn-success" value="Simpan">
                <input type="reset" class="btn btn-danger" data-dismiss="modal" value="Batal">
            </div>
            </form>
        </div>
    </div>
</div>

<div id="wrap">
	<?php echo $navigation; ?>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h1>Murid <small>master murid</small></h1>
                <ol class="breadcrumb">
                	<li><a href="<?php echo base_url().'index.php/master/main';?>">Beranda</a></li>
                    <li class="active">Murid</li>
                </ol>             
            </div>
        </div>
        <div class="row">
            <div class="col-xs-6">
                <div class="tile">
                    <img class="tile-image" alt="Clipboard" src="<?php echo base_url().'assets/';?>images/icons/svg/clipboard.svg"></img>
                    <h2 class="tile-title">Lihat dan Atur Murid</h2>
                    <p>Menampilkan semua murid yang sudah didaftarkan</p>                                          
                    <div class="btn-group btn-group-lg btn-group-justify btn-group-block">
                        <a href="<?php echo base_url().'index.php/master/murid/view';?>" class="btn btn-info btn-large">Lihat Murid Terdaftar</a>
                        <a href="<?php echo base_url().'index.php/master/murid/murid_baru';?>" class="btn btn-inverse btn-large">Murid Baru 
                        <?php (isset($murid_baru) && $murid_baru > 0) ? print"<span class='notif-new'>$murid_baru</span>" : ""; ?>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-xs-6">
                <div class="tile">
                    <img class="tile-image" alt="Pensils" src="<?php echo base_url().'assets/';?>images/icons/svg/pencils.svg"></img>
                    <h2 class="tile-title">Menambah Murid</h2>
                    <p>Memasukkan data murid terbaru</p>
                    <a href="#add_modal" data-toggle="modal" class="btn btn-success btn-large btn-block">Masukkan</a>
                    <?php
					if(validation_errors()){						
						echo validation_errors('<div class="alert alert-danger">Save Failed!<br>', '</div>');
					}
					if(isset($m_success)){
						echo "<div class='alert alert-success'>$m_success</div>";
					}
					if(isset($m_fail)){
						echo "<div class='alert alert-danger'>$m_fail</div>";
					}
					?>
                </div>
            </div>
        </div>    
    </div>
</div>

<div id="footer">
	<div class="container">
    	<p class="credit">Copyright&copy;2014.MasterPanel by ITS STTC.All right Reserved</p>
    </div>
</div>

<?php $this->load->view('template/javascript_link'); ?>
<script type="text/javascript">
$(document).ready(function(){    
    $(".modal").on('shown.bs.modal',function(){
        $(this).find("[autofocus]:first").focus();
    })
})
</script>

</body>
</html>