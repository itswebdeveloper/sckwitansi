<!doctype html>
<html>
<head>
<?php $this->load->view('template/head_link'); ?>
<style type="text/css">
  .REQBATAL{
    background: #F1C40F;
    color: #000;
  }
  .BATAL{
    background: #EC7063;
    color: #000;
  }
</style>
</head>

<body>
<div id="edit_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="MyModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">            
        </div>
    </div>
</div>

<div id="wrap">
	<?php echo $navigation; ?>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h1>Kwitansi <small>master kwitansi</small></h1>
                <ol class="breadcrumb">
                	<li><a href="<?php echo base_url().'index.php/master/main';?>">Beranda</a></li>
                    <li class="active">Kwitansi</li>
                </ol>             
            </div>
        </div>
        <div class="row">
            <?php
                if(validation_errors()){                        
                    echo validation_errors('<div class="alert alert-danger">Save Failed!<br>', '</div>');
                }
                if(isset($m_success)){
                    echo "<div class='alert alert-success'>$m_success</div>";
                }
            ?>                      
        </div>        
        <div class="row tile">
          <span class="label label-primary"><i class="fa fa-info pull-left"></i>Ketik REQBATAL pada kolom pencarian untuk menampilkan pemberitahuan pembatalan oleh kasir.</span>
          <table id="DataTable" class="display compact table table-striped table-responsive cell-border table-hovered">
            <thead class="label-default">
              <tr>
                <th>No Kwitansi</th>
                <th>Tanggal</th>
                <th>NIM</th>
                <th>Nama</th>
                <th>Total</th>
                <th>Status</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td colspan="4" class="dataTables_empty"> Loading Kwitansi</td>
              </tr>
            </tbody>
            <tfoot>
              <th>No Kwitansi</th>
              <th>Tanggal</th>
              <th>NIM</th>
              <th>Nama</th>
              <th>Total</th>
              <th>Status</th>
            </tfoot>
          </table>
        </div>
    </div>
</div>

<div id="footer">
	<div class="container">
    	<p class="credit">Copyright&copy;2014.MasterPanel by ITS STTC.All right Reserved</p>
    </div>
</div>

<?php $this->load->view('template/javascript_link'); ?>
<script type="text/javascript">
$(document).ready(function(){    
    $(".modal").on('shown.bs.modal',function(){
        $(this).find("[autofocus]:first").focus();
    });
    $("body").on('hidden.bs.modal','.modal',function(){
        $(this).removeData('bs.modal');
    });
    var oTable = $("#DataTable").DataTable({          
          "bProcessing" : true,
          "bServerSide" : true,          
          "sAjaxSource" : '<?php echo $ajax_source; ?>',          
          "sPaginationType": "full_numbers",
          'fnServerData' : function(sSource, aoData, fnCallback){
            $.ajax({
              'dataType' : 'json',
              'type' : 'POST',
              'url' : sSource,
              'data' : aoData,
              'success' : fnCallback
            });
          },
          "oLanguage": {
            "sSearch": "Search all columns:"
                },          
          "bStateSave" : true,
          'iCookieDuration':60*60,
          "columns" : [
                        {"data" : "no_kwitansi"},
                        {"data" : "tgl_kwitansi"},
                        {"data" : "nim"},
                        {"data" : "nama"},
                        {"data" : "total"},
                        {"data" : "status"}                        
                      ]
          
    });
    
})
</script>

</body>
</html>