<!doctype html>
<html>
<head>
<?php $this->load->view('template/head_link'); ?>
</head>

<body>
<div id="wrap">
	<?php echo $navigation; ?>
    <div class="container">
        <?php
            if($this->session->flashdata('Upgrade_message'))
                echo "<div class='alert alert-warning alert-dismissable'><i class='fa fa-times close'></i><i class='fa fa-info fa-3x pull-right'></i>".$this->session->flashdata('Upgrade_message')."</div>";
        ?>
        <div class="row">
            <div class="col-lg-12">
                <h1>Beranda <small>master menu</small></h1>                
                <ol class="breadcrumb">
                    <li class="active">Beranda</li>
                </ol>
                <?php                    
                    if($table_repair == TRUE)
                        print "<a href='".base_url()."index.php/db_migrate/upgrade_database' class='btn btn-block btn-danger btn-lg'><i class='fa fa-gears fa-2x'></i>Database need to repair. Click here to start module.</a>";                    
                ?>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-6">
                <div class="tile">
                    <i class="tile-image fa fa-barcode fa-5x"></i>                    
                    <h2 class="tile-title">Kwitansi Barang</h2>
                    <p>Pengaturan Kwitansi Barang</p>
                    <a href="<?php echo base_url().'index.php/master/kwitansi/barang';?>" class="btn btn-inverse btn-large btn-block"><i class="fa fa-2x fa-angle-double-left"></i><i class="fa fa-2x fa-gear"></i></a>
                </div>
            </div>
            <div class="col-xs-6">
                <div class="tile">
                    <i class="tile-image fa fa-paperclip fa-5x"></i>                    
                    <h2 class="tile-title">Kwitansi Pelajaran</h2>
                    <p>Pengaturan Kwitansi Pelajaran</p>
                    <a href="<?php echo base_url().'index.php/master/kwitansi/pelajaran';?>" class="btn btn-inverse btn-large btn-block"><i class="fa fa-2x fa-gear"></i><i class="fa fa-2x fa-angle-double-right"></i></a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">                
                <ol class="breadcrumb label-primary">
                	<li class="active">DATA MASTER</li>
                </ol>                
            </div>
        </div>
        <div class="row">
            <div class="col-xs-4">
                <div class="tile">
                    <img class="tile-image" alt="Pensils" src="<?php echo base_url().'assets/';?>images/icons/svg/paper-bag.svg"></img>
                    <h2 class="tile-title">Master Barang</h2>
                    <p>Pengaturan Barang</p>
                    <a href="<?php echo base_url().'index.php/master/barang';?>" class="btn btn-danger btn-large btn-block"><i class="fa fa-2x fa-gear"></i></a>
                </div>
            </div>
            <div class="col-xs-4">
                <div class="tile">
                    <?php ($murid_baru > 0 && $table_repair == FALSE) ? print"<a href='#'><span class='notif-new'>$murid_baru</span></a>" : ""; ?>  
                    <img class="tile-image" alt="Pensils" src="<?php echo base_url().'assets/';?>images/icons/svg/pencils.svg"></img>
                    <h2 class="tile-title">Master Murid</h2>
                    <p>Pengaturan Data Murid</p>
                    <a href="<?php echo base_url().'index.php/master/murid';?>" class="btn btn-danger btn-large btn-block"><i class="fa fa-2x fa-gear"></i></a>
                </div>
            </div>
            <div class="col-xs-4">
                <div class="tile">
                    <img class="tile-image" alt="Pensils" src="<?php echo base_url().'assets/';?>images/icons/svg/book.svg"></img>
                    <h2 class="tile-title">Master Pelajaran</h2>
                    <p>Pengaturan Pelajaran</p>
                    <a href="<?php echo base_url().'index.php/master/pelajaran';?>" class="btn btn-danger btn-large btn-block"><i class="fa fa-2x fa-gear"></i></a>
                </div>
            </div>            
        </div>
        <div class="row" style="margin-top:30px">
        	<div class="col-xs-3">
                <div class="tile">
                    <img class="tile-image" alt="Pensils" src="<?php echo base_url().'assets/';?>images/icons/svg/clipboard.svg"></img>
                    <h2 class="tile-title">Master Program</h2>
                    <p>Pengaturan Program Pelajaran</p>
                    <a href="<?php echo base_url().'index.php/master/program';?>" class="btn btn-success btn-large btn-block"><i class="fa fa-2x fa-gear"></i></a>
                </div>
            </div>
            <div class="col-xs-3">
                <div class="tile">
                    <img class="tile-image" alt="Pensils" src="<?php echo base_url().'assets/';?>images/icons/svg/clipboard.svg"></img>
                    <h2 class="tile-title">Master Subprogram</h2>
                    <p>Pengaturan Subprogram</p>
                    <a href="<?php echo base_url().'index.php/master/subprogram';?>" class="btn btn-success btn-large btn-block"><i class="fa fa-2x fa-gear"></i></a>
                </div>
            </div>
            <div class="col-xs-3">
                <div class="tile">
                    <img class="tile-image" alt="Pensils" src="<?php echo base_url().'assets/';?>images/icons/svg/clipboard.svg"></img>
                    <h2 class="tile-title">Master Jenis</h2>
                    <p>Pengaturan Jenis Pelajaran</p>
                    <a href="<?php echo base_url().'index.php/master/jenis';?>" class="btn btn-success btn-large btn-block"><i class="fa fa-2x fa-gear"></i></a>
                </div>
            </div>
            <div class="col-xs-3">
                <div class="tile">
                    <img class="tile-image" alt="Pensils" src="<?php echo base_url().'assets/';?>images/icons/svg/map.svg"></img>
                    <h2 class="tile-title">Master Tingkat</h2>
                    <p>Pengaturan Tingkat Kelas</p>
                    <a href="<?php echo base_url().'index.php/master/tingkat';?>" class="btn btn-success btn-large btn-block"><i class="fa fa-2x fa-gear"></i></a>
                </div>
            </div>
        </div>
        <div class="row" style="margin-top:30px">                        
            <div class="col-xs-4">
                <div class="tile">
                    <img class="tile-image" alt="Pensils" src="<?php echo base_url().'assets/';?>images/icons/svg/gift-box.svg"></img>
                    <h2 class="tile-title">Master Diskon</h2>
                    <p>Pengaturan Diskon</p>
                    <a href="<?php echo base_url().'index.php/master/diskon';?>" class="btn btn-primary btn-large btn-block"><i class="fa fa-2x fa-gear"></i></a>
                </div>
            </div>
            <div class="col-xs-4">
                <div class="tile">
                    <img class="tile-image" alt="Pensils" src="<?php echo base_url().'assets/';?>images/icons/svg/clocks.svg"></img>
                    <h2 class="tile-title">Master Denda</h2>
                    <p>Pengaturan Denda</p>
                    <a href="<?php echo base_url().'index.php/master/denda';?>" class="btn btn-primary btn-large btn-block"><i class="fa fa-2x fa-gear"></i></a>
                </div>
            </div>
            <div class="col-xs-4">
                <div class="tile">
                    <img class="tile-image" alt="Pensils" src="<?php echo base_url().'assets/';?>images/icons/svg/mail.svg"></img>
                    <h2 class="tile-title">Master Biaya</h2>
                    <p>Pengaturan Biaya</p>
                    <a href="<?php echo base_url().'index.php/master/biaya';?>" class="btn btn-primary btn-large btn-block"><i class="fa fa-2x fa-gear"></i></a>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="footer">
	<div class="container">
    	<p class="credit">Copyright&copy;2014.MasterPanel by ITS STTC.All right Reserved</p>
    </div>
</div>

<?php $this->load->view('template/javascript_link'); ?>

</body>
</html>