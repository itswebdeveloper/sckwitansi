<!doctype html>
<html>
<head>
<?php $this->load->view('template/head_link'); ?>
</head>

<body>
<div id="edit_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="MyModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
        	<form method="post" action="<?php echo base_url().'index.php/master/murid/edit';?>">
            <div class="modal-header label-default">
                <a type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</a>
                <h4 class="modal-title"><span class="fa fa-user fa-3x"></span> Edit Murid</h4>
            </div>
            <div class="modal-body">
                	            
            </div>
            <div class="modal-footer">
            	<input type="submit" class="btn btn-success" value="Simpan">
                <input type="reset" class="btn btn-danger" data-dismiss="modal" value="Batal">
            </div>
            </form>
        </div>
    </div>
</div>

<div id="wrap">
	<?php echo $navigation; ?>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h1>Murid <small>view murid</small></h1>
                <ol class="breadcrumb">
                	<li><a href="<?php echo base_url().'index.php/master/main';?>">Beranda</a></li>
                    <li><a href="<?php echo base_url().'index.php/master/murid';?>">Murid</a></li>
                    <li class="active">View</li>
                </ol>
            </div>
        </div>
        <div class="row">
        	<?php
			if(validation_errors()){						
				echo validation_errors('<div class="alert alert-danger">Save Failed!<br>', '</div>');
			}
			if(isset($m_success)){
				echo "<div class='alert alert-success'>$m_success</div>";
			}
			?>            
              <table id="DataTable" class="display compact table table-striped table-responsive cell-border table-hovered">
                <thead class="label-default">
                  <tr>
                    <th>NIM</th>
                    <th>Nama Murid</th>
                    <th>Keterangan</th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td colspan="4" class="dataTables_empty"> Loading Barang</td>
                  </tr>
                </tbody>
                <tfoot>
                  <th>Kode Barang</th>
                  <th>Nama Barang</th>
                  <th>Harga</th>
                </tfoot>
              </table>        
             <!-- <table id="tb_murid" class="table table-responsive table-bordered table-hover">
             	<thead>
                	<tr>
                    	<th>NIM</th><th>Nama Murid</th><th>Keterangan</th>
                    </tr>
                </thead>
                <tbody>
                	<?php foreach($murid_list->result_array() as $row){
						print"<tr>
							<td>$row[nim]</td>
							<td>$row[nm_murid]</td>
							<td>$row[ket]</td>
							<td width=14%><a id='$row[nim]' href='#edit_modal' data-toggle='modal' class='tb-edit btn btn-hg btn-info btn-embossed'><i class='fa fa-edit'></i></a>
							<a href='".base_url()."index.php/master/murid/delete/$row[nim]' class='btn btn-danger btn-hg btn-embossed'><i class='fa fa-trash-o'></i></a></td>
						</tr>";
					}
					?>
                </tbody>
             </table> -->
        </div>    
    </div>
</div>

<div id="footer">
	<div class="container">
    	<p class="credit">Copyright&copy;2014.MasterPanel by ITS STTC.All right Reserved</p>
    </div>
</div>

<?php $this->load->view('template/javascript_link'); ?>
<script language="javascript">
	$(document).ready(function() {  
        $("body").on('hidden.bs.modal','.modal',function(){
            $(this).removeData('bs.modal');
        });      
        var oTable = $("#DataTable").DataTable({          
          "bProcessing" : true,
          "bServerSide" : true,          
          "sAjaxSource" : '<?php echo base_url();?>index.php/master/murid/murid_datatable',          
          "sPaginationType": "full_numbers",
          'fnServerData' : function(sSource, aoData, fnCallback){
            $.ajax({
              'dataType' : 'json',
              'type' : 'POST',
              'url' : sSource,
              'data' : aoData,
              'success' : fnCallback
            });
          },
          "oLanguage": {
            "sSearch": "Search all columns:"
                },          
          "bStateSave" : true,
          "columns" : [
                        {"data" : "nim"},
                        {"data" : "nm_murid"},
                        {"data" : "ket"},
                        {"bSearchable":false, "data" : "edit"}
                      ]
          
        });
    });
	
</script>
</body>
</html>