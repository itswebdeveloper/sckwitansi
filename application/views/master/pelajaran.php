<!doctype html>
<html>
<head>
<?php $this->load->view('template/head_link'); ?>
</head>

<body>
<div id="add_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="MyModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
        	<form method="post" action="<?php echo base_url().'index.php/master/pelajaran/add';?>">
            <div class="modal-header label-default">
                <a type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</a>
                <h4 class="modal-title"><span class="fa fa-user fa-3x"></span> Pelajaran baru</h4>
            </div>
            <div class="modal-body">                  
                	  <!-- <div class="row">
                      <div class="col-md-5"><label for="kd_pelajaran">Kode</label></div>
                      <div class="col-md-7">
                          <input autofocus type="text" class="form-control" name="kd_pelajaran" required data-toggle='tooltip' title="Harus unik. Mengandung kombinasi huruf atau angka. Maksimal 10 karakter.">                          
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-5"><label for="nm_pelajaran">Nama</label></div>
                      <div class="col-md-7">
                          <input type="text" required class="form-control" name="nm_pelajaran">                          
                      </div>
                    </div> -->

                    <div class="row">
                      <div class="col-md-5"><label for="kd_program">Program</label></div>
                      <div class="col-md-7 kd_program">
                          <select name="kd_program" id="kd_program" class="select-block" required>
                            <option class="empty"></option>
                          	<?php
								foreach($program->result_array() as $row){
									print"<option value='$row[kd_program]'>$row[kd_program] - $row[nm_program]</option>";
								}
							?>
                          </select>
                      </div>
                    </div>
                    
                    <div class="row">
                      <div class="col-md-5"><label for="kd_program">Sub Program</label></div>
                      <div class="col-md-7 kd_subprogram">
                          <select name="kd_subprogram" id="kd_subprogram" class="select-block" required>
                            <option class="empty"></option>
                          	<?php
								foreach($subprogram->result_array() as $row){
                  $class = "";
                  /*foreach($filter_program as $key => $val){
                    	foreach ($val as $key2 => $val2) {
                        if($key2 == $row['kd_subprogram'])
                          $class.="pr-$key ";
                      }
                  }*/
                  print"<option class='$class subprogram' value='$row[kd_subprogram]'>$row[kd_subprogram] - $row[nm_subprogram]</option>";
								}
							?>
                          </select>
                      </div>
                    </div>
                    
                    <div class="row">
                      <div class="col-md-5"><label for="kd_jenis">Jenis Pelajaran</label></div>
                      <div class="col-md-7 kd_jenis">
                          <select name="kd_jenis" id="kd_jenis" class="select-block" required>
                          <option class="empty"></option>
                          	<?php
								foreach($jenis->result_array() as $row){
                  $class = "";
                  /*foreach($filter_program as $key => $val){
                      foreach ($val as $key2 => $val2) {
                        foreach ($val2 as $key3 => $val3) {
                          if($key3 == $row['kd_jenis'])
                            $class.="pr-$key-spr-$key2 "; 
                        }                        
                      }
                  }*/
									print"<option class='$class jenis' value='$row[kd_jenis]'>$row[kd_jenis] - $row[nm_jenis]</option>";
								}
							?>
                          </select>
                      </div>
                    </div>  

                    <div class="row">
                      <div class="col-md-5"><label for="kd_tingkat">Kode Tingkat</label></div>
                      <div class="col-md-7 kd_tingkat">
                          <select name="kd_tingkat" id="kd_tingkat" class="select-block" required>
                          <option class="empty"></option>
                          	<?php
								foreach($tingkat->result_array() as $row){
                  $class = "";
                  /*foreach($filter_program as $key => $val){
                      foreach ($val as $key2 => $val2) {
                        foreach ($val2 as $key3 => $val3) {
                          foreach ($val3 as $key4) {
                            if($key4 == $row['kd_tingkat'])
                              $class.="pr-$key-spr-$key2-jn-$key3 "; 
                          }
                          
                        }                        
                      }
                  }*/
									print"<option class='$class tingkat' value='$row[kd_tingkat]'>$row[kd_tingkat] - $row[nm_tingkat]</option>";
								}
							?>
                          </select>
                      </div>
					</div>                          
                    
                    <div class="row">
                      <div class="col-md-5"><label for="usekolah_standar">Uang Sekolah</label></div>
                      <div class="col-md-7">
                      <input type="number" required placeholder="standar" class="form-control" name="usekolah_standar">
                      <input type="number" placeholder="khusus" class="form-control" name="usekolah_khusus">                          
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-5"><label for="uujian_ganjil">Uang Ujian</label></div>
                      <div class="col-md-7">
                          <input type="number" placeholder="ganjil" class="form-control" name="uujian_ganjil">                      
                          <input type="number" placeholder="genap" class="form-control" name="uujian_genap">                          
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-5"><label for="ket">Keterangan</label></div>
                      <div class="col-md-7">
                          <input type="text" name="ket" id="ket" class="form-control">                                                                
                      </div>
                    </div>
            </div>
            <div class="modal-footer">
            	<input type="submit" class="btn btn-success" value="Simpan">
                <input type="reset" class="btn btn-danger" data-dismiss="modal" value="Batal">
            </div>
            </form>
        </div>
    </div>
</div>
<div id="edit_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="MyModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
          <form method="post" action="<?php echo base_url().'index.php/master/pelajaran/edit';?>">
            <div class="modal-header label-default">
                <a type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</a>
                <h4 class="modal-title"><span class="fa fa-user fa-3x"></span> Edit Pelajaran</h4>
            </div>
            <div class="modal-body">
                <!-- content here -->                
             </div>            
            <div class="modal-footer">
              <input type="submit" class="btn btn-success" value="Simpan">
                <input type="reset" class="btn btn-danger" data-dismiss="modal" value="Batal">
            </div>
            </form>
        </div>
    </div>
</div>

<div id="wrap">
	<?php echo $navigation; ?>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h1>Pelajaran <small>master pelajaran</small></h1>
                <ol class="breadcrumb">
                	<li><a href="<?php echo base_url().'index.php/master/main';?>">Beranda</a></li>
                    <li class="active">Pelajaran</li>
                </ol>             
            </div>
        </div>   
        <div class="row">
            <?php
                if(validation_errors()){                        
                    echo validation_errors('<div class="alert alert-danger">Save Failed!<br>', '</div>');
                }
                if(isset($m_success)){
                    echo "<div class='alert alert-success'>$m_success</div>";
                }
            ?>             
        </div>
          <a href="#add_modal" style="margin-bottom: 20px;" id="masukkan" data-toggle="modal" class="btn btn-embossed btn-info btn-lg btn-block"><i class="fa fa-plus fa-1x"></i> Masukkan Data Baru <i class="fa fa-download fa-1x"></i></a>
          <?php $this->load->view('master/pelajaran_view'); ?>      
    </div>
</div>

<div id="footer">
	<div class="container">
    	<p class="credit">Copyright&copy;2014.MasterPanel by ITS STTC.All right Reserved</p>
    </div>
</div>

<?php $this->load->view('template/javascript_link'); ?>
<script>
$(document).ready(function(){
  $(".modal").on('shown.bs.modal',function(){
      $(this).find("[autofocus]:first").focus();
  });
  $("body").on('hidden.bs.modal','.modal',function(){
        $(this).removeData('bs.modal');
    });
    var oTable = $("#DataTable").DataTable({          
          "bProcessing" : true,
          "bServerSide" : true,          
          "sAjaxSource" : '<?php echo base_url();?>index.php/master/pelajaran/pelajaran_datatable',          
          "sPaginationType": "full_numbers",
          'fnServerData' : function(sSource, aoData, fnCallback){
            $.ajax({
              'dataType' : 'json',
              'type' : 'POST',
              'url' : sSource,
              'data' : aoData,
              'success' : fnCallback
            });
          },
          "oLanguage": {
            "sSearch": "Search all columns:"
                },          
          "bStateSave" : true,
          'iCookieDuration':60*60,
          "columns" : [
                        {"data" : "kd_pelajaran"},
                        {"data" : "nm_pelajaran"},
                        {"data" : "ket"},
                        {"bSearchable":false, "data" : "edit"}
                      ]
          
    });

	$("select").selectpicker({style: 'btn btn-primary', menuStyle: 'dropdown-inverse'});
  $('#kd_subprogram, #kd_tingkat, #kd_jenis').hide();
  $('#kd_program').val('');

  $(".kd_program .filter-option").text('Nothing selected');
  $(".kd_program ul li").removeClass('selected');
  $(".kd_program ul li:first-child").addClass('selected');

  <?php
    $php_array = $filter_program;
    $js_array = json_encode($php_array);
    echo "var p_array = ".$js_array.";\n";
  ?>

  $('#kd_program').on('change',function(){
    $('#kd_subprogram.dropdown-toggle').show();
    /*var program = $("#kd_program").val();
    $('#kd_subprogram option').hide();
    $('.kd_subprogram .dropdown-menu a').hide();
    $('.pr-'+program).show();*/

    $('#kd_tingkat, #kd_jenis').hide();
    $('#kd_subprogram').val('');
    $(".kd_subprogram .filter-option").text('Nothing selected');
    $(".kd_subprogram ul li").removeClass('selected');
    $(".kd_subprogram ul li:first-child").addClass('selected');
  })

  $('#kd_subprogram').on('change',function(){
    $('#kd_jenis.dropdown-toggle').show();
    /*var program = $("#kd_program").val();
    var subprogram = $("#kd_subprogram").val();
    $('#kd_jenis option').hide();
    $('.kd_jenis .dropdown-menu a').hide();
    $('.pr-'+program+'-spr-'+subprogram).show();*/

    $('#kd_jenis').hide();
    $('#kd_jenis').val('');
    $(".kd_jenis .filter-option").text('Nothing selected');
    $(".kd_jenis ul li").removeClass('selected');
    $(".kd_jenis ul li:first-child").addClass('selected');
  })
  
  $('#kd_jenis').on('change',function(){
    $('#kd_tingkat.dropdown-toggle').show();
    /*var program = $("#kd_program").val();
    var subprogram = $("#kd_subprogram").val();
    var jenis = $("#kd_jenis").val();    
    $('#kd_tingkat option').hide();
    $('.kd_tingkat .dropdown-menu a').hide();
    $('.pr-'+program+'-spr-'+subprogram+'-jn-'+jenis).show();*/

    $('#kd_tingkat').val('');
    $(".kd_tingkat .filter-option").text('Nothing selected');
    $(".kd_tingkat ul li").removeClass('selected');
    $(".kd_tingkat ul li:first-child").addClass('selected');
  })

});
</script>

</body>
</html>