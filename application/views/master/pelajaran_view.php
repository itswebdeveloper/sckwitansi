        <div class="row tile">
             <table id="DataTable" class="display compact table table-striped table-responsive cell-border table-hovered">
            <thead class="label-default">
              <tr>
                <th>Kode Pelajaran</th>
                <th>Nama Pelajaran</th>
                <th>Keterangan</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td colspan="4" class="dataTables_empty"> Loading Pelajaran</td>
              </tr>
            </tbody>
            <tfoot>
              <th>Kode Pelajaran</th>
                <th>Nama Pelajaran</th>
                <th>Keterangan</th>
                <th></th>
            </tfoot>
          </table>
        </div>