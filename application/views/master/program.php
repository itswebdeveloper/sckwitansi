<!doctype html>
<html>
<head>
<?php $this->load->view('template/head_link'); ?>
</head>

<body>
<div id="add_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="MyModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
        	<form method="post" action="<?php echo base_url().'index.php/master/program/add';?>">
            <div class="modal-header label-default">
                <a type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</a>
                <h4 class="modal-title"><span class="fa fa-user fa-3x"></span> Program baru</h4>
            </div>
            <div class="modal-body">
                	<div class="row">
                      <div class="col-md-5"><label for="kd_program">Kode program</label></div>
                      <div class="col-md-7">
                          <input autofocus type="text" class="form-control" name="kd_program" required data-toggle='tooltip'>                          
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-5"><label for="nm_program">Nama program</label></div>
                      <div class="col-md-7">
                          <input type="text" required class="form-control" name="nm_program">                          
                      </div>
                    </div>                    
            </div>
            <div class="modal-footer">
            	<input type="submit" class="btn btn-success" value="Simpan">
                <input type="reset" class="btn btn-danger" data-dismiss="modal" value="Batal">
            </div>
            </form>
        </div>
    </div>
</div>
<div id="edit_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="MyModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form method="post" action="<?php echo base_url().'index.php/master/program/edit';?>">
            <div class="modal-header label-default">
                <a type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</a>
                <h4 class="modal-title"><span class="fa fa-user fa-3x"></span> Edit Program</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                  <div class="col-md-5"><label for="kd_program">Kode program</label></div>
                  <div class="col-md-7">
                      <input autofocus type="text" disabled class="form-control disabled" name="kd_program">                          
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-5"><label for="nm_program">Nama program</label></div>
                  <div class="col-md-7">
                      <input type="text" required class="form-control" name="nm_program">
                  </div>
                </div>
            </div>
            <div class="modal-footer">
                <input type="submit" class="btn btn-success" value="Simpan">
                <input type="reset" class="btn btn-danger" data-dismiss="modal" value="Batal">
            </div>
            </form>
        </div>
    </div>
</div>

<div id="wrap">
	<?php echo $navigation; ?>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h1>Program <small>master program</small></h1>
                <ol class="breadcrumb">
                	<li><a href="<?php echo base_url().'index.php/master/main';?>">Beranda</a></li>
                    <li class="active">Program</li>
                </ol>             
            </div>
        </div>         
        <div class="row">
            <?php
                if(validation_errors()){                        
                    echo validation_errors('<div class="alert alert-danger">Save Failed!<br>', '</div>');
                }
                if(isset($m_success)){
                    echo "<div class='alert alert-success'>$m_success</div>";
                }
            ?>             
        </div>
        <a href="#add_modal" style="margin-bottom: 20px;" id="masukkan" data-toggle="modal" class="btn btn-embossed btn-info btn-lg btn-block"><i class="fa fa-plus fa-1x"></i> Masukkan Data Baru <i class="fa fa-download fa-1x"></i></a>
        <?php $this->load->view('master/program_view'); ?>     
    </div>
</div>

<div id="footer">
	<div class="container">
    	<p class="credit">Copyright&copy;2014.MasterPanel by ITS STTC.All right Reserved</p>
    </div>
</div>

<?php $this->load->view('template/javascript_link'); ?>
<script type="text/javascript">
$(document).ready(function(){    
    $(".modal").on('shown.bs.modal',function(){
        $(this).find("[autofocus]:first").focus();
    });
    $("body").on('hidden.bs.modal','.modal',function(){
        $(this).removeData('bs.modal');
    });
    var oTable = $("#DataTable").DataTable({          
          "bProcessing" : true,
          "bServerSide" : true,          
          "sAjaxSource" : '<?php echo base_url();?>index.php/master/program/program_datatable',          
          "sPaginationType": "full_numbers",
          'fnServerData' : function(sSource, aoData, fnCallback){
            $.ajax({
              'dataType' : 'json',
              'type' : 'POST',
              'url' : sSource,
              'data' : aoData,
              'success' : fnCallback
            });
          },
          "oLanguage": {
            "sSearch": "Search all columns:"
                },          
          "bStateSave" : true,
          'iCookieDuration':60*60,
          "columns" : [
                        {"data" : "kd_program"},
                        {"data" : "nm_program"},                        
                        {"bSearchable":false, "data" : "edit"}
                      ]
          
    });
})
</script>
</body>
</html>