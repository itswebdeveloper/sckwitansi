<div class="row tile">
          <table id="DataTable" class="display compact table table-striped table-responsive cell-border table-hovered">
            <thead class="label-default">
              <tr>
                <th>Kode Diskon</th>
                <th>Nama Diskon</th>
                <th>Persen Diskon</th>
                <th>Keterangan</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td colspan="4" class="dataTables_empty"> Loading Diskon</td>
              </tr>
            </tbody>
            <tfoot>
                <th>Kode Diskon</th>
                <th>Nama Diskon</th>
                <th>Persen Diskon</th>
                <th>Keterangan</th>
            </tfoot>
          </table>
        </div>