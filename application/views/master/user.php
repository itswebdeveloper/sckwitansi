<!doctype html>
<html>
<head>
<?php $this->load->view('template/head_link'); ?>
</head>

<body>
<div id="edit_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="MyModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
        	<form method="post" action="<?php echo base_url().'index.php/master/user/edit';?>">
            <div class="modal-header label-default">
                <a type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</a>
                <h4 class="modal-title"><span class="fa fa-user fa-3x"></span> Edit Login</h4>
            </div>
            <div class="modal-body">
                	<div class="row">
                      <div class="col-md-5"><label for="kd_barang">Kode barang</label></div>
                      <div class="col-md-7">
                          <input autofocus type="text" class="form-control" name="kd_barang" required data-toggle='tooltip' title="Harus unik. Mengandung kombinasi huruf atau angka. Maksimal 10 karakter.">                          
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-5"><label for="nm_barang">Nama barang</label></div>
                      <div class="col-md-7">
                          <input type="text" required class="form-control" name="nm_barang">                          
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-5"><label for="harga">Harga</label></div>
                      <div class="col-md-7">
                          <input autofocus type="number" required class="form-control" name="harga">
                      </div>
                    </div>                
            </div>
            <div class="modal-footer">
            	<input type="submit" class="btn btn-success" value="Simpan">
                <input type="reset" class="btn btn-danger" data-dismiss="modal" value="Batal">
            </div>
            </form>
        </div>
    </div>
</div>

<div id="wrap">
	<?php echo $navigation; ?>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h1>User <small>pengaturan User</small></h1>
                <ol class="breadcrumb">
                	<li><a href="<?php echo base_url().'index.php/master/main';?>">Beranda</a></li>
                    <li class="active">User</li>
                </ol>             
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 well">
            <?php
            if(validation_errors()){                        
                echo validation_errors('<div class="alert alert-danger">Save Failed!<br>', '</div>');
            }
            if(isset($m_success)){
                echo "<div class='alert alert-success'>$m_success</div>";
            }
            if(isset($m_failed)){
                echo "<div class='alert alert-danger'>$m_failed</div>";
            }
            ?>
                <table class="table">
                    <tr>
                        <th class="info">MASTER</th><td>:</td><td><?php echo $admin['name'];?> <a id="admin" href="#edit_modal" data-toggle="modal" class="btn btn-primary btn-sm">Edit</a></td>
                        <td><button class='btn btn-sm btn-info'>Give direct access to USER panel. Can't cross access to MASTER panel.</button></td>
                    </tr>
                    <tr>
                        <th class="success">USER</th><td>:</td><td><?php echo $user['name'];?> <a id="user" href="#edit_modal" data-toggle="modal" class="btn btn-primary btn-sm">Edit</a></td>
                        <td><button class='btn btn-sm btn-info'>Give direct access to MASTER panel. Can't cross access to USER panel.</button></td>
                    </tr>
                </table>
            </div>
        </div>    
    </div>
</div>

<div id="footer">
	<div class="container">
    	<p class="credit">Copyright&copy;2014.MasterPanel by ITS STTC.All right Reserved</p>
    </div>
</div>

<?php $this->load->view('template/javascript_link'); ?>
<script language="javascript">
    $(document).ready(function() {
        $("#admin").click(function(){          
          $('#edit_modal .modal-body').load('<?php echo base_url().'index.php/master/user/disp_login/admin';?>');
        });
        $("#user").click(function(){          
          $('#edit_modal .modal-body').load('<?php echo base_url().'index.php/master/user/disp_login/user';?>');
        });
    });
</script>

</body>
</html>