        <div class="row tile">
          <table id="DataTable" class="display compact table table-striped table-responsive cell-border table-hovered">
            <thead class="label-default">
              <tr>
                <th>Kode Subprogram</th>
                <th>Nama Subprogram</th>                
                <th></th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td colspan="4" class="dataTables_empty"> Loading Subprogram</td>
              </tr>
            </tbody>
            <tfoot>
              <th>Kode Subprogram</th>
              <th>Nama Subprogram</th>              
            </tfoot>
          </table>
        </div>