<!doctype html>
<html>
<head>
<?php $this->load->view('template/head_link'); ?>
</head>

<body>
<div id="edit_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="MyModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
        	<form method="post" action="<?php echo base_url().'index.php/master/murid/move_murid_baru';?>">
            <div class="modal-header label-default">
                <a type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</a>
                <h4 class="modal-title"><span class="fa fa-user fa-3x"></span> Pemindahan data Murid Baru</h4>
            </div>
            <div class="modal-body">
                	            
            </div>
            <div class="modal-footer">
            	<input type="submit" class="btn btn-success" value="Pindahkan">
                <input type="reset" class="btn btn-danger" data-dismiss="modal" value="Batal">
            </div>
            </form>
        </div>
    </div>
</div>
<div id="delete_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="MyModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">            
            <div class="modal-header label-danger">
                <a type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</a>
                <h4 class="modal-title"><span class="fa fa-user fa-3x"></span> Hapus Murid Baru</h4>
            </div>
            <div class="modal-body">
                <h5>Anda Yakin akan menghapus data murid Baru?       </h5>
            </div>
            <div class="modal-footer">
                <a class="btn btn-danger delete" href='".base_url()."index.php/master/murid/delete/$row[nim]'><i class='fa fa-times'></i> Hapus</a>
                <a class="btn btn-inverse" data-dismiss="modal">Batal</a>
            </div>
            </form>
        </div>
    </div>
</div>

<div id="wrap">
	<?php echo $navigation; ?>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h1>Murid <small>view murid</small></h1>
                <ol class="breadcrumb">
                	<li><a href="<?php echo base_url().'index.php/master/main';?>">Beranda</a></li>
                    <li><a href="<?php echo base_url().'index.php/master/murid';?>">Murid</a></li>
                    <li class="active">View</li>
                </ol>
            </div>
        </div>
        <div class="row">
        	<?php
			if(validation_errors()){						
				echo validation_errors('<div class="alert alert-danger">Save Failed!<br>', '</div>');
			}
			echo (isset($m_success)) ? "<div class='alert alert-success'>$m_success</div>" : "";
            echo (isset($m_failed)) ? "<div class='alert alert-danger'>$m_failed</div>" : "";            			
			?>
             <table id="tb_murid" class="table table-responsive table-bordered table-hover">
             	<thead>
                	<tr>
                    	<th>NIM</th><th>Nama Murid</th><th>Keterangan</th>
                    </tr>
                </thead>
                <tbody>
                	<?php foreach($murid_baru->result_array() as $row){
						print"<tr>
							<td>$row[nim]</td>
							<td>$row[nm_murid]</td>
							<td>$row[ket]</td>
							<td width=14%>
                                <div class='btn-group'>
                                    <a id='$row[nim]' href='#edit_modal' data-toggle='modal' class='tb-edit btn btn-hg btn-info btn-embossed'><i class='fa fa-book'></i></a>
        							<a id='$row[nim]' href='#delete_modal' data-toggle='modal' class='tb-delete btn btn-danger btn-hg btn-embossed'><i class='fa fa-trash-o'></i></a>
                                </div>
                            </td>                            
						</tr>";
					}
					?>
                </tbody>
             </table>
        </div>    
    </div>
</div>

<div id="footer">
	<div class="container">
    	<p class="credit">Copyright&copy;2014.MasterPanel by ITS STTC.All right Reserved</p>
    </div>
</div>

<?php $this->load->view('template/javascript_link'); ?>
<script language="javascript">
	$(document).ready(function() {
        $(".tb-edit").click(function(){
		  var id = $(this).attr('id');          
		  $('#edit_modal .modal-body').load('<?php echo base_url().'index.php/master/murid/disp_murid_baru/';?>'+id);
		  $("#pilih-diskon").selectpicker({style: 'btn btn-primary', menuStyle: 'dropdown-inverse'});
		});

        $(".tb-delete").click(function(){
          var id = $(this).attr('id');
          $('#delete_modal .delete').attr('href','<?php echo base_url().'index.php/master/murid/delete_murid_baru/';?>'+id);
        });
    });
	
</script>
</body>
</html>