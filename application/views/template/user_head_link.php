
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE-Edge">
<meta name="viewport" content="width=device-width">

<title><?php echo $title; ?></title>
	<!--<link rel="icon" href="<?php echo $icon; ?>">-->
	
    <!-- Loading Bootstrap -->
    <link href="<?php echo base_url(); ?>assets/bootstrap/css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/bootstrap/css/prettify.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/font-awesome/css/font-awesome.min.css" rel="stylesheet">

    <!-- Loading Flat UI -->
    <link href="<?php echo base_url(); ?>assets/css/flat-ui.css" rel="stylesheet">
    
    <!-- Custom css -->
    <link href="<?php echo base_url(); ?>assets/css/user_custom.css" rel="stylesheet">
                
    <!--[if Lt IE 9]>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/respond.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/html5shiv.js"></script>
    <![endif]-->