<!doctype html>
<html>
<head>
<?php $this->load->view('template/user_head_link'); ?>
</head>

<body>
<div id="wrap">
	<?php echo $navigation; ?>
    <div class="container">
       <div class="row">
            <div class="col-lg-12">
                <h1>Barang <small>Tambah Pembelian</small></h1>
                <ol class="breadcrumb">
                	<li><a href="<?php echo base_url().'index.php/user/main';?>">Beranda</a></li>
                    <li class="active">Barang</li>
                </ol>             
            </div>
        </div>
        <div class="row">
        	<div class="well">            	
            	<table class="table table-responsive table-bordered">
                	<tr>
                    	<td class="info" width="20%">Telah diterima dari : </td>
                        <td> <?php if($nim!=NULL){echo $nim.' | '.$nama;}else{echo $nama;} ?></td>
                        <td class="info" width="5%">Tgl.</td><td width="20%"> <?php echo $tanggal; ?></td> 
                    </tr>
                    <tr>
                    	<td colspan="4" class="info">Pembelian:</td>
                    </tr>
                    <tr>
                    	<td colspan="4">
                        	<table class="table table-responsive table-bordered">
                            	<thead>
                                	<tr class="success">
                                    	<th width="3%">No.</th>
                                        <th>Uraian</th>
                                        <th width="15%">Harga</th>
                                        <th width="5%">Qty.</th>
                                        <th width="25%">Sub Total</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php
									$i=1;
                                	foreach($uraian as $row){
										print"
											<tr>
												<td>$i</td>
												<td>$row[nm_barang]</td>
												<td>@".number_format($row['harga'])."</td>
												<td>$row[quantity]</td>
												<td><i class='pull-left'>Rp.</i><i class='pull-right'>".number_format($row['stotal'])."</i></td>
											</tr>
										";
										$i++;
									}
								?>
                                </tbody>
                                <tfoot>
                                	<tr>
                                    	<th colspan="3"><i class='pull-right'>TOTAL</i></th>
                                        <td colspan="2">
										<?php echo "<i class='pull-left'>Rp.</i><i class='pull-right'>
										".number_format($total)."
										</i>";?>
                                        </td>
                                    </tr>
                                </tfoot>
                            </table>
                        </td>
                    </tr>
                    <tr>
                    	<td>Terbilang :</td>
                        <th colspan="3"><?php echo $terbilang.' rupiah'; ?></th>
                    </tr>
                </table>
                <a class="btn btn-danger btn-hg btn-embossed" href="<?php echo base_url().'index.php/user/barang';?>">Batal</a>                
                <a class="btn btn-success btn-hg btn-embossed pull-right" href="<?php echo base_url().'index.php/user/barang/save';?>">
                Simpan</a>
            </div>
        </div>
    </div>
</div>

<div id="footer">
	<div class="container">
    	<p class="credit">Copyright&copy;2014.MasterPanel by ITS STTC.All right Reserved</p>
    </div>
</div>

<?php $this->load->view('template/javascript_link'); ?>
<script language="javascript">
$(document).ready(function() {
});
</script>

</body>
</html>