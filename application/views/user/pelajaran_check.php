<!doctype html>
<html>
<head>
<?php $this->load->view('template/user_head_link'); ?>
</head>

<body>
<div id="wrap">
	<?php echo $navigation; ?>
    <div class="container">
       <div class="row">
            <div class="col-lg-12">
                <h1>Pelajaran <small>Tambah Pembayaran</small></h1>
                <ol class="breadcrumb">
                	<li><a href="<?php echo base_url().'index.php/user/main';?>">Beranda</a></li>
                    <li class="active">Pelajaran</li>
                </ol>             
            </div>
        </div>
        <div class="row">
        	<div class="well">            	
            	<table class="table table-responsive table-bordered">
                	<tr>
                    	<td class="info" width="16%">Telah diterima dari : </td>
                        <td> <?php if($nim!=NULL){echo $nim.' | '.$nama;}else{echo $nama;} ?></td>
                        <td class="info" width="5%">Tgl.</td><td width="20%"> <?php echo $tanggal; ?></td> 
                    </tr>
                    <tr>
                    	<td colspan="4" class="info">Pembelian:</td>
                    </tr>
                    <tr>
                    	<td colspan="4">
                        	<table class="table table-responsive table-bordered">
                            	<thead>
                                	<tr class="success">
                                    	<th width="3%">No.</th>
                                        <th>Pembayaran</th>
                                        <th width="5%">Prog.</th>
                                        <th width="5%">S.Prog.</th>
                                        <th width="5%">Ting.</th>
                                        <th width="15%">Bulan</th>
                                        <th width="5%">Per.</th>
                                        <th width="15%">@</th>
                                        <th width="15%">Jumlah</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php
									$i=1;
                                	foreach($uraian as $row){
										print"
											<tr>
												<td>$i</td>
												<td>$row[nm_biaya]</td>
												<td>$row[kd_program]</td>
                                                <td>$row[kd_subprogram]</td>
												<td>$row[kd_tingkat]</td>
                                                <td>";                                                
                                                $mth = array('','Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec');                                                
                                                if($row['periode'] > 1){
                                                    $split_bln = explode(',', $row['bulan']);
                                                    echo $mth[$split_bln[0]].' - '.$mth[$split_bln[($row['periode'])-1]].' '.$row['tahun'];
                                                }else{
                                                    if($row['bulan']!='')
                                                        echo $mth[$row['bulan']].' '.$row['tahun'];
                                                }
                                            
                                        print"  </td>
                                                <td>$row[periode]</td>
                                                <td><i class='pull-left'>Rp.</i><i class='pull-right'>".number_format($row['biaya'])."</i></td>
												<td><i class='pull-left'>Rp.</i><i class='pull-right'>".number_format($row['periode']*$row['biaya'])."</i></td>
											</tr>
										";
										$i++;
									}
								?>
                                </tbody>
                                <tfoot>
                                	<tr>
                                    	<th colspan="8"><i class='pull-right'>Sub Total</i></th>
                                        <td>
										<?php echo "<i class='pull-left'>Rp.</i><i class='pull-right'>
										".number_format($stotal)."
										</i>";?>
                                        </td>
                                    </tr>                                    
                                    <tr>
                                        <th colspan="8"><i class='pull-right'>Nilai Diskon</i></th>
                                        <td>
                                        <?php echo "<i class='pull-left'>Rp.</i><i class='pull-right'>
                                        ".number_format($nilai_diskon)."
                                        </i>";?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th colspan="8"><i class='pull-right'>Nilai Denda</i></th>
                                        <td>
                                        <?php echo "<i class='pull-left'>Rp.</i><i class='pull-right'>
                                        ".number_format($nilai_denda)."
                                        </i>";?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th colspan="8"><i class='pull-right'>Kurang / Lebih Bayar</i></th>
                                        <td>
                                        <?php 
                                        echo "<i class='pull-left'>Rp.</i><i class='pull-right'>";
                                        if($lk_status == "lebih")
                                            echo "- ".number_format($lebih_bayar);
                                        else
                                            echo "+ ".number_format($kurang_bayar);
                                        echo "</i>"
                                        ;?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th colspan="8"><i class='pull-right'>Total</i></th>
                                        <td>
                                        <?php echo "<i class='pull-left'>Rp.</i><i class='pull-right'>
                                        ".number_format($total)."
                                        </i>";?>
                                        </td>
                                    </tr>
                                </tfoot>
                            </table>
                            <table class="table">
                                <tr><th colspan="4">Keterangan</th></tr>
                                <tr>                                    
                                    <td colspan="4"><?php echo $keterangan ?></td>
                                </tr>
                                <tr><td>Diskon : </td><td>
                                <?php
                                    $temp = '';
                                    foreach ($diskon as $row) {

                                        if($temp != '' && $row['kd_biaya'] != $temp)
                                            print " , ";

                                        if($row['kd_biaya'] != $temp){
                                            print "($row[kd_pembayaran]) $row[ket_diskon] $row[persen_diskon]%";
                                        }


                                        if($row['kd_biaya'] != $temp){
                                            //print "$row[persen_diskon]%";
                                        }else{
                                            print "+ $row[ket_diskon] $row[persen_diskon]%";
                                        }                                        

                                        $temp = $row['kd_biaya'];                                        
                                     } 
                                ?>
                                </td>
                                </tr>
                                <tr><td>Denda : </td><td>
                                <?php                                    
                                    $temp = '';
                                    foreach ($denda as $row) {
                                        /*if($temp != '' && $row['kd_denda'] != $temp)
                                            print " , ";*/

                                        print" ($row[status])$row[keterangan] ($row[periode] periode=$row[total]) ,"; 

                                        //$temp = $row['kd_denda'];
                                     } 
                                ?>
                                </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                    	<td>Terbilang :</td>
                        <th colspan="3"><?php echo $terbilang." rupiah"; ?></th>
                    </tr>
                </table>
                <a class="btn btn-danger btn-hg btn-embossed" href="<?php echo base_url().'index.php/user/pelajaran';?>">Batal</a>                
                <a class="btn btn-success btn-hg btn-embossed pull-right" href="<?php echo base_url().'index.php/user/pelajaran/save';?>">
                Simpan</a>
            </div>
        </div>
    </div>
</div>

<div id="footer">
	<div class="container">
    	<p class="credit">Copyright&copy;2014.MasterPanel by ITS STTC.All right Reserved</p>
    </div>
</div>

<?php $this->load->view('template/javascript_link'); ?>
<script language="javascript">
$(document).ready(function() {
});
</script>

</body>
</html>