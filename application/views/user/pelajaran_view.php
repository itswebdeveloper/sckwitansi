<!doctype html>
<html>
<head>
<?php $this->load->view('template/user_head_link'); ?>
</head>

<body>
<div id="wrap">
	<?php echo $navigation; ?>
    <div class="container">
       <div class="row">
            <div class="col-lg-12">
                <h1>Pelajaran <small>Kwitansi pelajaran</small></h1>
                <ol class="breadcrumb">
                	<li><a href="<?php echo base_url().'index.php/user/main';?>">Beranda</a></li>
                    <li class="active">Kwitansi Pelajaran</li>
                </ol>             
            </div>
        </div>
        <div class="row">
        	<div class="well">
            	<table class="table label-success table-bordered table-responsive table-hover">
					<thead>
                    	<tr>
                        	<th width="15%">No Kwitansi</th><th width="20%">Tanggal</th><th width="35%">Murid</th><th width="30%">Total</th><th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
						<?php foreach($kwitansi->result_array() as $row){
                            if (substr($row['nim'], 0,2) == "N_") {
                                $murid_baru = $this->master_model->view_new_murid($row['nim']);
                                $nm_murid = strtoupper($murid_baru['nm_murid']);
                            }else{
                                $murid = $this->master_model->get_murid($row['nim']);
                                $nm_murid = strtoupper($murid['nm_murid']);
                            }
                            if($row['status'] == 'REQBATAL')
                                print "<tr class='warning'>";
                            elseif($row['status'] == 'BATAL')
                                print "<tr class='danger'>";

    						print"	
                                <td>
    								<a class='btn btn-info' target='_blank' href='".base_url()."index.php/user/pdfprint/pelajaran/?nomor=$row[no_kwitansi]'>$row[no_kwitansi]</a>
    							</td>
    							<td>
    								".date('d M Y',strtotime($row['tgl_kwitansi']))."
    							</td>
    							<td>
    								".$row['nim'].'|'.$nm_murid."
    							</td>
    							<td>
    								Rp. ".number_format($row['total'])."
    							</td>
                                <td>";
                            if($row['status'] == 'REQBATAL' || $row['status'] == 'BATAL')
                            print"<a class='btn disabled' href='#'>BATAL</a>";
                            else
                            print"<a class='btn btn-danger' href='".base_url()."index.php/user/pelajaran/batal/?no=$row[no_kwitansi]'>BATAL</a>";
                            print"
                                </td>
							</tr>                            
							";
                        }
                        ?>
                	</tbody>
                </table>
                <?php echo $pagination; ?>
            </div>
        </div>
    </div>
</div>

<div id="footer">
	<div class="container">
    	<p class="credit">Copyright&copy;2014.MasterPanel by ITS STTC.All right Reserved</p>
    </div>
</div>

<?php $this->load->view('template/javascript_link'); ?>
<script language="javascript">
$(document).ready(function() {
	
});
</script>

</body>
</html>