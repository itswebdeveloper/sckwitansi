<!doctype html>
<html>
<head>
<?php $this->load->view('template/user_head_link'); ?>
<link href="<?php echo base_url(); ?>assets/css/jquery-ui/jquery-ui.css" rel="stylesheet">
	<style type="text/css">
		.ui-autocomplete{
			max-height: 300px;
			overflow: auto;
		}
	</style>
</head>

<body>
<div id="diskon_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="MyModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
        	<div class="modal-header label-default">
                <a type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</a>
                <h4 class="modal-title"><span class="fa fa-user fa-3x"></span> Menambah Diskon Pembayaran</h4>
            </div>
            <div class="modal-body">
            	<div class="row">
            		<div class="col-md-6">
            			Kode Biaya
            		</div>
            		<div class="col-md-6">
            			<input type="button" id="d_kd_biaya" readonly class="btn btn-block btn-info">
            			<input type='hidden' id="d_kd_pembayaran">
            		</div>            		
            	</div>
            	<div class="row">
            		<div class="col-md-6">
            			Nama Biaya
            		</div>
            		<div class="col-md-6">
            			<input type="button" id="d_nm_pembayaran" readonly class="btn btn-block btn-info">
            		</div>            		
            	</div>
	        	<div class="row">
	        		<div class="col-md-6">
	        			Pilih Diskon
	        		</div>
					<div class="col-md-6">
	                  <select class="select-block" id="pilih-diskon" name="pilih-diskon">
	                  	<option value=""></option>
	                  	<?php
	                  		foreach ($diskon->result_array() as $row) {
	                  			print"
	                  				<option 
	                  				value='$row[kd_diskon]|$row[nm_diskon]|$row[persen]'>
	                  					$row[nm_diskon] - $row[persen]%
	                  				</option>
	                  			";
	                  		}
	                  	?>
	                  	<option value="lain">Lainnya..</option>
	                  </select>                          
	                </div>
	            </div>
	            <div class="row">	            	
	            	<div class="col-md-6">
	            		Keterangan Diskon
	            	</div>
	                <div class="col-md-6">
	                	<input type="text" id="ket_diskon" class="hide-diskon form-control" placeholder='masukkan keterangan diskon'>
	                </div>
	            </div>
	           	<div class="row">
	           		<div class="col-md-6">
	            		Persen Diskon
	            	</div>
	                <div class="col-md-6">
	                	<input type="text" id="p_diskon" maxlength="3" class="hide-diskon form-control" value="0" placeholder='%'>
	                </div>
	            </div>	           		                                           	                			
			</div>
			<div class="modal-footer">
				<a class="btn btn-primary" name="add" onClick="addDiskon('DiskonListTable')">
				  Add	                      
				</a>
                <input type="button" class="btn btn-default" data-dismiss="modal" value="Tutup">
            </div>
        </div>
    </div>
</div>

<div id="wrap">
	<?php echo $navigation; ?>
    <div class="container">
       <div class="row">
            <div class="col-lg-12">
                <h1>Pelajaran <small>Tambah Pembayaran</small></h1>
                <ol class="breadcrumb">
                	<li><a href="<?php echo base_url().'index.php/user/main';?>">Beranda</a></li>
                    <li class="active">Pelajaran</li>
                </ol>             
            </div>
        </div>
        <div class="row">
        	<?php 
        	($this->session->flashdata('message')) ? print"<div class='alert alert-success'>".$this->session->flashdata('message')."</div>" : NULL;
        	(validation_errors()) ? print "<div class='alert alert-warning'>".validation_errors()."</div>" : NULL; ?>
        	<div class="">
            	<form name='frpelajaran' method="post" action="<?php echo base_url().'index.php/user/pelajaran/add';?>">                	
                    <div class="row">
                    	<div class="panel panel-primary">
                    		<div class="panel-heading">
                    			<h3 class="panel-title">
                    				Identitas
                    			</h3>
                    		</div>
                    		<div class="panel-body">
                    			<div class="col-md-3">
		                        	<label class="label label-info" for="nim">NIM</label><br>
		                            <input name="nim" class="form-control autocomplete-f-nim" type="text">
		                            <input type="hidden" class="autocomplete-f-nim-id autocomplete-f-nama-id">
		                        </div>
		                        <div class="col-md-3">
		                            <label class="label label-info" for="nama">Nama</label><br>
		                        	<input class="form-control nama autocomplete-f-nama" type="text" name="nama">
		                        </div>
                    		</div>
                    	</div>                    	                        
                    </div>
                    <div class="row">
                    	<div class="panel panel-primary">
                    		<div class="panel-heading">
                    			<h3 class="panel-title">
                    				Pembayaran
                    			</h3>
                    		</div>
                    		<div class="panel-body">
                    			<div class="row">
	                    			<div class="col-md-2">
			                          <select class="select-block pilih-biaya" id="pilih-biaya" name="pilih-biaya">
			                          	<option value="USS">USS</option>
			                          	<option value="USK">USK</option>
			                          	<option value="Sem1">UU Semester 1</option>
			                          	<option value="Sem2">UU Semester 2</option>
			                          	<option value="adm">Administrasi</option>
			                          	<option value="daftar">Pendaftaran</option>
			                          	<option value="test">Testing</option>
			                          </select>
			                        </div>
			                        <div class="col-md-3">
			                          <select class="form-control pilih-detail-biaya" id="pilih-detail-biaya" name="pilih-detail-biaya">
			                          	<option value=""></option>
			                          	<?php
			                          		$rec = '';
			                          		foreach ($biaya->result_array() as $row) {
			                          			if($rec != $row['kd_program'] && $rec != ''){
			                          				print "</optgroup> ";
			                          			}
			                          			if($rec != $row['kd_program']){
			                          				print "<optgroup label = '$row[kd_program]'> ";
			                          			}

			                          			$split = explode('-', $row['kd_pelajaran']);

			                          			print"
			                          				<option class='opt-$row[jenis_biaya]' 
			                          				value='$row[kd_biaya]|$row[nm_biaya]|$row[biaya]-$row[biaya_lama]|$row[kd_program]|$row[kd_tingkat]|$split[1]'>
			                          					$row[nm_biaya] $split[1]&nbsp;&nbsp; $row[kd_tingkat]
			                          				</option>
			                          			";

			                          			$rec = $row['kd_program'];
			                          		}
			                          	?>
			                          	</optgroup>			                          	
			                          </select>                          
			                        </div>
			                        <div class="col-md-7 periode">
			                        	<div class="col-md-3">
				                        	<select class="select-block" id="from" name="from">
				                        		<option value="1">Januari</option><option value="2">Pebruari</option>
				                        		<option value="3">Maret</option><option value="4">April</option>
				                        		<option value="5">Mei</option><option value="6">Juni</option>
				                        		<option value="7">Juli</option><option value="8">Agustus</option>
				                        		<option value="9">September</option><option value="10">Oktober</option>
				                        		<option value="11">Nopember</option><option value="12">Desember</option>
				                        	</select>
				                        </div>
				                        <div class="col-md-2">
				                        	<select class="select-block" id="tahun_from" name="tahun_from">
				                        		<?php
				                        			for($i=round(date('Y'))+1;$i >= round(date('Y'))-1; $i--)
				                        				if($i == round(date('Y')))
				                        					print "<option value='$i' selected>$i</option>";
				                        				else
				                        					print "<option value='$i'>$i</option>";
				                        		?>
				                        	</select>
				                        </div>
				                        <div class="col-md-1">
				                        	<i class='fa fa-minus'></i>
				                        </div>
				                        <div class="col-md-3">
				                        	<select class="select-block" id="to" name="to">
				                        		
				                        		<option value="1">Januari</option><option value="2">Pebruari</option>
				                        		<option value="3">Maret</option><option value="4">April</option>
				                        		<option value="5">Mei</option><option value="6">Juni</option>
				                        		<option value="7">Juli</option><option value="8">Agustus</option>
				                        		<option value="9">September</option><option value="10">Oktober</option>
				                        		<option value="11">Nopember</option><option value="12">Desember</option>
				                        	</select>
				                        </div>
			                        	<div class="col-md-2">
				                        	<select class="select-block" id="tahun_to" name="tahun">
				                        		
				                        		<?php
				                        			for($i=(round(date('Y'))+1);$i >= round(date('Y'))-1; $i--){
				                        				if($i == round(date('Y')))
				                        					print "<option value='$i' selected>$i</option>";
				                        				else
				                        					print "<option value='$i'>$i</option>";
				                        			}
				                        		?>
				                        	</select>
				                        </div>
			                        </div>			                             
			                    </div>
			                    <div class="row">
			                    	<div class="col-md-1">
			                          <a class="btn btn-primary" name="add" onClick="addPembayaran('ListTable')">
			                              Add
			                              <span class="badge" id="pTotal">0</span>
			                          </a>                         
			                        </div>
			                        <div class="col-md-5">
			                          <span class="btn btn-primary" id="total">
			                              Total :                    
			                          </span>                         
			                        </div>
			                    </div>               
			                    <div class="row">
			                    	<div class="col-md-12">
			                            <table id="ListTable" class="table table-bordered table-hover">
			    							<tr>
			    								<th style="text-align: center" width="25%">Pembayaran</th>
			    								<th style="text-align: center" width="5%">Pro.</th>
			    								<th style="text-align: center" width="5%">S.Pro.</th>
			    								<th style="text-align: center" width="5%">Ting.</th>
			    								<th style="text-align: center" width="10%">Bulan</th>
			    								<th style="text-align: center" width="5%">Per.</th>
			    								<th style="text-align: center" width="15%">Biaya</th>
			    								<th style="text-align: center" width="15%">Jumlah</th>
			    								<th style="text-align: center" width="3%"></th>
			    							</tr>
			                            </table>
			                        </div>
			                    </div>
                    		</div>
                    	</div>
                    </div>
                    <div class="row">
                    	<div class="panel panel-success">
                    		<div class="panel-heading">
                    			<h3 class="panel-title">
                    				Diskon
                    			</h3>
                    		</div>
                    		<div class="panel-body">
                    			
                    			<div class="row">
			                    	<div class="col-md-12">
			                            <table id="DiskonListTable" class="table table-bordered table-hover">
			    							<tr>			    								
			    								<th style="text-align: center">Pembayaran</th>
			    								<th style="text-align: center">Nama Diskon</th>
			    								<th style="text-align: center">Persen</th>
			    								<th style="text-align: center" width="3%"></th>
			    							</tr>
			                            </table>
			                        </div>
			                    </div>
                    		</div>
                    	</div>                    	                        
                    </div>
                    <div class="row">
                    	<div class="panel panel-danger">
                    		<div class="panel-heading">
                    			<h3 class="panel-title">
                    				Denda
                    			</h3>
                    		</div>
                    		<div class="panel-body">
                    			<div class="row">
					        		<div class="col-md-5">
					                  <select class="select-block" id="pilih-denda" name="pilih-denda">
					                  	<option value=""></option>
					                  	<?php
					                  		foreach ($denda->result_array() as $row) {
					                  			print"
					                  				<option 
					                  				value='$row[kd_denda]|$row[nm_denda]|$row[harga]|$row[status]'>
					                  					$row[nm_denda] - Rp. $row[harga] ,-
					                  				</option>
					                  			";
					                  		}
					                  	?>
					                  	<option value="lain">Lainnya..</option>
					                  </select>					                                            
					                </div>
					                <div class="col-md-7">
				                  	 	<input type="text" id="ket_denda" class="form-control" placeholder='masukkan keterangan denda'>
				                  	</div>			            					                			                    					            
					            </div>
					            <div class="row">
					            	<div class="col-md-7">
			                        	<div class="col-md-3">
				                        	<select class="select-block" id="d_from" name="d_from">
				                        		<option value="1">Januari</option><option value="2">Pebruari</option>
				                        		<option value="3">Maret</option><option value="4">April</option>
				                        		<option value="5">Mei</option><option value="6">Juni</option>
				                        		<option value="7">Juli</option><option value="8">Agustus</option>
				                        		<option value="9">September</option><option value="10">Oktober</option>
				                        		<option value="11">Nopember</option><option value="12">Desember</option>
				                        	</select>
				                        </div>
				                        <div class="col-md-2">
				                        	<select class="select-block" id="d_tahun_from" name="d_tahun_from">
				                        		<?php
				                        			for($i=round(date('Y'));$i >= 2012; $i--)
				                        				print"<option value='$i'>$i</option>";
				                        		?>
				                        	</select>
				                        </div>
				                        <div class="col-md-1">
				                        	<i class='fa fa-minus'></i>
				                        </div>
				                        <div class="col-md-3">
				                        	<select class="select-block" id="d_to" name="d_to">
				                        		
				                        		<option value="1">Januari</option><option value="2">Pebruari</option>
				                        		<option value="3">Maret</option><option value="4">April</option>
				                        		<option value="5">Mei</option><option value="6">Juni</option>
				                        		<option value="7">Juli</option><option value="8">Agustus</option>
				                        		<option value="9">September</option><option value="10">Oktober</option>
				                        		<option value="11">Nopember</option><option value="12">Desember</option>
				                        	</select>
				                        </div>
			                        	<div class="col-md-2">
				                        	<select class="select-block" id="d_tahun_to" name="d_tahun_to">
				                        		
				                        		<?php
				                        			for($i=round(date('Y'));$i >= 2012; $i--)
				                        				print"<option value='$i'>$i</option>";
				                        		?>
				                        	</select>
				                        </div>
			                        </div>
			                        <div class="col-md-5">
					                	<div class="col-md-5">
						                	<input type="text" id="n_denda" placeholder='nominal' maxlength="9" class="hide-denda form-control" value="0">
						                </div>
						                <div class="col-md-7">
				                        	<select class="select-block" id="s_denda" name="s_denda">
				                        		<option value='bulanan'>Bulanan</option>
				                        		<option value='semester'>Semester</option>
				                        	</select>
				                        </div>
					                </div>
					            </div>
					            <div class="row">
					            	<div class="col-md-1">
					            		<a class="btn btn-success" name="add" onClick="addDenda('DendaListTable')">
										  <i class= 'fa fa-plus'></i> ADD <span class="badge" id="dTotal">0</span> <i class= 'fa fa-plus'></i> 
										</a>
					            	</div>					            															            
					            </div>	           		                                           	                																	                    	
                    			<div class="row">
			                    	<div class="col-md-12">
			                    	<table id="DendaListTable" class="table table-bordered table-hover">
			    							<tr>			    								
			    								<th style="text-align: center">Keterangan Denda</th>
			    								<th style="text-align: center">status</th>
			    								<th style="text-align: center">Bulan</th>
			    								<th style="text-align: center">Periode</th>
			    								<th style="text-align: center">Harga</th>
			    								<th style="text-algin: center">Total</th>
			    								<th style="text-align: center" width="3%"></th>
			    							</tr>
			                            </table>
			                    	</div>
			                    </div>
                    		</div>
                    	</div>                    	                        
                    </div>
                    <div class="row">
                    	<div class="panel panel-primary">                    		
                    		<div class="panel-body">
                    			<div class="col-md-3">
		                        	<select class="select-block" name="lkstatus">
		                        		<option value="lebih">Lebih Bayar</option>
		                        		<option value="kurang">Kurang Bayar</option>
		                        	</select>
		                        </div>
		                        <div class="col-md-3">
		                        	<input class="form-control" value=0 maxlength="9" placeholder="nominal" name="lkbayar">
		                        </div>
                    		</div>
                    	</div>                    	                        
                    </div> 
                    <div class="row">
                    	<div class="panel panel-primary">                    		
                    		<div class="panel-body">                    			
		                        <div class="col-md-12">
		                        	<label for="keterangan">Keterangan :</label>
		                        	<textarea maxlength="200" class="form-control" name="keterangan" placeholder="keterangan..">
		                        	</textarea> 
		                        </div>
                    		</div>
                    	</div>                    	                        
                    </div>                	                        
                    <div class="row">
                    	<div class="col-md-12">
                            <input type="button" onClick="check_pelajaran()" class="btn btn-primary btn-hg btn-embossed btn-block" value="LANJUT">
                        </div>
                    </div>                 
                </form>
            </div>
        </div>
    </div>
</div>

<div id="footer">
	<div class="container">
    	<p class="credit">Copyright&copy;2014.MasterPanel by ITS STTC.All right Reserved</p>
    </div>
</div>

<?php $this->load->view('template/javascript_link'); ?>
<script src="<?php echo base_url();?>assets/js/jquery.ui.core.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.ui.widget.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.ui.position.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.ui.menu.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.ui.autocomplete.js"></script>
<script src="<?php echo base_url();?>assets/js/custom/user_pelajaran.js"></script>
<script language="javascript">
$(document).ready(function() {	
	$(".select-block").selectpicker({style: 'btn btn-primary', menuStyle: 'dropdown-inverse'});
	$("button#s_denda").addClass('hide-denda');	
	//$('#murid').checkbox('check');
	var pp = $(".pilih-biaya");
	var pdb = $("#pilih-detail-biaya");
	if(pp.val() == "USS"){
		$(".opt-USK,.opt-Sem1,.opt-Sem2,.opt-adm,.opt-daftar,.opt-test").hide();
		$(".opt-USS,.periode").show();
	}else if(pp.val() == "USK"){
		$(".opt-USS,.opt-Sem1,.opt-Sem2,.opt-adm,.opt-daftar,.opt-test").hide();
		$(".opt-USK,.periode").show();
	}else if(pp.val() == "Sem1"){
		$(".opt-USK,.opt-USS,.opt-Sem2,.opt-adm,.opt-daftar,.opt-test,.periode").hide();
		$(".opt-Sem1").show();
	}else if(pp.val() == "Sem2"){
		$(".opt-USK,.opt-Sem1,.opt-USS,.opt-adm,.opt-daftar,.opt-test,.periode").hide();
		$(".opt-Sem2").show();
	}else if(pp.val() == "adm"){
		$(".opt-USK,.opt-Sem1,.opt-Sem2,.opt-USS,.opt-daftar,.opt-test,.periode").hide();
		$(".opt-adm").show();
	}else if(pp.val() == "daftar"){
		$(".opt-USK,.opt-Sem1,.opt-Sem2,.opt-adm,.opt-USS,.opt-test,.periode").hide();
		$(".opt-daftar").show();
	}else if(pp.val() == "test"){
		$(".opt-USK,.opt-Sem1,.opt-Sem2,.opt-adm,.opt-daftar,.opt-USS,.periode").hide();
		$(".opt-test").show();
	}else{
		$(".opt-USS,.opt-USK,.opt-Sem1,.opt-Sem2,.opt-adm,.opt-daftar,.opt-USS,.periode").hide();
	}		
	pp.on("change",function(){
		pdb.val(" ");
		$(".pilih-detail-biaya .filter-option").text('Nothing selected');
		$(".pilih-detail-biaya ul li:first-child").addClass('selected');
		if(pp.val() == "USS"){
			$(".opt-USK,.opt-Sem1,.opt-Sem2,.opt-adm,.opt-daftar,.opt-test").hide();
			$(".opt-USS,.periode").show();
		}else if(pp.val() == "USK"){
			$(".opt-USS,.opt-Sem1,.opt-Sem2,.opt-adm,.opt-daftar,.opt-test").hide();
			$(".opt-USK,.periode").show();
		}else if(pp.val() == "Sem1"){
			$(".opt-USK,.opt-USS,.opt-Sem2,.opt-adm,.opt-daftar,.opt-test,.periode").hide();
			$(".opt-Sem1").show();
		}else if(pp.val() == "Sem2"){
			$(".opt-USK,.opt-Sem1,.opt-USS,.opt-adm,.opt-daftar,.opt-test,.periode").hide();
			$(".opt-Sem2").show();
		}else if(pp.val() == "adm"){
			$(".opt-USK,.opt-Sem1,.opt-Sem2,.opt-USS,.opt-daftar,.opt-test,.periode").hide();
			$(".opt-adm").show();
		}else if(pp.val() == "daftar"){
			$(".opt-USK,.opt-Sem1,.opt-Sem2,.opt-adm,.opt-USS,.opt-test,.periode").hide();
			$(".opt-daftar").show();
		}else if(pp.val() == "test"){
			$(".opt-USK,.opt-Sem1,.opt-Sem2,.opt-adm,.opt-daftar,.opt-USS,.periode").hide();
			$(".opt-test").show();
		}else{
			$(".opt-USS,.opt-USK,.opt-Sem1,.opt-Sem2,.opt-adm,.opt-daftar,.opt-USS,.periode").hide();
		}		
	});	

	if($("#pilih-diskon").val() == 'lain'){
		$(".hide-diskon").show();
		$(".hide-diskon").val('');
	}else{
		$(".hide-diskon").hide();
		$(".hide-diskon").val('');
	}
	$("#pilih-diskon").on("change",function(){
		if($("#pilih-diskon").val() == 'lain'){
			$(".hide-diskon").show();
			$(".hide-diskon").val('');
		}else{
			$(".hide-diskon").hide();
			$(".hide-diskon").val('');
		}
	})

	if($("#pilih-denda").val() == 'lain'){
		$(".hide-denda").show();
		$(".hide-denda").val('');
	}else{
		$(".hide-denda").hide();
		$(".hide-denda").val('');
	}
	$("#pilih-denda").on("change",function(){
		if($("#pilih-denda").val() == 'lain'){
			$(".hide-denda").show();
			$(".hide-denda").val('');
		}else{
			$(".hide-denda").hide();
			$(".hide-denda").val('');
		}
	})

	$(function() {
		var availablenimTags = [
			<?php foreach($murid->result_array() as $row){
				echo '{
						value: "'.$row['nim'].'",
						label: "'.$row['nim'].'",
						name: "'.$row['nm_murid'].'",
						desc: "'.$row['ket'].'"						
					  },';
			}?>			
		];
		var availablenameTags = [
			<?php foreach($murid->result_array() as $row){
				echo '{
						value: "'.$row['nim'].'",
						label: "'.$row['nm_murid'].'",
						name: "'.$row['nim'].'",
						desc: "'.$row['ket'].'"
					  },';
			}?>			
		];
		$( ".autocomplete-f-nim" ).autocomplete({
			minLength: 0,
			source: availablenimTags,
			focus: function( event, ui ) {
				$( ".autocomplete-f-nim" ).val( ui.item.label );
				return false;
			},
			select: function( event, ui ) {
				$( ".autocomplete-f-nim" ).val( ui.item.label );
				$( ".autocomplete-f-nim-id" ).val( ui.item.value );
				$( ".autocomplete-f-nim-name" ).val( ui.item.name );
				$( ".autocomplete-f-nim-description" ).val( ui.item.desc );
				$( ".autocomplete-f-nama" ).val( ui.item.name );

				return false;
			}
		})
		.data( "ui-autocomplete" )._renderItem = function( ul, item ) {
			return $( "<li>" )
				.append( "<a>" + item.label + "|" + item.name + "<br>" + item.desc + "</a>" )
				.appendTo( ul );
		};


		$( ".autocomplete-f-nama" ).autocomplete({
			minLength: 0,
			source: availablenameTags,
			focus: function( event, ui ) {
				$( ".autocomplete-f-nama" ).val( ui.item.label );
				return false;
			},
			select: function( event, ui ) {
				$( ".autocomplete-f-nama" ).val( ui.item.label );
				$( ".autocomplete-f-nama-id" ).val( ui.item.value );
				$( ".autocomplete-f-nama-name" ).val( ui.item.name );
				$( ".autocomplete-f-nama-description" ).val( ui.item.desc );
				$( ".autocomplete-f-nim" ).val( ui.item.name );

				return false;
			}
		})
		.data( "ui-autocomplete" )._renderItem = function( ul, item ) {
			return $( "<li>" )
				.append( "<a>" + item.label + "|" + item.name + "<br>" + item.desc + "</a>" )
				.appendTo( ul );
		};
	});
});


</script>

</body>
</html>