<!doctype html>
<html>
<head>
<?php $this->load->view('template/user_head_link'); ?>
</head>

<body>
<div id="wrap">
	<?php echo $navigation; ?>
    <div class="container">
        <div class="row">
        	<div class="col-md-6 tile">
            	<img class="tile-image" alt="Pensils" src="<?php echo base_url().'assets/';?>images/icons/svg/paper-bag.svg"></img>
                <h2 class="tile-title">Barang</h2>
                <p>Penjualan Barang</p>
                <a href="<?php echo base_url().'index.php/user/barang';?>" class="btn btn-warning btn-large btn-block"><i class="fa fa-plus"></i> Tambah</a>
                <a href="<?php echo base_url().'index.php/user/barang/view';?>" class="btn btn-warning btn-large btn-block"> 
                <i class="fa fa-eye"></i>
                View Kwitansi</a>
            </div>
            <div class="col-md-6 tile">
            	<img class="tile-image" alt="Pensils" src="<?php echo base_url().'assets/';?>images/icons/svg/book.svg"></img>
                <h2 class="tile-title">Pelajaran</h2>
                <p>Pembayaran Pelajaran & Iuran</p>
                <a href="<?php echo base_url().'index.php/user/pelajaran';?>" class="btn btn-danger btn-large btn-block"><i class="fa fa-plus"></i> Tambah</a>
                <a href="<?php echo base_url().'index.php/user/pelajaran/view';?>" class="btn btn-danger btn-large btn-block"> 
                <i class="fa fa-eye"></i>
                View Kwitansi</a>
            </div>
        </div>
    </div>
</div>

<div id="footer">
	<div class="container">
    	<p class="credit">Copyright&copy;2014.MasterPanel by ITS STTC.All right Reserved</p>
    </div>
</div>

<?php $this->load->view('template/javascript_link'); ?>

</body>
</html>