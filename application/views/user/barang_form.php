<!doctype html>
<html>
<head>
<?php $this->load->view('template/user_head_link'); ?>
<link href="<?php echo base_url(); ?>assets/css/jquery-ui/jquery-ui.css" rel="stylesheet">
	<style type="text/css">
		.ui-autocomplete{
			max-height: 300px;
			overflow: auto;
		}
	</style>
</head>

<body>
<div id="wrap">
	<?php echo $navigation; ?>
    <div class="container">
       <div class="row">
            <div class="col-lg-12">
                <h1>Barang <small>Tambah Pembelian</small></h1>
                <ol class="breadcrumb">
                	<li><a href="<?php echo base_url().'index.php/user/main';?>">Beranda</a></li>
                    <li class="active">Barang</li>
                </ol>             
            </div>
        </div>
        <div class="row">
        	<?php
        	 ($this->session->flashdata('message')) ? print"<div class='alert alert-success'>".$this->session->flashdata('message')."</div>" : NULL;
        	 if(validation_errors()){echo "<div class='alert alert-danger'>".validation_errors()."</div>";} ?>
        	<div class="well">
            	<form name='frbarang' method="post" action="<?php echo base_url().'index.php/user/barang/add';?>">
                	<div class="row">
                        <label class="label label-info" for="murid">Murid SC?</label><br>
                        <div class="switch switch-square" data-on-label="<i class='fui-check'></i>" data-off-label="<i class='fui-cross'></i>">
  							<input type="checkbox" checked name="murid" value="1" id="murid">
                        </div>
                    </div>                                			            
                    <div class="row">

                    	<div class="col-md-3">
                        	<label class="label label-info" for="nim">NIM</label><br>
                            <input name="nim" class="form-control autocomplete-f-nim" type="text">
                            <input type="hidden" class="autocomplete-f-nim-id autocomplete-f-nama-id">
                        </div>
                        <div class="col-md-3">
                            <label class="label label-info" for="nama">Nama</label><br>
                        	<input class="form-control nama autocomplete-f-nama" type="text" name="nama">
                        </div>

                        <!-- <div class="col-md-3">
                        	<label class="label label-info" for="nim">NIM</label><br>
                            <select class="select-block form-control nim" name="nim">
                            	<option></option>
								<?php foreach($murid->result_array() as $row){
                                        print"<option value='$row[nim]|$row[nm_murid]'>$row[nim]</option>";
                                }
                                ?>
                            </select>
                        </div>
                        <div class="col-md-3">
                            <label class="label label-info" for="nama">Nama</label><br>
                        	<input class="form-control nama" type="text" name="nama">
                        </div> -->
                    </div>
                    <div class="row">
                    	<label class="label label-info" for="pilih-barang">Pilih Barang</label><br>
                        <div class="col-md-3">
                          <select class="select-block" id="pilih-barang" name="pilih-barang">
                          	<?php foreach($barang->result_array() as $row){
									print"<option value='$row[kd_barang]|$row[harga]'>$row[nm_barang]</option>";
							}
							?>
                          </select>
                        </div>
                        <div class="col-md-1">
                        	<input placeholder="Qty" maxlength="4" type="number" class="form-control" id="qty" name="quantity">
                        </div>
                        <div class="col-md-1">
                          <a class="btn btn-primary" name="add" onClick="addList('ListTable')">
                              Add
                              <span class="badge" id="pTotal">0</span>
                          </a>                         
                        </div>
                        <div class="col-md-5">
                          <span class="btn btn-primary" id="total">
                              Total :                    
                          </span>                         
                        </div>                                   	
                    </div>                    
                    <div class="row">
                    	<div class="col-md-12">
                            <table id="ListTable" class="table table-bordered table-hover">
    
                            </table>
                        </div>
                    </div>
                    <div class="row">
                    	<div class="col-md-12">
                            <input type="button" onClick="check_barang()" class="btn btn-primary btn-hg btn-embossed btn-block" value="LANJUT">
                        </div>
                    </div>                 
                </form>
            </div>
        </div>
    </div>
</div>

<div id="footer">
	<div class="container">
    	<p class="credit">Copyright&copy;2014.MasterPanel by ITS STTC.All right Reserved</p>
    </div>
</div>

<?php $this->load->view('template/javascript_link'); ?>
<script src="<?php echo base_url();?>assets/js/jquery.ui.core.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.ui.widget.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.ui.position.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.ui.menu.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.ui.autocomplete.js"></script>
<script language="javascript">
$(document).ready(function() {
	//$('#murid').checkbox('check');
	$(".select-block").selectpicker({style: 'btn btn-primary', menuStyle: 'dropdown-inverse'});
    //$("#pilih-barang").selectpicker({style: 'btn btn-primary', menuStyle: 'dropdown-inverse'});
	var innama = $(".autocomplete-f-nama");
	var innim = $(".autocomplete-f-nim");
	//var murid = innim.val().split("|");
	if(frbarang.murid.checked == true){
		innama.attr('disabled',false);
		//innama.val(murid[1]);
		innama.attr('required',true);
		innim.attr('disabled',false);
		innim.attr('required',true);
		innim.focus();
	}else{		
		innama.attr('disabled',false);
		innama.focus();
		innama.attr('required',true);
		innim.attr('disabled',true);
		innim.val('');
		innim.attr('required',false);
	}
	
	$("#murid").on('change',function(){
		if(frbarang.murid.checked == true){			
			innama.attr('disabled',false);
			innama.val('');
			innama.attr('required',true);
			innim.removeClass('disabled');
			innim.attr('disabled',false);
			innim.attr('required',true);
			innim.focus();
		}else{			
			innama.attr('disabled',false);
			innama.focus();
			innama.attr('required',true);
			innim.addClass('disabled');
			innim.attr('disabled',true);
			innim.val('');
			innim.attr('required',false);
		}
	});
	
	innim.on("change",function(){
		murid = innim.val().split("|");
		innama.val(murid[1]);
	});

	$(function() {
		var availablenimTags = [
			<?php foreach($murid->result_array() as $row){
				echo '{
						value: "'.$row['nim'].'",
						label: "'.$row['nim'].'",
						name: "'.$row['nm_murid'].'",
						desc: "'.$row['ket'].'"						
					  },';
			}?>			
		];
		var availablenameTags = [
			<?php foreach($murid->result_array() as $row){
				echo '{
						value: "'.$row['nim'].'",
						label: "'.$row['nm_murid'].'",
						name: "'.$row['nim'].'",
						desc: "'.$row['ket'].'"
					  },';
			}?>			
		];
		$( ".autocomplete-f-nim" ).autocomplete({
			minLength: 0,
			source: availablenimTags,
			focus: function( event, ui ) {
				$( ".autocomplete-f-nim" ).val( ui.item.label );
				return false;
			},
			select: function( event, ui ) {
				$( ".autocomplete-f-nim" ).val( ui.item.label );
				$( ".autocomplete-f-nim-id" ).val( ui.item.value );
				$( ".autocomplete-f-nim-name" ).val( ui.item.name );
				$( ".autocomplete-f-nim-description" ).val( ui.item.desc );
				$( ".autocomplete-f-nama" ).val( ui.item.name );

				return false;
			}
		})
		.data( "ui-autocomplete" )._renderItem = function( ul, item ) {
			return $( "<li>" )
				.append( "<a>" + item.label + "|" + item.name + "<br>" + item.desc + "</a>" )
				.appendTo( ul );
		};


		$( ".autocomplete-f-nama" ).autocomplete({
			minLength: 0,
			source: availablenameTags,
			focus: function( event, ui ) {
				$( ".autocomplete-f-nama" ).val( ui.item.label );
				return false;
			},
			select: function( event, ui ) {
				$( ".autocomplete-f-nama" ).val( ui.item.label );
				$( ".autocomplete-f-nama-id" ).val( ui.item.value );
				$( ".autocomplete-f-nama-name" ).val( ui.item.name );
				$( ".autocomplete-f-nama-description" ).val( ui.item.desc );
				$( ".autocomplete-f-nim" ).val( ui.item.name );

				return false;
			}
		})
		.data( "ui-autocomplete" )._renderItem = function( ul, item ) {
			return $( "<li>" )
				.append( "<a>" + item.label + "|" + item.name + "<br>" + item.desc + "</a>" )
				.appendTo( ul );
		};
	});

});

function check_barang(){
	if($("#pTotal").text() == 0){
		alert('Please Fill in the Barang List');
	}else{
		frbarang.submit();
	}
}

function addList(mtable){
	var table = document.getElementById(mtable);
	var qty = $("#qty").val();
	var rowCount = table.rows.length;
	var barang = $("#pilih-barang").val().split("|");
	var kd = barang[0];
	var hrg = barang[1];
	var exist = false;
	
	for(var i = 0; i < rowCount; i++){
		var row = table.rows[i];
		var rec_kd = row.cells[0].childNodes[0].value;
		if(rec_kd == kd){		
			exist = true;
			row.cells[0].childNodes[1].value = row.cells[0].childNodes[6].value = qty;
			row.cells[0].childNodes[3].value = hrg * qty;
			row.cells[1].childNodes[0].value ="Rp. " +addCommas(hrg * qty);
			total(mtable);
		}
	}
	
	if(isNaN(qty) || qty == '' || qty <= 0 || rowCount >= 7){
		$("#qty").focus();
	}else if(exist === true){
			
	}else{
		var row = table.insertRow(rowCount);		
		var selectedText = $("#pilih-barang option:selected").text();
		
		var kd_barang = document.createElement("input");
		kd_barang.type ="hidden";
		kd_barang.name ="kd_barang[]";
		kd_barang.value = kd;
		var quantity = document.createElement("input");
		quantity.type ="hidden";
		quantity.name ="quantity[]";
		quantity.value = qty;
		var harga = document.createElement("input");
		harga.type ="hidden";
		harga.name ="harga[]";
		harga.value = hrg;
		var stotal = document.createElement("input");
		stotal.type ="hidden";
		stotal.name ="stotal[]";
		stotal.value = hrg*qty;
		
		var times = document.createElement("span");
		times.className = "fa fa-times";
		times.style.width= "10%";
		times.style.textAlign = "center";
		var cell1 = row.insertCell(0);
		var element1 = document.createElement("input");
		element1.type = "text";
		element1.readOnly = "true";
		element1.style.width = "60%";
		element1.className = "btn btn-primary";
		element1.style.textAlign ="left";
		element1.value = selectedText;
		var element2 = document.createElement("input");
		element2.type = "button";
		element2.readOnly = "true";
		element2.style.width = "10%";
		element2.style.paddingLeft = "0";
		element2.style.paddingRight = "0";
		element2.value = qty;
		element2.className = "btn btn-info";
		var element4 = document.createElement("input");
		element4.className = "btn btn-info";
		element4.style.width = "20%";
		element4.style.paddingLeft = "0";
		element4.style.paddingRight = "0";		
		element4.type = "button";
		element4.value = "@." + addCommas(hrg);
		cell1.appendChild(kd_barang);
		cell1.appendChild(quantity);
		cell1.appendChild(harga);
		cell1.appendChild(stotal);				
		cell1.appendChild(element1);
		cell1.appendChild(times);
		cell1.appendChild(element2);
		cell1.appendChild(element4);				
		
		var cell2 = row.insertCell(1);
		var element5 = document.createElement("input");
		element5.className = "btn btn-success";
		element5.style.width = "100%";
		element5.style.paddingLeft = "0";
		element5.style.paddingRight = "0";		
		element5.type = "button";
		element5.value = "Rp. " +addCommas(stotal.value);
		cell2.appendChild(element5);		
		
		var cell3 = row.insertCell(2);
		var element3 = document.createElement("input");
		element3.type = "button";
		element3.className = "btn btn-danger";
		element3.value = "Delete";
		element3.setAttribute("onClick","deleteRow('"+kd_barang.value+"','ListTable')");
		cell3.appendChild(element3);
		
		document.getElementById("pilih-barang").value=" ";
		document.getElementById("pTotal").innerHTML = rowCount + 1;
		total(mtable);
	}
}

function total(mtable){
	var table = document.getElementById(mtable);
	var rowCount = table.rows.length;
	var total=0;
	for(var i = 0; i < rowCount; i++){
		var row = table.rows[i];
		var stotal = parseInt(row.cells[0].childNodes[3].value);
		total += stotal;		
	}
	document.getElementById('total').innerHTML = "Total : Rp. "+addCommas(total);
}

function deleteRow(kode,mtable){
	var table = document.getElementById(mtable);
	var rowCount = table.rows.length;
	for(var i = 0; i < rowCount; i++){
		var row = table.rows[i];
		var rec_kd = row.cells[0].childNodes[0].value;
		if(rec_kd == kode){			
			table.deleteRow(i);
			rowCount--;
			i--;
			
			document.getElementById('pTotal').innerHTML = rowCount;
		}
	}	
	total(mtable);
}

function addCommas(nStr){
	nStr += '';
	x = nStr.split('.');
	x1 = x[0];
	x2 = x.length > 1 ? '.' + x[1] : '';
	var rgx = /(\d+)(\d{3})/;
	while (rgx.test(x1)){
		x1 = x1.replace(rgx, '$1' + ',' + '$2');
	}
	return x1 + x2;
}
</script>

</body>
</html>