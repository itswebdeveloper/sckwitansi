<!DOCTYPE html>
<html lang="en">
  <head>
  	<?php $this->load->view('template/head_link'); ?>
  </head>

  <body>
<div id="wrap">
  	<div class="login-screen">
      <div class="login-icon">
        <i style=" text-shadow:5px 5px 1px #428BCA;" class="fa fa-music fa-5x"></i> 
        <h4>
          v2.7.3
          <small>SC Kwitansi</small>
        </h4>
      </div>
    	<div class="login-form">
    		<form class="form-signin" method="post" action="<?php echo base_url().'index.php/login'?>">              
              <h5 class="form-signin-heading">Please sign in</h5>
              <div class="form-group">
                <input type="text" name="name" id="name" value=" " class="form-control login-field" placeholder="Login Name" required autofocus>
                <label class="login-field-icon fui-user" for="name"></label>
              </div>
              <div class="form-group">
                <input type="password" name="pass" id="pass" value="" class="form-control login-field" placeholder="Password" required>
                <label class="login-field-icon fui-lock" for="pass"></label>
              </div>
              <button class="btn btn-embossed btn-success btn-block" type="submit">Sign in</button>
            </form>
            <?php if(isset($msg)){
				print"<div class='alert alert-danger'>$msg</div>";} ?>
    	</div>
    </div>
</div>

<div id="footer">
    <div class="container">
        <p class="credit">Copyright&copy;2014.MasterPanel by ITS STTC.All right Reserved</p>
    </div>
</div>
    <!-- JavaScript -->
    <?php $this->load->view('template/javascript_link'); ?>

  </body>
</html>