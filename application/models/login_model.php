<?php if( ! defined('BASEPATH')) exit ('No direct script access allowed');

class Login_model extends CI_Model{
	function __construct(){
		parent::__construct();
		$this->load->helper('url');
	}
	
	public function validate(){		
		//grab user input
		$username = $this->security->xss_clean($this->input->post('name'));
		$password = $this->security->xss_clean($this->input->post('pass'));
		
		//prep the query
		$this->db->where('name', $username);
		$this->db->where('pass', md5($password));
		
		//run the query
		$query = $this->db->get('login');
		//lets check if there are any results
		if($query->num_rows == 1)
		{
			//if there is a user, then create session data
			$row = $query->row();
			$data = array(				
				'username' => $row->name,
				'level' => $row->level,
				'validated' => true
				);
			$this->session->set_userdata($data);
			return true;
		}
		//if process did not validate
		//then return false
		return false;
	}
	
}
?>