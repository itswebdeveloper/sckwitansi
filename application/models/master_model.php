<?php
class master_model extends CI_Model{
	
	function master_model(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->database();
	}
	
	function general(){		
		//$this->load->library('session');								
		$this->load->library('navigation');
		$navigation = new navigation;
		
		$data['navigation'] 	=  $navigation -> show_menu();
		$data['title']		 = 'Sumatra Conservatoire Administrator';		
		$data['footer']		= '';//'Copyright &copy; 2011. <font class="sumatracfont">Sumatra Conservatoire</font>. All Rights Reserved.';
		
		return $data;
	}

	/*function brute_fix(){
		$jenis = $this->get_jenis();
		foreach ($jenis->result_array() as $key) {
			$data = array('nm_biaya' => "UU ".$key['nm_jenis']." Semester 1");
			$this->db->update('biaya', $data, array('kd_jenis' => $key['kd_jenis'], 'jenis_biaya' => 'Sem1'));	

			$data2 = array('nm_biaya' => "UU ".$key['nm_jenis']." Semester 2");
			$this->db->update('biaya', $data, array('kd_jenis' => $key['kd_jenis'], 'jenis_biaya' => 'Sem2'));	
		}
	}*/
	
	function set_kwitansi_status($no_kwitansi, $status){
		(substr($no_kwitansi, 0, 1) == 'B') ? $tb_kwitansi = "m_kwitansibrg" : $tb_kwitansi = "m_kwitansipljr" ;
		$data = array(
				'status' => $status
			);
		return ($this->db->update($tb_kwitansi, $data, array('no_kwitansi' => $no_kwitansi) ))? TRUE : FALSE;
	}

	//user
	function get_selected_login($id = NULL){	
		$this->db->where('id',$id);
		$query = $this->db->get('login');
		return $query->row_array();		
	}

	function get_login($level){
		$this->db->where('level',$level);
		$query = $this->db->get('login');
		return $query->row_array();
	}

	function update_login(){
		$id = $this->input->post('id');
		$current = $this->get_selected_login($id);	
		
		if($current['pass'] === md5($this->input->post('oldpass'))){
			$data = array(
			'name' => $this->input->post('name'),
			'pass' => md5($this->input->post('pass'))
			);
			
			$this->db->update('login',$data, array('id'=>$id));
			return true;
		}else{
			return false;
		}
		
	}
	
	//barang
	function get_barang($kode = NULL){
		if($kode==NULL){
			$query = $this->db->get('barang');
			return $query;
		}else{
			$this->db->where('kd_barang',$kode);
			$query = $this->db->get('barang');
			return $query->row_array();
		}
	}
	
	function add_barang(){
		$data = array(
		'kd_barang' => strtoupper($this->input->post('kd_barang')),
		'nm_barang' => $this->input->post('nm_barang'),
		'harga' => $this->input->post('harga')
		);

		$this->db->insert('barang', $data);
	}
	
	function update_barang(){
		$data = array(		
		'nm_barang' => $this->input->post('nm_barang'),
		'harga' => $this->input->post('harga')
		);																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																			
		$kode = $this->input->post('kd_barang');
		
		$this->db->update('barang', $data, array('kd_barang' => $kode));
	}
	
	function delete_barang($kode){
		$this->db->delete('barang', array('kd_barang' => $kode)); 
	}
	
	//murid
	function get_murid($nim = NULL){
		if($nim==NULL){
			$query = $this->db->get('murid');
			return $query;
		}else{
			$this->db->where('nim',$nim);
			$query = $this->db->get('murid');
			return $query->row_array();
		}
	}
	
	function get_diskon_murid($nim = NULL){
		$this->db->select("diskon_murid.*,diskon.*");
		$this->db->join("diskon","diskon_murid.kd_diskon = diskon.kd_diskon");
		$this->db->where('diskon_murid.nim',$nim);
		$query = $this->db->get('diskon_murid');
		return $query;
	}
	
	function add_murid($nim=NULL){
		if($nim==NULL)
		{
			$data = array(
			'nim' => strtoupper($this->input->post('nim')),
			'nm_murid' => $this->input->post('nm_murid'),
			'ket' => $this->input->post('ket')
			);

			if($this->db->insert('murid', $data))
				return true;
			else
				return false;
		}else{
			$data = array(
			'nim' => strtoupper($nim),
			'nm_murid' => $this->input->post('nm_murid'),
			'ket' => $this->input->post('ket')
			);

			if($this->db->insert('murid', $data))
				return true;
			else
				return false;
		}
	}
	
	function update_murid(){
		$data = array(		
		'nm_murid' => $this->input->post('nm_murid'),
		'ket' => $this->input->post('ket')
		);
		$kode = $this->input->post('nim');
		
		$this->db->update('murid', $data, array('nim' => $kode));		
	}
	
	function delete_diskon_murid($nim){
		$this->db->delete('diskon_murid',array('nim'=>$nim));
	}
	
	function delete_murid($nim){
		$this->db->delete('murid', array('nim' => $nim)); 
	}

	function murid_kwitansipljr_relation($nim){
		$this->db->where('nim', $nim);
		$kwitansi = $this->db->get("m_kwitansipljr");
		return ($kwitansi->num_rows() > 0) ? $kwitansi : NULL;
	}

	function murid_kwitansibrg_relation($nim){
		$this->db->where('nim', $nim);
		$kwitansi = $this->db->get("m_kwitansibrg");
		return ($kwitansi->num_rows() > 0) ? $kwitansi : NULL;
	}

	function view_new_murid($nim){
		$this->db->select("*");
		$this->db->where('nim',$nim);
		$query = $this->db->get('murid_baru');
		return $query->row_array();
	}

	function count_new_murid($select = NULL){
		if($select == NULL){
			$this->db->select("*");
			$this->db->where('status','NEW');
			$query = $this->db->get('murid_baru');
		}else{
			$query = $this->db->get('murid_baru');
		}
		return $query;
	}

	function add_new_murid($nama){
		if($this->count_new_murid("all")->num_rows() > 0){
			$this->db->select("nim");
			$this->db->order_by("id", "desc");
			$murid_baru = $this->db->get("murid_baru")->row_array();
			$murid_baru = 'N_'.(((int)substr($murid_baru['nim'],2,10))+1);
		}else{
			$murid_baru = 'N_0';
		}
		$data = array(
			'nim' => $murid_baru,
			'nim_baru' => NULL,
			'nm_murid' => $nama,
			'ket' => "Murid Baru",
			'status' => "NEW"
		);
		$this->db->insert('murid_baru',$data);

		return $murid_baru;
	}

	function move_murid_baru(){
		if($this->add_murid($this->input->post('nim_baru'))){
			$data = array(
				'nim_baru' => $this->input->post('nim_baru'),
				'nm_murid' => $this->input->post('nm_murid'),
				'ket'	   => $this->input->post('ket'),
				'status'   => 'MOVED'
			);
			$this->db->where('nim',$this->input->post('nim'));
			if($this->db->update('murid_baru',$data)) { 				
				$data = array(
					'nim'  => $this->input->post('nim_baru'),
					'nama' => $this->input->post('nm_murid')
				);
				$this->db->where('nim', $this->input->post('nim'));
				$this->db->update('m_kwitansipljr', $data);
				return TRUE; 
			}else{ 
				return FALSE ;
			}
		}else{
			return FALSE;
		}
	}

	function delete_murid_baru($nim){
		$data = array('status'=>"REMOVED");
		$this->db->where('nim',$nim);
		return ($this->db->update('murid_baru', $data))? TRUE : FALSE;
	}
	
	
	//program
	function get_program($kd_program = NULL){
		if($kd_program==NULL){
			$query = $this->db->get('program');
			return $query;
		}else{
			$this->db->where('kd_program',$kd_program);
			$query = $this->db->get('program');
			return $query->row_array();
		}
	}
	
	function add_program(){
		$data = array(
		'kd_program' => strtoupper($this->input->post('kd_program')),
		'nm_program' => $this->input->post('nm_program')
		);

		$this->db->insert('program', $data);
	}
	
	function update_program(){
		$data = array(		
		'nm_program' => $this->input->post('nm_program'),
		);
		$kode = $this->input->post('kd_program');
		
		$this->db->update('program', $data, array('kd_program' => $kode));
	}
	
	function delete_program($kd_program){
		$this->db->delete('program', array('kd_program' => $kd_program)); 
	}
	
	//subprogram
	function get_subprogram($kd_subprogram = NULL){
		if($kd_subprogram==NULL){
			$query = $this->db->get('subprogram');
			return $query;
		}else{
			$this->db->where('kd_subprogram',$kd_subprogram);
			$query = $this->db->get('subprogram');
			return $query->row_array();
		}
	}
	
	function add_subprogram(){
		$data = array(
		'kd_subprogram' => strtoupper($this->input->post('kd_subprogram')),
		'nm_subprogram' => $this->input->post('nm_subprogram')
		);

		$this->db->insert('subprogram', $data);
	}
	
	function update_subprogram(){
		$data = array(		
		'nm_subprogram' => $this->input->post('nm_subprogram'),
		);
		$kode = $this->input->post('kd_subprogram');
		
		$this->db->update('subprogram', $data, array('kd_subprogram' => $kode));
	}
	
	function delete_subprogram($kd_subprogram){
		$this->db->delete('subprogram', array('kd_subprogram' => $kd_subprogram)); 
	}
	
	//jenis
	function get_jenis($kd_jenis = NULL){
		if($kd_jenis==NULL){
			$query = $this->db->get('jenis');
			return $query;
		}else{
			$this->db->where('kd_jenis',$kd_jenis);
			$query = $this->db->get('jenis');
			return $query->row_array();
		}
	}
	
	function add_jenis(){
		$data = array(
		'kd_jenis' => strtoupper($this->input->post('kd_jenis')),
		'nm_jenis' => $this->input->post('nm_jenis')
		);

		$this->db->insert('jenis', $data);
	}
	
	function update_jenis(){
		$data = array(		
		'nm_jenis' => $this->input->post('nm_jenis'),
		);
		$kode = $this->input->post('kd_jenis');
		
		$this->db->update('jenis', $data, array('kd_jenis' => $kode));
	}
	
	function delete_jenis($kd_jenis){
		$this->db->delete('jenis', array('kd_jenis' => $kd_jenis)); 
	}
	
	//tingkat
	function get_tingkat($kd_tingkat = NULL){
		if($kd_tingkat==NULL){
			$query = $this->db->get('tingkat');
			return $query;
		}else{
			$this->db->where('kd_tingkat',$kd_tingkat);
			$query = $this->db->get('tingkat');
			return $query->row_array();
		}
	}
	
	function add_tingkat(){
		$data = array(
		'kd_tingkat' => strtoupper($this->input->post('kd_tingkat')),
		'nm_tingkat' => $this->input->post('nm_tingkat')
		);

		$this->db->insert('tingkat', $data);
	}
	
	function update_tingkat(){
		$data = array(		
		'nm_tingkat' => $this->input->post('nm_tingkat'),
		);
		$kode = $this->input->post('kd_tingkat');
		
		$this->db->update('tingkat', $data, array('kd_tingkat' => $kode));
	}
	
	function delete_tingkat($kd_tingkat){
		$this->db->delete('tingkat', array('kd_tingkat' => $kd_tingkat)); 
	}
	
	//diskon
	function get_diskon($kd_diskon = NULL){
		if($kd_diskon==NULL){
			$query = $this->db->get('diskon');
			return $query;
		}else{
			$this->db->where('kd_diskon',$kd_diskon);
			$query = $this->db->get('diskon');
			return $query->row_array();
		}
	}
	
	function add_diskon(){
		$data = array(
		'kd_diskon' => strtoupper($this->input->post('kd_diskon')),
		'nm_diskon' => $this->input->post('nm_diskon'),
		'persen' => $this->input->post('persen'),
		'ket_diskon' => $this->input->post('ket_diskon')
		);

		$this->db->insert('diskon', $data);
	}
	
	function update_diskon(){
		$data = array(		
		'nm_diskon' => $this->input->post('nm_diskon'),
		'persen' => $this->input->post('persen'),
		'ket_diskon' => $this->input->post('ket_diskon')
		);
		$kode = $this->input->post('kd_diskon');
		
		$this->db->update('diskon', $data, array('kd_diskon' => $kode));
	}
	
	function delete_diskon($kd_diskon){
		$this->db->delete('diskon', array('kd_diskon' => $kd_diskon)); 
	}

	//denda
	function get_denda($kd_denda = NULL){
		if($kd_denda==NULL){
			$query = $this->db->get('denda');
			return $query;
		}else{
			$this->db->where('kd_denda',$kd_denda);
			$query = $this->db->get('denda');
			return $query->row_array();
		}
	}
	
	function add_denda(){
		$data = array(
		'kd_denda' => strtoupper($this->input->post('kd_denda')),
		'nm_denda' => $this->input->post('nm_denda'),
		'status' => $this->input->post('status'),
		'harga' => $this->input->post('harga'),
		'ket_denda' => $this->input->post('ket_denda')
		);

		$this->db->insert('denda', $data);
	}
	
	function update_denda(){
		$data = array(		
		'nm_denda' => $this->input->post('nm_denda'),
		'status' => $this->input->post('status'),
		'harga' => $this->input->post('harga'),
		'ket_denda' => $this->input->post('ket_denda')
		);
		$kode = $this->input->post('kd_denda');
		
		$this->db->update('denda', $data, array('kd_denda' => $kode));
	}
	
	function delete_denda($kd_denda){
		$this->db->delete('denda', array('kd_denda' => $kd_denda)); 
	}
	
	//biaya
	function get_biaya($kode = NULL){
		if($kode==NULL){
			$query = $this->db->get('biaya');
			return $query;
		}else{			
			$this->db->where('biaya.kd_biaya',$kode);
			$this->db->order_by("biaya.jenis_biaya", "asc"); 
			$this->db->order_by("biaya.kd_program", "asc"); 
			$query = $this->db->get('biaya');
			return $query->row_array();
		}
	}
	
	function get_biaya_by_jenis($kode){
			if($kode == 'adm' || $kode == 'daftar' || $kode == 'test'){
				$this->db->where('jenis_biaya',$kode);
				$this->db->order_by("kd_program", "asc");			
				$query = $this->db->get('biaya');
			}else{
				$this->db->select("subprogram.nm_subprogram, biaya.*");
				$this->db->join('pelajaran','pelajaran.kd_pelajaran = biaya.kd_pelajaran');
				$this->db->join('subprogram','subprogram.kd_subprogram = pelajaran.kd_subprogram');
				$this->db->where('biaya.jenis_biaya',$kode);
				$this->db->order_by("biaya.kd_program", "asc"); 			
				$query = $this->db->get('biaya');
			}
			return $query;	
	}

	function get_biaya_pelajaran($kode){
		$this->db->where('kd_pelajaran',$kode);
		$query = $this->db->get('biaya');
		
		return $query;
	}
	
	function add_biaya(){
		if($this->input->post('jenis_biaya') == 'adm' || $this->input->post('jenis_biaya') == 'daftar'){
			$data = array(
				//'kd_biaya' => strtoupper($this->input->post('kd_biaya')),
				'jenis_biaya'=> $this->input->post('jenis_biaya'),
				'kd_program' => $this->input->post('kd_program'),
				'nm_biaya' => $this->input->post('nm_biaya'),
				'biaya' => $this->input->post('biaya'),
				'biaya_lama' => $this->input->post('biaya'),
				'modified_date'=> date('Y-m-d')
			);
		}elseif($this->input->post('jenis_biaya') == 'test'){
			$data= array(
				'jenis_biaya'=> $this->input->post('jenis_biaya'),
				'kd_jenis' => $this->input->post('kd_jenis'),
				'nm_biaya' => $this->input->post('nm_biaya'),
				'biaya' => $this->input->post('biaya'),
				'biaya_lama' => $this->input->post('biaya'),
				'modified_date'=> date('Y-m-d')
			);
		}
		

		$this->db->insert('biaya', $data);
	}
	
	function update_biaya(){
		$data = array(		
		'nm_biaya' => $this->input->post('nm_biaya'),
		'biaya' => $this->input->post('biaya'),
		'modified_date'=> date('Y-m-d')
		);
		$kode = $this->input->post('kd_biaya');
		
		$this->db->update('biaya', $data, array('kd_biaya' => $kode));
	}
	
	function delete_biaya($kode){
		$this->db->delete('biaya', array('kd_biaya' => $kode)); 
	}
	
	//pelajaran
	function get_pelajaran($kode = NULL){
		if($kode==NULL){
			$this->db->select("pelajaran.*, program.nm_program, tingkat.nm_tingkat");
			$this->db->join('program','program.kd_program = pelajaran.kd_program');
			$this->db->join('tingkat','tingkat.kd_tingkat = pelajaran.kd_tingkat');
			$this->db->order_by("pelajaran.kd_program", "asc"); 
			$this->db->order_by("pelajaran.kd_tingkat", "asc"); 
			$query = $this->db->get('pelajaran');
			return $query;
		}else{
			$this->db->where('kd_pelajaran',$kode);
			$query = $this->db->get('pelajaran');
			return $query->row_array();
		}
	}

	function get_pelajaran_by_program($kode_program){
		$this->db->select("pelajaran.*, subprogram.nm_subprogram, tingkat.nm_tingkat");
			$this->db->join('subprogram','subprogram.kd_subprogram = pelajaran.kd_subprogram');
			$this->db->join('tingkat','tingkat.kd_tingkat = pelajaran.kd_tingkat');
			$this->db->where('kd_program',$kode_program);
			$this->db->order_by("pelajaran.kd_subprogram", "asc"); 
			$this->db->order_by("pelajaran.kd_tingkat", "asc"); 
			$query = $this->db->get('pelajaran');
			return $query;
	}
	
	function add_pelajaran(){

		$kd_pelajaran = $this->input->post('kd_program').'-'.$this->input->post('kd_subprogram').'-'.$this->input->post('kd_jenis').'-'.$this->input->post('kd_tingkat');
		$program = $this->get_program($this->input->post('kd_program'));
		$subprogram = $this->get_subprogram($this->input->post('kd_subprogram'));
		$jenis = $this->get_jenis($this->input->post('kd_jenis'));
		$tingkat = $this->get_tingkat($this->input->post('kd_tingkat'));
		$nm_pelajaran = $program['nm_program'].'-'.$subprogram['nm_subprogram'].'-'.$jenis['nm_jenis'].'-'.$tingkat['nm_tingkat'];		

		$data = array(
		'kd_pelajaran' => $kd_pelajaran,
		'nm_pelajaran' => $nm_pelajaran,
		'kd_jenis' => $this->input->post('kd_jenis'),
		'kd_tingkat' => $this->input->post('kd_tingkat'),
		'kd_program' => $this->input->post('kd_program'),
		'kd_subprogram' => $this->input->post('kd_subprogram'),
		'ket' => $this->input->post('ket')
		);

		if($this->db->insert('pelajaran', $data)){
			if(!$this->input->post('usekolah_khusus') || $this->input->post('usekolah_khusus') == 0)
				$usekolah_khusus = $this->input->post('usekolah_standar') + 20000;
			else
				$usekolah_khusus = $this->input->post('usekolah_khusus');

			$data=array(
				array(
					'jenis_biaya' => 'USS',					
					'kd_pelajaran' => $kd_pelajaran,
					'kd_program' => $this->input->post('kd_program'),
					'kd_tingkat' => $this->input->post('kd_tingkat'),
					'kd_jenis' => $this->input->post('kd_jenis'),
					'nm_biaya' => 'US '.$jenis['nm_jenis'],
					'biaya' => $this->input->post('usekolah_standar'),
					'biaya_lama' => $this->input->post('usekolah_standar'),
					'modified_date'=> date('Y-m-d')
				),
				array(
					'jenis_biaya' => 'USK',
					'kd_pelajaran' => $kd_pelajaran,
					'kd_program' => $this->input->post('kd_program'),
					'kd_tingkat' => $this->input->post('kd_tingkat'),
					'kd_jenis' => $this->input->post('kd_jenis'),
					'nm_biaya' => 'USK '.$jenis['nm_jenis'],
					'biaya' => $usekolah_khusus,
					'biaya_lama' => $usekolah_khusus,
					'modified_date'=> date('Y-m-d')
				)
			);

			if($this->input->post('kd_pelajaran') != 'AV' && $this->input->post('kd_pelajaran') != 'AB'){
				$u_sem1 =  array(
					'jenis_biaya' => 'Sem1',
					'kd_pelajaran' => $kd_pelajaran,
					'kd_program' => $this->input->post('kd_program'),
					'kd_tingkat' => $this->input->post('kd_tingkat'),
					'kd_jenis' => $this->input->post('kd_jenis'),
					'nm_biaya' => 'UU '.$jenis['nm_jenis'].' Semester 1',
					'biaya' => $this->input->post('uujian_ganjil'),
					'biaya_lama' => $this->input->post('uujian_ganjil'),
					'modified_date'=> date('Y-m-d')
				);
				array_push($data, $u_sem1);
				$u_sem2 = array(
					'jenis_biaya' => 'Sem2',
					'kd_pelajaran' => $kd_pelajaran,
					'kd_program' => $this->input->post('kd_program'),
					'kd_tingkat' => $this->input->post('kd_tingkat'),
					'kd_jenis' => $this->input->post('kd_jenis'),
					'nm_biaya' => 'UU '.$jenis['nm_jenis'].' Semester 2',
					'biaya' => $this->input->post('uujian_genap'),
					'biaya_lama' => $this->input->post('uujian_genap'),
					'modified_date'=> date('Y-m-d')
				);
				array_push($data, $u_sem2);
			}

			$this->db->insert_batch('biaya', $data); 
		}
	}

	function update_pelajaran(){
		$data = array(		
		/*'nm_pelajaran' => $this->input->post('nm_pelajaran'),
		'kd_jenis' => $this->input->post('kd_jenis'),
		'kd_tingkat' => $this->input->post('kd_tingkat'),
		'kd_program' => $this->input->post('kd_program'),*/
		'ket' => $this->input->post('ket')		
		);
		$kode = $this->input->post('kd_pelajaran');
		
		if($this->db->update('pelajaran', $data, array('kd_pelajaran' => $kode))){
			$data=array(
				array(
					'kd_biaya' =>$this->input->post('idUSS'),
					/*'kd_program' => $this->input->post('kd_program'),
					'kd_tingkat' => $this->input->post('kd_tingkat'),
					'kd_jenis' => $this->input->post('kd_jenis'),
					'nm_biaya' => 'US '.$this->input->post('nm_pelajaran'),*/
					'biaya' => $this->input->post('USS')
				),
				array(
					'kd_biaya' =>$this->input->post('idUSK'),
					/*'kd_program' => $this->input->post('kd_program'),
					'kd_tingkat' => $this->input->post('kd_tingkat'),
					'kd_jenis' => $this->input->post('kd_jenis'),
					'nm_biaya' => 'USK '.$this->input->post('nm_pelajaran'),*/
					'biaya' => $this->input->post('USK')
				),
				array(
					'kd_biaya' =>$this->input->post('idSem1'),
					/*'kd_program' => $this->input->post('kd_program'),
					'kd_tingkat' => $this->input->post('kd_tingkat'),
					'kd_jenis' => $this->input->post('kd_jenis'),
					'nm_biaya' => 'UU '.$this->input->post('nm_pelajaran').' Semester 1',*/
					'biaya' => $this->input->post('Sem1')
				),
				array(
					'kd_biaya' =>$this->input->post('idSem2'),
					/*'kd_program' => $this->input->post('kd_program'),
					'kd_tingkat' => $this->input->post('kd_tingkat'),
					'kd_jenis' => $this->input->post('kd_jenis'),
					'nm_biaya' => 'UU '.$this->input->post('nm_pelajaran').' Semester 2',*/
					'biaya' => $this->input->post('Sem2')
				)
			);
			//$this->db->update('biaya', $data, array('kd_pelajaran' => $kode));
			$this->db->update_batch('biaya', $data, 'kd_biaya'); 
		}
	}
	
	function delete_pelajaran($kode){
		$this->db->delete('biaya',array('kd_pelajaran' => $kode));
		$this->db->delete('pelajaran', array('kd_pelajaran' => $kode));
	}

}


?>