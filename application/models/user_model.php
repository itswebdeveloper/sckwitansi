<?php
class user_model extends CI_Model{
	
	function user_model(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->database();
	}

	function general(){		
		//$this->load->library('session');								
		$this->load->library('user_navigation');
		$navigation = new user_navigation;
		
		$data['navigation'] 	=  $navigation -> show_menu();
		$data['title']		 = 'Sumatra Conservatoire User';		
		$data['footer']		=''; //'Copyright &copy; 2011. <font class="sumatracfont">Sumatra Conservatoire</font>. All Rights Reserved.';
		
		return $data;
	}

	function change_kwitansi_status($status, $jenis_kwitansi, $no_kwitansi){
		($jenis_kwitansi == 'barang') ? $table = 'm_kwitansibrg' : $table = 'm_kwitansipljr' ;
		$data = array('status'=>$status);
		$this->db->where('no_kwitansi', $no_kwitansi);
		$this->db->update($table,$data);
	}

	function get_selected_mkwitansi($no_kwitansi,$jenis_kwitansi){				
		if($jenis_kwitansi == 'barang'){			
			$this->db->where('no_kwitansi',$no_kwitansi);
			$query = $this->db->get('m_kwitansibrg');
		}else{
			/*$this->db->select('nim');
			$this->db->where('no_kwitansi',$no_kwitansi);
			$kwitansi = $this->db->get('m_kwitansipljr')->row_array();
			$nim = $kwitansi['nim'];
			if(substr($nim, 0,2) == "N_"){
				$this->db->select('m_kwitansipljr.*,murid_baru.nm_murid');
				$this->db->join('murid_baru','m_kwitansipljr.nim = murid_baru.nim');	
			}else{
				$this->db->select('m_kwitansipljr.*,murid.nm_murid');
				$this->db->join('murid','m_kwitansipljr.nim = murid.nim');
			}*/
			
			$this->db->where('no_kwitansi',$no_kwitansi);
			$query = $this->db->get('m_kwitansipljr');
		}
		return $query->row_array();
	}

	//kwitansibarang
	function set_kwitansi_barang($pembelian){
		$tanggal_kwitansi = date('Y-m-d',strtotime($pembelian['tanggal']));
		$last_kwitansi = $this->get_last_kwitansi_barang();		
		if($last_kwitansi === false){
			$no_kwitansi = 'B00001/'.date('Y');
		}else{
			$old_no_kwitansi = $last_kwitansi['no_kwitansi'];
			$split = explode('/',$old_no_kwitansi);
			if((int)$split[1] < (int)date('Y')){
				$no_kwitansi = 'B00001/'.date('Y');
			}else{
				$no = substr($old_no_kwitansi,1,5);
				$length = strlen(((int)$no)+1);
				$no = ((int)$no) + 1;
				$no_kwitansi = 'B'.str_repeat('0',5-$length).$no.'/'.$split[1];
			}			
		}	
			
		$m_kwitansi = array(
			'no_kwitansi' => $no_kwitansi,
			'tgl_kwitansi' => $tanggal_kwitansi,
			'nim' => $pembelian['nim'],
			'nama' => $pembelian['nama'],
			'total' => $pembelian['total'],
			'keterangan' => ''
		);		
		if($this->db->insert('m_kwitansibrg',$m_kwitansi)){
			$data = array();
			foreach($pembelian['uraian'] as $row){
				$urai = array(
					'no_kwitansi' => $no_kwitansi,
					'tgl_kwitansi' => $tanggal_kwitansi,
					'kd_barang' => $row['kd_barang'],
					'harga' => $row['harga'],
					'qty' => $row['quantity'],
					'jumlah' => $row['stotal']
				);
				array_push($data,$urai);
			}
			
			$this->db->insert_batch('p_kwitansibrg', $data);
			
			return $no_kwitansi;
		}
	}

	function get_last_kwitansi_barang(){
		$query = $this->db->query("select * from m_kwitansibrg order by tgl_kwitansi desc, no_kwitansi desc limit 1");
		
		if ($query->num_rows() <= 0)
			return false;
		else
			return $query->row_array();
	}

	function get_count_mkwitansi_barang(){
		$this->db->order_by("no_kwitansi","desc");
		$query = $this->db->get("m_kwitansibrg");
		return $query->num_rows();
	}

	function get_mkwitansi_barang($page,$row){		
		$this->db->limit($row,$page);
		$this->db->order_by("tgl_kwitansi","desc");
		$this->db->order_by("no_kwitansi","desc");
		$query = $this->db->get("m_kwitansibrg");				
		return $query;
	}

	//kwitansipelajaran
	function set_kwitansi_pelajaran($pembelian){
		//check nim first
		if($pembelian['nim'] == ""){
			$this->load->model('master_model');
			$murid_baru = $this->master_model->add_new_murid($pembelian['nama']);
			$pembelian['nim'] = $murid_baru;
		}

		$tanggal_kwitansi = date('Y-m-d',strtotime($pembelian['tanggal']));
		$last_kwitansi = $this->get_last_kwitansi_pelajaran();
		if($last_kwitansi === false){
			$no_kwitansi = 'U00001/'.date('Y');
		}else{
			$old_no_kwitansi = $last_kwitansi['no_kwitansi'];
			$split = explode('/',$old_no_kwitansi);
			if((int)$split[1] < (int)date('Y')){
				$no_kwitansi = 'U00001/'.date('Y');
			}else{
				$no = substr($old_no_kwitansi,1,5);
				$length = strlen(((int)$no)+1);
				$no = ((int)$no) + 1;
				$no_kwitansi = 'U'.str_repeat('0',5-$length).$no.'/'.$split[1];
			}			
		}
		
		$m_kwitansi = array(
			'no_kwitansi' => $no_kwitansi,
			'tgl_kwitansi' => $tanggal_kwitansi,
			'nim' => $pembelian['nim'],
			'nama' => $pembelian['nama'],
			'subtotal' => $pembelian['subtotal'],
			'nilai_diskon' => $pembelian['nilai_diskon'],
			'nilai_denda' => $pembelian['nilai_denda'],
			'kurang_bayar' => $pembelian['kurang_bayar'],
			'lebih_bayar' => $pembelian['lebih_bayar'],			
			'total' => $pembelian['total'],
			'keterangan' => $pembelian['keterangan']
		);		
		if($this->db->insert('m_kwitansipljr',$m_kwitansi)){
			$p_kwitansi = array();
			$diskon = array();
			$denda = array();
			foreach($pembelian['uraian'] as $row){										
				$urai = array(
					'no_kwitansi' => $no_kwitansi,
					'tgl_kwitansi' => $tanggal_kwitansi,
					'kd_biaya' => $row['kd_biaya'],
					'bulan' => $row['bulan'],
					'tahun' => $row['tahun'],
					'jlh_bln' => $row['periode'],
					'biaya' => $row['biaya'],														
					'jumlah' => ($row['biaya'] * $row['periode'])
				);
				array_push($p_kwitansi,$urai);
			}
			$this->db->insert_batch('p_kwitansipljr', $p_kwitansi);

			if(count($pembelian['diskon']) > 0){
				foreach ($pembelian['diskon'] as $row) {
					$urai = array(
						'no_kwitansi' => $no_kwitansi,
						'kd_biaya' => $row['kd_biaya'],
						'kd_diskon' => $row['kd_diskon'],
						'persen_diskon' => $row['persen_diskon'],
						'ket' => $row['ket_diskon']
					);
					array_push($diskon, $urai);
				}
				$this->db->insert_batch('diskon_p_kwitansipljr',$diskon);
			}

			if(count($pembelian['denda']) > 0){
				foreach ($pembelian['denda'] as $row) {
					$urai = array(
						'no_kwitansi' => $no_kwitansi,
						'kd_denda' => $row['kd_denda'],
						'bulan' => $row['bulan'],
						'periode' => $row['periode'],
						'status' => $row['status'],
						'harga' => $row['harga'],
						'ket' => $row['keterangan']
					);
					array_push($denda, $urai);
				}
				$this->db->insert_batch('p_dendakwipljr',$denda);
			}														
			return $no_kwitansi;
		}
	}	

	function get_last_kwitansi_pelajaran(){
		$query = $this->db->query("select * from m_kwitansipljr order by tgl_kwitansi desc, no_kwitansi desc limit 1");
		
		if ($query->num_rows() <= 0)
			return false;
		else
			return $query->row_array();
	}

	function get_selected_pkwitansi($no_kwitansi,$jenis_kwitansi){
		if($jenis_kwitansi == 'barang'){
			$query = $this->db->query("select pkb.*, b.nm_barang from p_kwitansibrg pkb
			inner join barang b on pkb.kd_barang = b.kd_barang
			where pkb.no_kwitansi = '$no_kwitansi'");
		}else{
			$query = $this->db->query("select pkp.*,pkp.biaya pkpbiaya, b.* from p_kwitansipljr pkp
			inner join biaya b on pkp.kd_biaya = b.kd_biaya
			where pkp.no_kwitansi = '$no_kwitansi'");
		}
		
		return $query;
	}

	function get_ket_diskon($no_kwitansi){
		$this->db->select("b.*, dpk.* ");
		$this->db->join("biaya b","b.kd_biaya = dpk.kd_biaya");
		$this->db->where("dpk.no_kwitansi",$no_kwitansi);
		$query = $this->db->get("diskon_p_kwitansipljr dpk");
		return $query;
	}
	function get_ket_denda($no_kwitansi){
		$this->db->where("no_kwitansi", $no_kwitansi);
		$query = $this->db->get("p_dendakwipljr");
		return $query;
	}

	function get_count_mkwitansi_pelajaran(){
		$this->db->order_by("no_kwitansi","desc");
		$query = $this->db->get("m_kwitansipljr");
		return $query->num_rows();
	}

	function get_mkwitansi_pelajaran($page,$row){		
		$this->db->select("*");		
		$this->db->limit($row,$page);
		$this->db->order_by("tgl_kwitansi","desc");
		$this->db->order_by("no_kwitansi","desc");
		$query = $this->db->get("m_kwitansipljr");
		return $query;				
	}

	//barang
	function get_barang($kode = NULL){
		if($kode==NULL){
			$query = $this->db->get('barang');
			return $query;
		}else{
			$this->db->where('kd_barang',$kode);
			$query = $this->db->get('barang');
			return $query->row_array();
		}
	}

	function add_barang(){
		$data = array(
		'kd_barang' => strtoupper($this->input->post('kd_barang')),
		'nm_barang' => $this->input->post('nm_barang'),
		'harga' => $this->input->post('harga')
		);

		$this->db->insert('barang', $data);
	}

	//pelajaran
	function get_pelajaran($kode = NULL){
		if($kode==NULL){
			$this->db->select("pelajaran.*, program.nm_program, tingkat.nm_tingkat");
			$this->db->join('program','program.kd_program = pelajaran.kd_program');
			$this->db->join('tingkat','tingkat.kd_tingkat = pelajaran.kd_tingkat');
			$this->db->order_by("pelajaran.kd_program", "asc"); 
			$this->db->order_by("pelajaran.kd_tingkat", "asc"); 
			$query = $this->db->get('pelajaran');
			return $query;
		}else{
			$this->db->where('kd_pelajaran',$kode);
			$query = $this->db->get('pelajaran');
			return $query->row_array();
		}
	}

	//murid
	function get_suggest_murid($option = array()){
		$this->db->select('nim');
		$this->db->like('nim', $option['keyword'], 'after');
		$query = $this->db->get('murid');
		return $query->result();
	}

	/*function get_diskon_murid($nim){
		$this->db->select("murid.nim, diskon.*");
		$this->db->join('diskon_murid','murid.nim = diskon_murid.nim');
		$this->db->join('diskon','diskon_murid.kd_diskon = diskon.kd_diskon');
		$this->db->where('murid.nim',$nim);
		$query = $this->db->get('murid');
		return $query;
	}*/

	function get_murid($nim = NULL){
		if($nim==NULL){
			$query = $this->db->get('murid');
			return $query;
		}else{
			$this->db->where('nim',$nim);
			$query = $this->db->get('murid');
			return $query->row_array();
		}
	}

	//program
	function get_program($kd_program = NULL){
		if($kd_program==NULL){
			$query = $this->db->get('program');
			return $query;
		}else{
			$this->db->where('kd_program',$kd_program);
			$query = $this->db->get('jenis');
			return $query->row_array();
		}
	}

	//jenis
	function get_jenis($kd_jenis = NULL){
		if($kd_jenis==NULL){
			$query = $this->db->get('jenis');
			return $query;
		}else{
			$this->db->where('kd_jenis',$kd_jenis);
			$query = $this->db->get('jenis');
			return $query->row_array();
		}
	}

	//tingkat
	function get_tingkat($kd_tingkat = NULL){
		if($kd_tingkat==NULL){
			$query = $this->db->get('tingkat');
			return $query;
		}else{
			$this->db->where('kd_tingkat',$kd_tingkat);
			$query = $this->db->get('tingkat');
			return $query->row_array();
		}
	}

	//diskon
	function get_diskon($kd_diskon = NULL){
		if($kd_diskon==NULL){
			$query = $this->db->get('diskon');
			return $query;
		}else{
			$this->db->where('kd_diskon',$kd_diskon);
			$query = $this->db->get('diskon');
			return $query->row_array();
		}
	}

	//diskon
	function get_denda($kd_denda = NULL){
		if($kd_denda==NULL){
			$query = $this->db->get('denda');
			return $query;
		}else{
			$this->db->where('kd_denda',$kd_denda);
			$query = $this->db->get('denda');
			return $query->row_array();
		}
	}

	//biaya
	function get_biaya($kode = NULL){
		if($kode==NULL){
			$this->db->order_by("kd_program","asc");
			$this->db->order_by("jenis_biaya","asc");
			$this->db->order_by("kd_jenis","asc");
			$query = $this->db->get("biaya");
			return $query;
		}else{
			$this->db->where('kd_biaya',$kode);			
			$query = $this->db->get('biaya');
			return $query->row_array();
		}
	}
}

?>