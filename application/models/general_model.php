<?php
class general_model extends CI_Model{
	
	function general_model(){
		parent::__construct();
		$this->load->helper('url');

	}
	
	function general(){		
		$this->load->library('session');								
				
		$data['title']		 = 'Sumatra Conservatoire Kwitansi';		
		$data['footer']		= 'Copyright &copy; 2011. <font class="sumatracfont">Sumatra Conservatoire</font>. All Rights Reserved.';
		
		return $data;
	}		
}

?>