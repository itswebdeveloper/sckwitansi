<?php if( ! defined('BASEPATH')) exit ('No direct script access allowed');

class Migrate_model extends CI_Model{
	function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->database();
	}

	function sync_m_kwitansipljr(){
		$this->db->where('nama','');
		$kwitansi = $this->db->get('m_kwitansipljr');
		$update = array();
		foreach ($kwitansi->result_array() as $row) {
			if((substr($row['nim'], 0,2)) == "N_"){
				$murid = $this->get_murid_baru($row['nim']);
				$line = array(
						'nim' => $row['nim'],
						'nama' => $murid['nm_murid']
					);
				array_push($update, $line);
			}else{
				$murid = $this->get_murid($row['nim']);
				$line = array(
						'nim' => $row['nim'],
						'nama' => $murid['nm_murid']
					);
				array_push($update, $line);
			}
		}

		return (!$this->db->update_batch('m_kwitansipljr', $update, 'nim')) ? TRUE : FALSE;
	}	

	function sync_biaya_lama(){
		$this->db->where('biaya_lama', NULL);
		$biaya = $this->db->get('biaya');
		$update = array();
		foreach ($biaya->result() as $row) {
			if($row->biaya_lama == NULL){
				$line = array(
						'kd_biaya'	=> $row->kd_biaya,
						'biaya_lama'=> $row->biaya
					);
				array_push($update, $line);
			}
		}

		return (!$this->db->update_batch('biaya', $update, 'kd_biaya')) ? TRUE : FALSE;
	}
	function sync_biaya_modifieddate(){
		$this->db->where("modified_date = '0000-00-00' OR YEAR(modified_date) < ".date("Y"));		
		$biaya = $this->db->get('biaya');

		$update = array();
		if($biaya->num_rows() > 0){
			foreach ($biaya->result() as $row) {
				if($row->modified_date == "0000-00-00"){
					$line = array(
							'kd_biaya'	=> $row->kd_biaya,
							'modified_date'=> date("Y-m-d"),
						);
					array_push($update, $line);
				}
				elseif (date("Y",strtotime($row->modified_date)) < date("Y")) {
					$line = array(
							'kd_biaya'	=> $row->kd_biaya,
							'biaya_lama' => $row->biaya,
							'modified_date'=> date("Y-m-d"),
						);
					array_push($update, $line);
				}
			}
			return (!$this->db->update_batch('biaya', $update, 'kd_biaya')) ? TRUE : FALSE;
		}
	}
	
	function get_murid($nim){
		$this->db->where('nim',$nim);
		return $this->db->get('murid')->row_array();
	}

	function get_murid_baru($nim){	
		$this->db->where('nim',$nim);	
		return $this->db->get('murid_baru')->row_array();
	}

	function check_table_murid_baru(){
		return $this->db->get('murid_baru');
	}
	function check_table_m_kwitansibrg(){
		$this->db->select('status');
		return $this->db->get('m_kwitansibrg');
	}
	function check_table_m_kwitansipljr(){
		$this->db->select('nama','status');
		return $this->db->get('m_kwitansipljr');
	}
	function check_m_kwitansipljr_records(){
		$this->db->where('nama','');
		return $this->db->get('m_kwitansipljr');
	}	
	function check_table_biaya(){
		$this->db->select('biaya_lama, modified_date');
		return $this->db->get('biaya');
	}

	function check_outdated_biaya(){		
		$this->db->select('modified_date');
		$this->db->where('YEAR(modified_date) <', date("Y"));
		return $this->db->get('biaya');
	}
}
?>