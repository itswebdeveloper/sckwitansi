<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class user extends CI_Controller {
	function __construct(){
		parent::__construct();		
		$this->load->helper(array('form','url'));
		$this->load->library('session');
		$this->load->model('master_model');
		$this->check_isvalidated();
	}
	
	private function check_isvalidated(){
		if(! $this->session->userdata('validated'))
			redirect('login');
		else
			if($this->session->userdata('level') === 'user')
					redirect('user/main');						
	}
	
	public function logout(){
		$this->session->sess_destroy();
		redirect('login');
	}
		
	public function index()
	{
		$data = $this->master_model->general();
		if($this->session->userdata('m_success')){
			$data['m_success'] = $this->session->userdata('m_success');
			$this->session->unset_userdata('m_success');
		}
		if($this->session->userdata('m_failed')){
			$data['m_failed'] = $this->session->userdata('m_failed');
			$this->session->unset_userdata('m_failed');
		}
		$data['admin'] = $this->master_model->get_login('admin');
		$data['user'] = $this->master_model->get_login('user');
		
		$this->load->view('master/user',$data);
	}	
	
	public function edit()
	{
		$data = $this->master_model->general();
		$data['admin'] = $this->master_model->get_login('admin');
		$data['user'] = $this->master_model->get_login('user');
		$this->load->library('form_validation');
				
		$this->form_validation->set_rules('name', 'Login Name', 'trim|required|min_length[4]|max_length[50]|xss_clean');
		$this->form_validation->set_rules('oldpass', 'Current Password', 'trim|required|');
		$this->form_validation->set_rules('pass', 'Password', 'min_length[4]|trim|required|matches[re-pass]');
		$this->form_validation->set_rules('re-pass', 'Password Confirmation', 'min_length[4]|required');
		
		if ($this->form_validation->run() == FALSE)
		{
			$this->load->view('master/user',$data);
		}else{
			$stat = $this->master_model->update_login();
			if($stat == true)
				$this->session->set_userdata('m_success',"Login berhasil diubah");
			else
				$this->session->set_userdata('m_failed',"Login tidak berhasil diubah. Silahkan ulangi sekali lagi.");
			redirect(base_url().'index.php/master/user');
		}
	}		
	
	function disp_login($kode){
		$d_login = $this->master_model->get_login($kode);
		
		$data = '<div class="row">
                      <div class="col-md-5"><label for="kd_barang">'.$kode.' Login Name</label></div>
                      <div class="col-md-7">
                      	  <input type="hidden" value='.$d_login['id'].' name="id">
                          <input autofocus type="text" class="form-control" value="'.$d_login['name'].'" name="name">                          
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-5"><label for="nm_barang">Old Password</label></div>
                      <div class="col-md-7">
                          <input type="password" required class="form-control" name="oldpass">                          
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-5"><label for="nm_barang">New Password</label></div>
                      <div class="col-md-7">
                          <input type="password" required class="form-control" name="pass">                          
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-5"><label for="nm_barang">Password Confirmation</label></div>
                      <div class="col-md-7">
                          <input type="password" required class="form-control" name="re-pass">                          
                      </div>
                    </div>';
		echo $data;
	}
}