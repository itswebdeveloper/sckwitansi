<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class barang extends CI_Controller {
	function __construct(){
		parent::__construct();		
		$this->load->helper(array('form','url'));
		$this->load->library('session');
		$this->load->database();
		$this->load->model('master_model');
		$this->check_isvalidated();
	}
	
	private function check_isvalidated(){
		if(! $this->session->userdata('validated'))
			redirect('login');
		else
			if($this->session->userdata('level') === 'user')
					redirect('user/main');						
	}
	
	public function logout(){
		$this->session->sess_destroy();
		redirect('login');
	}
		
	public function index()
	{
		$data = $this->master_model->general();
		if($this->session->userdata('m_success')){
			$data['m_success'] = $this->session->userdata('m_success');
			$this->session->unset_userdata('m_success');
		}
		
		$this->load->view('master/barang',$data);
	}
	
	public function view()
	{
		$data = $this->master_model->general();
		if($this->session->userdata('m_success')){
			$data['m_success'] = $this->session->userdata('m_success');
			$this->session->unset_userdata('m_success');
		}
		$data['barang_list'] = $this->master_model->get_barang();
		
		$this->load->view('master/barang_view',$data);
	}
	
	public function add()
	{
		$data = $this->master_model->general();
		$this->load->library('form_validation');
		
		$this->form_validation->set_rules('kd_barang', 'Kode Barang', 'trim|required|min_length[2]|max_length[10]|xss_clean');
		$this->form_validation->set_rules('nm_barang', 'Nama Barang', 'trim|required|min_length[4]|max_length[50]|xss_clean');
		$this->form_validation->set_rules('harga', 'Harga', 'trim|required|numeric');
		
		if ($this->form_validation->run() == FALSE)
		{
			$this->load->view('master/barang',$data);
		}else{
			$this->master_model->add_barang();
			$this->session->set_userdata('m_success',"Barang berhasil disimpan");
			redirect(base_url().'index.php/master/barang');
		}
	}
	
	public function edit()
	{
		$data = $this->master_model->general();
		$data['barang_list'] = $this->master_model->get_barang();
		$this->load->library('form_validation');
				
		$this->form_validation->set_rules('nm_barang', 'Nama Barang', 'trim|required|min_length[4]|max_length[50]|xss_clean');
		$this->form_validation->set_rules('harga', 'Harga', 'trim|required|numeric');
		
		if ($this->form_validation->run() == FALSE)
		{
			$this->load->view('master/barang',$data);
		}else{
			$this->master_model->update_barang();
			$this->session->set_userdata('m_success',"Barang berhasil disimpan");
			redirect(base_url().'index.php/master/barang');
		}
	}
	
	public function delete($kode){
		$this->master_model->delete_barang($kode);
		$this->session->set_userdata('m_success',"Barang berhasil dihapus");
		redirect(base_url().'index.php/master/barang');
	}
	
	function disp_barang($kode){
		$d_barang = $this->master_model->get_barang($kode);
		
		$data = '<form method="post" action="'.base_url().'index.php/master/barang/edit">
				<div class="modal-header label-default">
	                <a type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</a>
	                <h4 class="modal-title"><span class="fa fa-user fa-3x"></span> Edit Barang</h4>
	            </div>';

		$data.= '<div class="modal-body">
					<div class="row">					  
                      <div class="col-md-5"><label for="kd_barang">Kode barang</label></div>
                      <div class="col-md-7">
                          <input autofocus type="text" readonly class="form-control disabled" value="'.$d_barang['kd_barang'].'" name="kd_barang">                          
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-5"><label for="nm_barang">Nama barang</label></div>
                      <div class="col-md-7">
                          <input type="text" required class="form-control" name="nm_barang" value="'.$d_barang['nm_barang'].'">                          
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-5"><label for="harga">Harga</label></div>
                      <div class="col-md-7">
                          <input autofocus type="number" required class="form-control" name="harga" value="'.$d_barang['harga'].'">
                      </div>
                    </div>
                </div>';
        $data.= '<div class="modal-footer">
	            	<input type="submit" class="btn btn-success" value="Simpan">
	                <input type="reset" class="btn btn-danger" data-dismiss="modal" value="Batal">
	            </div>
	            </form>';
		echo $data;
	}

	public function barang_datatable(){
		$this->load->library('datatables');
		//$this->load->library('jquery');
		//$this->jquery->

		$this->datatables->select('kd_barang, nm_barang, harga')->from('barang');		
		$this->datatables->edit_column('harga','Rp. $1,-','number_format(harga)');
		$this->datatables->add_column('edit', '<a id="$1" data-toggle="modal" data-target="#edit_modal" data-remote="'.base_url().'index.php/master/barang/disp_barang/$1" href="#edit-modal" class="tb-edit btn btn-info btn-embossed"><i class="fa fa-edit"></i></a>
											   <a href="'.base_url().'index.php/master/barang/delete/$1" class="btn btn-danger btn-embossed"><i class="fa fa-trash-o"></i></a></td>','kd_barang');
		
    	echo $this->datatables->generate();		
	}
}