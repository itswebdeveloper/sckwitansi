<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class pelajaran extends CI_Controller {
	function __construct(){
		parent::__construct();		
		$this->load->helper(array('form','url'));
		$this->load->library('session');
		$this->load->model('master_model');
		$this->check_isvalidated();
	}
	
	private function check_isvalidated(){
		if(! $this->session->userdata('validated'))
			redirect('login');
		else
			if($this->session->userdata('level') === 'user')
					redirect('user/main');						
	}
	
	public function logout(){
		$this->session->sess_destroy();
		redirect('login');
	}
		
	public function index()
	{
		$data = $this->master_model->general();
		if($this->session->userdata('m_success')){
			$data['m_success'] = $this->session->userdata('m_success');			
			$this->session->unset_userdata('m_success');
		}else if($this->session->userdata('m_fail')){
			$data['m_fail'] = $this->session->userdata('m_fail');			
			$this->session->unset_userdata('m_fail');
		}
		$data['jenis'] = $this->master_model->get_jenis();
		$data['tingkat'] = $this->master_model->get_tingkat();
		$data['program'] = $this->master_model->get_program();
		$data['subprogram'] = $this->master_model->get_subprogram();
		$data['filter_program'] = $this->filter_program();
		
		$this->load->view('master/pelajaran',$data);
	}
	
	public function view()
	{
		$data = $this->master_model->general();
		if($this->session->userdata('m_success')){
			$data['m_success'] = $this->session->userdata('m_success');
			$this->session->unset_userdata('m_success');
		}
		
		/*$pelajaran_list = $this->master_model->get_pelajaran_by_program();
		
		$data['list'] ='';
		
		foreach($pelajaran_list->result_array() as $row){
			$jenis = $this->master_model->get_jenis($row['kd_jenis']);
			$tingkat = $this->master_model->get_tingkat($row['kd_tingkat']);
			$subprogram = $this->master_model->get_subprogram($row['kd_subprogram']);
			
			$data['list'].= "<tr>
							<td>$row[kd_pelajaran]</td>
							<td>$row[nm_pelajaran]</td>
							<td>$subprogram[nm_subprogram]</td>
							<td>$jenis[nm_jenis]</td>
							<td>$tingkat[nm_tingkat]</td>							
							<td width=14%><a id='$row[kd_pelajaran]' href='#edit_modal' data-toggle='modal' class='tb-edit btn btn-hg btn-info btn-embossed'><i class='fa fa-edit'></i></a>
							<a href='".base_url()."index.php/master/pelajaran/delete/$row[kd_pelajaran]' class='btn btn-danger btn-hg btn-embossed'><i class='fa fa-trash-o'></i></a></td>
						</tr>";
		}*/
		
		$data['jenis'] = $this->master_model->get_jenis();
		$data['tingkat'] = $this->master_model->get_tingkat();
		$data['program'] = $this->master_model->get_program();
		
		$this->load->view('master/pelajaran_view',$data);
	}
	
	public function add()
	{
		$data = $this->master_model->general();
		$this->load->library('form_validation');
		
		$data['jenis'] = $this->master_model->get_jenis();
		$data['tingkat'] = $this->master_model->get_tingkat();
		$data['program'] = $this->master_model->get_program();
		
		/*$this->form_validation->set_rules('kd_pelajaran', 'Kode Pelajaran', 'trim|required|min_length[1]|max_length[10]|xss_clean');
		$this->form_validation->set_rules('nm_pelajaran', 'Nama Pelajaran', 'trim|required|min_length[4]|max_length[50]|xss_clean');*/
		$this->form_validation->set_rules('ket', 'Keterangan', 'trim|max_length[100]|xss_clean');
		$this->form_validation->set_rules('kd_program', 'Kode Program', 'required');
		$this->form_validation->set_rules('kd_subprogram', 'Kode SubProgram', 'required');
		$this->form_validation->set_rules('kd_jenis', 'Kode Jenis', 'required');
		$this->form_validation->set_rules('kd_tingkat', 'Kode Tingkat', 'required');
		$this->form_validation->set_rules('usekolah_standar', 'Uang Sekolah Standar', 'trim|required|numeric|xss_clean');
		$this->form_validation->set_rules('usekolah_khusus', 'Uang Sekolah Khusus', 'trim|numeric|xss_clean');
		$this->form_validation->set_rules('uujian_ganjil', 'Uang Ujian Semester 1', 'trim|numeric|xss_clean');
		$this->form_validation->set_rules('uujian_genap', 'Uang Ujian Semester 2', 'trim|numeric|xss_clean');
		
		if ($this->form_validation->run() == FALSE)
		{
			$this->load->view('master/pelajaran',$data);			
		}else{
			$cek_kode = $this->input->post('kd_program').'-'.$this->input->post('kd_subprogram').'-'.$this->input->post('kd_jenis').'-'.$this->input->post('kd_tingkat');
			
			if($this->master_model->get_pelajaran($cek_kode)){				
				$this->session->set_userdata('m_fail',"Pelajaran dengan Format yang sama sudah pernah dimasukkan...!!!");
			}else{
				$this->master_model->add_pelajaran();
				$this->session->set_userdata('m_success',"Pelajaran berhasil disimpan");				
			}

			redirect(base_url().'index.php/master/pelajaran');
		}
	}
	
	public function edit()
	{
		$data = $this->master_model->general();
		$data['pelajaran_list'] = $this->master_model->get_pelajaran();
		$this->load->library('form_validation');
		
		$data['jenis'] = $this->master_model->get_jenis();
		$data['tingkat'] = $this->master_model->get_tingkat();
		$data['program'] = $this->master_model->get_program();
				
		$this->form_validation->set_rules('USS', 'Uang Sekolah Standar', 'trim|required|numeric|xss_clean');
		$this->form_validation->set_rules('USK', 'Uang Sekolah Khusus', 'trim|numeric|xss_clean');
		$this->form_validation->set_rules('Sem1', 'Uang Ujian Semester 1', 'trim|numeric|xss_clean');
		$this->form_validation->set_rules('Sem2', 'Uang Ujian Semester 2', 'trim|numeric|xss_clean');

		if ($this->form_validation->run() == FALSE)
		{
			$this->load->view('master/pelajaran',$data);
		}else{
			$this->master_model->update_pelajaran();
			$this->session->set_userdata('m_success',"Pelajaran berhasil disimpan");
			redirect(base_url().'index.php/master/pelajaran');
		}
	}
	
	public function delete($kode){
		$this->master_model->delete_pelajaran($kode);
		$this->session->set_userdata('m_success',"Pelajaran berhasil dihapus");
		redirect(base_url().'index.php/master/pelajaran');
	}
	
	function filter_program(){
		$kpr = array(
					'AK' =>array(
								'CL' => array(
											'SF' => array('PD1','PD2','DS1','DS2','MN1','MN2','MN3','LA1','LA2'),
											'HR' => array('HR0'),
											'SJ' => array('SJ0'),
											'AM' => array('AM0'),
											'PN' => array('PD0','DS1','DS2','DS3','DS4','DS5','MN1','MN2','MN3','LA1','LA2'),
											'BL' => array('PD1','DS1','DS2','DS3','DS4','DS5','MN1','MN2','MN3','LA1','LA2'),
											'OF' => array('PK1','PK2'),
											'CM' => array('CM0')
											)
								),
					'NA'=>array(
								'CL' =>array(
											'SF' =>array('PD0','PD1','PD2','DS1','DS2','DS3','DS4','DS5','MN1','MN2','MN3','LA1','LA2'),
											'PN' =>array('PD0','PD1','PD2','DS1','DS2','DS3','DS4','DS5','MN1','MN2','MN3','LA1','LA2'),
											'BL' =>array('PD0','PD1','PD2','DS1','DS2','DS3','DS4','DS5','MN1','MN2','MN3','LA1','LA2'),
											'OF' =>array('PK1','PK2','PD0','PD1','PD2','DS1','DS2','DS3','DS4','DS5','MN1','MN2','MN3','LA1','LA2')
											),
								'AP' =>array(
											'PN' =>array('PD0'),
											'KB' =>array('PD0'),
											'VC' =>array('PD0')
											),
								'AJ'=>array(
											'PN' =>array('PD0'),
											'KB' =>array('PD0'),
											'VC' =>array('PD0')
											),
								'AB'=>array(
											'PN' =>array('PD0'),
											'KB' =>array('PD0'),
											'VC' =>array('PD0')
											),
								'AR'=>array(
											'PN' =>array('PD0'),
											'KB' =>array('PD0'),
											'VC' =>array('PD0')
											),
								'SI' =>array(
											'BL'=>array('BK1','BK2','BK3'),
											'PN'=>array('BK1','BK2','BK3')
											),
								'SK'=>array(
											'BL'=>array('BK1','BK2','BK3')											
											),),
					'AV'=>array(
								'PP' => array(
											'PN'=>array('PD0'),
											'VC'=>array('PD0'),
											'KB'=>array('PD0'),
											'GT'=>array('PD0'),
											 ),
								'AT' => array(
											'MK'=>array('CR0','DW0','SK0','PT0'),
											 ),
								'DC' => array(
											'MD'=>array('PD0'),
											'TD'=>array('PD0'),
											 ),
								'GM'=> array(
											'JW'=>array('PD0'),
											 )
							),
					'AB'=>array(
								'RY'=>array(
											'PN'=>array('GR1','GR2','GR3','GR4','GR5','GR6','GR7','GR8'),
											'BL'=>array('GR1','GR2','GR3','GR4','GR5','GR6','GR7','GR8'),
											'TR'=>array('GR1','GR2','GR3','GR4','GR5','GR6','GR7','GR8')
											)
								)
				);

		return $kpr;
	}

	function disp_pelajaran($kode){
		$d_pelajaran = $this->master_model->get_pelajaran($kode);
		$jenis = $this->master_model->get_jenis($d_pelajaran['kd_jenis']);
		$tingkat = $this->master_model->get_tingkat($d_pelajaran['kd_tingkat']);
		$program = $this->master_model->get_program($d_pelajaran['kd_program']);
		$subprogram = $this->master_model->get_subprogram($d_pelajaran['kd_subprogram']);
		$biaya = $this->master_model->get_biaya_pelajaran($kode);

		$data = '<form method="post" action="'.base_url().'index.php/master/pelajaran/edit">
				<div class="modal-header label-default">
	                <a type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</a>
	                <h4 class="modal-title"><span class="fa fa-user fa-3x"></span> Edit Barang</h4>
	            </div>';
		
		$data.= '<div class="modal-body">
					<div class="row">					  
                      <div class="col-md-5"><label for="kd_pelajaran">Kode pelajaran</label></div>
                      <div class="col-md-7">
                      	  <input type="button" value="'.$d_pelajaran['kd_pelajaran'].'" class="btn btn-block btn-default" style="text-align:left;">
                          <input autofocus type="hidden" value="'.$d_pelajaran['kd_pelajaran'].'" name="kd_pelajaran">                          
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-5"><label for="nm_pelajaran">Nama pelajaran</label></div>
                      <div class="col-md-7">
                      	  <input type="button" value="'.$d_pelajaran['nm_pelajaran'].'" class="btn btn-block btn-default" style="text-align:left;">
                      </div>
                    </div>';
          $data.='  <div class="row">
                      <div class="col-md-5"><label for="kd_program">Kode Program</label></div>
                      <div class="col-md-7">
                      	 <input type="button" value="'.$d_pelajaran['kd_program'].'-'.$program['nm_program'].'" class="btn btn-block btn-info" style="text-align:left">                         
                      </div>
                    </div>';
          $data.='  <div class="row">
                      <div class="col-md-5"><label for="kd_subprogram">Kode SubProgram</label></div>
                      <div class="col-md-7">
                      	 <input type="button" value="'.$d_pelajaran['kd_subprogram'].'-'.$subprogram['nm_subprogram'].'" class="btn btn-block btn-info" style="text-align:left">                         
                      </div>
                    </div>';
          $data.='  <div class="row">
                      <div class="col-md-5"><label for="kd_jenis">Kode Jenis</label></div>
                      <div class="col-md-7">
                      	 <input type="button" value="'.$d_pelajaran['kd_jenis'].'-'.$jenis['nm_jenis'].'" class="btn btn-block btn-info" style="text-align:left">                         
                      </div>
                    </div>';
          $data.='  <div class="row">
                      <div class="col-md-5"><label for="kd_tingkat">Kode Tingkat</label></div>
                      <div class="col-md-7">
                      	 <input type="button" value="'.$d_pelajaran['kd_tingkat'].'-'.$tingkat['nm_tingkat'].'" class="btn btn-block btn-info" style="text-align:left">                         
                      </div>
                    </div>';                    
		  foreach($biaya->result_array() as $row){
			   if($row['jenis_biaya'] == 'USS')
			   		$disp = 'Uang Sekolah Standar';
			   else if($row['jenis_biaya'] == 'USK')
			   		$disp = 'Uang Sekolah Khusus';
			   else if($row['jenis_biaya'] == 'Sem1')
			   		$disp = 'Uang Ujian Semester Ganjil';
			   else if($row['jenis_biaya'] == 'Sem2')
			   		$disp = 'Uang Ujian Semester Genap';
			   $data.='<div class="row">
			   				<div class="col-md-5"><label for="'.$row['jenis_biaya'].'">'.$disp.'</label></div>
							<div class="col-md-7">
								<input type="hidden" name="id'.$row['jenis_biaya'].'" value="'.$row['kd_biaya'].'">
								<input type="number" class="form-control" name="'.$row['jenis_biaya'].'" placeholder="'.$disp.'" value="'.$row['biaya'].'">
							</div>
					   </div>
				';			   
		   }
		   $data.='  <div class="row">
                      <div class="col-md-5"><label for="kd_tingkat">Keterangan</label></div>
                      <div class="col-md-7">
                      	 <input type="text" name="ket" value="'.$d_pelajaran['ket'].'" class="form-control" style="text-align:left">                         
                      </div>
                    </div>';  
		   $data.="</div>";
		   $data.= '<div class="modal-footer">
	            	<input type="submit" class="btn btn-success" value="Simpan">
	                <input type="reset" class="btn btn-danger" data-dismiss="modal" value="Batal">
	            </div>
	            </form>';
		   		
		   $data.=' <script>
					$(function(){
						$("select").selectpicker({style: "btn btn-primary", menuStyle: "dropdown-inverse"});						   
					});
					</script>
					';
		echo $data;
	}

	public function pelajaran_datatable(){
		$this->load->library('datatables');
		//$this->load->library('jquery');
		//$this->jquery->

		$this->datatables->select('kd_pelajaran, nm_pelajaran, ket')->from('pelajaran');				
		$this->datatables->add_column('edit', '<a id="$1" data-toggle="modal" data-target="#edit_modal" data-remote="'.base_url().'index.php/master/pelajaran/disp_pelajaran/$1" href="#edit-modal" class="tb-edit btn btn-info btn-embossed"><i class="fa fa-edit"></i></a>
											   <a href="'.base_url().'index.php/master/pelajaran/delete/$1" class="btn btn-danger btn-embossed"><i class="fa fa-trash-o"></i></a></td>','kd_pelajaran');
		
    	echo $this->datatables->generate();		
	}
}