<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class main extends CI_Controller {
	function __construct(){
		parent::__construct();		
		$this->load->helper(array('form','url'));
		$this->load->library('session');
		$this->load->model('master_model');
		$this->check_isvalidated();
	}

	private function check_isvalidated(){
		if(! $this->session->userdata('validated'))
			redirect('login');
		else
			if($this->session->userdata('level') === 'user')
					redirect('user/main');						
	}
	
	public function logout(){
		$this->session->sess_destroy();
		redirect('login');
	}	
	
	public function index()
	{
		$data = $this->master_model->general();

		
		if(($data['table_repair'] = $this->check_database_upgrade_requirement())){
			$data['murid_baru'] = NULL;
		}else
			$data['murid_baru'] = $this->master_model->count_new_murid()->num_rows();	

		$this->load->view('master/index',$data); 
	}

	function check_database_upgrade_requirement(){
		$this->load->model('migrate_model');
		$errors = FALSE;

		if($this->migrate_model->check_table_murid_baru() == TRUE &&
		   $this->migrate_model->check_table_m_kwitansibrg() == TRUE &&
		   $this->migrate_model->check_table_m_kwitansipljr() == TRUE &&
		   $this->migrate_model->check_table_biaya() == TRUE &&
		   $this->migrate_model->check_outdated_biaya()->num_rows() == 0 &&
		   $this->migrate_model->check_m_kwitansipljr_records()->num_rows() == 0)
		{
			$errors = FALSE;
		}else
			$errors = TRUE;
			
		return $errors;
	}	
}