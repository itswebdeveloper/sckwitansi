<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class denda extends CI_Controller {
	function __construct(){
		parent::__construct();		
		$this->load->helper(array('form','url'));
		$this->load->library('session');
		$this->load->model('master_model');
		$this->check_isvalidated();
	}
	
	private function check_isvalidated(){
		if(! $this->session->userdata('validated'))
			redirect('login');
		else
			if($this->session->userdata('level') === 'user')
					redirect('user/main');						
	}
	
	public function logout(){
		$this->session->sess_destroy();
		redirect('login');
	}
		
	public function index()
	{
		$data = $this->master_model->general();
		if($this->session->userdata('m_success')){
			$data['m_success'] = $this->session->userdata('m_success');
			$this->session->unset_userdata('m_success');
		}
		
		$this->load->view('master/denda',$data);
	}
	
	public function view()
	{
		$data = $this->master_model->general();
		if($this->session->userdata('m_success')){
			$data['m_success'] = $this->session->userdata('m_success');
			$this->session->unset_userdata('m_success');
		}
		$data['denda_list'] = $this->master_model->get_denda();
		
		$this->load->view('master/denda_view',$data);
	}
	
	public function add()
	{
		$data = $this->master_model->general();
		$this->load->library('form_validation');
		
		$this->form_validation->set_rules('kd_denda', 'Kode Denda', 'trim|required|min_length[2]|max_length[10]|xss_clean');
		$this->form_validation->set_rules('nm_denda', 'Nama Denda', 'trim|required|min_length[4]|max_length[50]|xss_clean');
		$this->form_validation->set_rules('harga', 'Harga', 'trim|required|numeric');
		$this->form_validation->set_rules('ket_denda', 'Keterangan Denda', 'trim|max_length[100]');
		
		if ($this->form_validation->run() == FALSE)
		{
			$this->load->view('master/denda',$data);
		}else{
			$this->master_model->add_denda();
			$this->session->set_userdata('m_success',"Denda berhasil disimpan");
			redirect(base_url().'index.php/master/denda');
		}
	}
	
	public function edit()
	{
		$data = $this->master_model->general();
		$data['denda_list'] = $this->master_model->get_denda();
		$this->load->library('form_validation');
				
		$this->form_validation->set_rules('kd_denda', 'Kode Denda', 'trim|required|min_length[2]|max_length[10]|xss_clean');
		$this->form_validation->set_rules('nm_denda', 'Nama Denda', 'trim|required|min_length[4]|max_length[50]|xss_clean');
		$this->form_validation->set_rules('harga', 'Harga', 'trim|required|numeric');
		$this->form_validation->set_rules('ket_denda', 'Keterangan Denda', 'trim');
		
		if ($this->form_validation->run() == FALSE)
		{
			$this->load->view('master/denda',$data);
		}else{
			$this->master_model->update_denda();
			$this->session->set_userdata('m_success',"Denda berhasil disimpan");
			redirect(base_url().'index.php/master/denda');
		}
	}
	
	public function delete($kode){
		$this->master_model->delete_denda($kode);
		$this->session->set_userdata('m_success',"Denda berhasil dihapus");
		redirect(base_url().'index.php/master/denda');
	}
	
	function disp_denda($kode){
		$d_denda = $this->master_model->get_denda($kode);
		
		$data = '<form method="post" action="'.base_url().'index.php/master/denda/edit">
				<div class="modal-header label-default">
	                <a type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</a>
	                <h4 class="modal-title"><span class="fa fa-user fa-3x"></span> Edit Denda</h4>
	            </div>';
		$data.= '<div class="modal-body">
					<div class="row">					  
                      <div class="col-md-5"><label for="kd_denda">Kode denda</label></div>
                      <div class="col-md-7">
                          <input autofocus type="text" readonly class="form-control disabled" value="'.$d_denda['kd_denda'].'" name="kd_denda">                          
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-5"><label for="nm_denda">Nama denda</label></div>
                      <div class="col-md-7">
                          <input type="text" required class="form-control" name="nm_denda" value="'.$d_denda['nm_denda'].'">                          
                      </div>
                    </div>
                    <div class="row">
	                  <div class="col-md-5"><label for="harga">Status</label></div>
	                  <div class="col-md-7">
	                      <select class="select-block" name="status">';
	                      if($d_denda['status'] == "bulanan"){
	                        $data.='<option value="bulanan" selected>Bulanan</option>
	                        	    <option value="semester">Semester</option>';
	                      }else{
	                      	$data.='<option value="bulanan">Bulanan</option>
	                      		    <option value="semester" selected>Semester</option>';
	                      }
	    $data.='
	                      </select>
	                  </div>
	                </div>
                    <div class="row">
                      <div class="col-md-5"><label for="harga">Harga</label></div>
                      <div class="col-md-7">
                          <input autofocus type="number" required class="form-control" name="harga" value="'.$d_denda['harga'].'">
                      </div>
                    </div>
					<div class="row">
                      <div class="col-md-5"><label for="persen">Keterangan</label></div>
                      <div class="col-md-7">
                          <input class="form-control" name="ket_denda" value="'.$d_denda['ket_denda'].'">
                      </div>
                    </div>
                </div>';
        $data.= '<div class="modal-footer">
	            	<input type="submit" class="btn btn-success" value="Simpan">
	                <input type="reset" class="btn btn-danger" data-dismiss="modal" value="Batal">
	            </div>
	            </form>';
        $data.='<script type="text/javascript">
				  $(document).ready(function(){
				    $(".select-block").selectpicker({style: "btn btn-primary", menuStyle: "dropdown-inverse"});
				  })
				</script>';
		echo $data;
	}

	public function denda_datatable(){
		$this->load->library('datatables');
		//$this->load->library('jquery');
		//$this->jquery->

		$this->datatables->select('kd_denda, nm_denda, harga, ket_denda')->from('denda');		
		$this->datatables->edit_column('harga','Rp. $1,-','number_format(harga)');
		$this->datatables->add_column('edit', '<a id="$1" data-toggle="modal" data-target="#edit_modal" data-remote="'.base_url().'index.php/master/denda/disp_denda/$1" href="#edit-modal" class="tb-edit btn btn-info btn-embossed"><i class="fa fa-edit"></i></a>
											   <a href="'.base_url().'index.php/master/denda/delete/$1" class="btn btn-danger btn-embossed"><i class="fa fa-trash-o"></i></a></td>','kd_denda');
		
    	echo $this->datatables->generate();		
	}
}