<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class program extends CI_Controller {
	function __construct(){
		parent::__construct();		
		$this->load->helper(array('form','url'));
		$this->load->library('session');
		$this->load->model('master_model');
		$this->check_isvalidated();
	}
	
	private function check_isvalidated(){
		if(! $this->session->userdata('validated'))
			redirect('login');
		else
			if($this->session->userdata('level') === 'user')
					redirect('user/main');						
	}
	
	public function logout(){
		$this->session->sess_destroy();
		redirect('login');
	}
		
	public function index()
	{
		$data = $this->master_model->general();
		if($this->session->userdata('m_success')){
			$data['m_success'] = $this->session->userdata('m_success');
			$this->session->unset_userdata('m_success');
		}
		
		$this->load->view('master/program',$data);
	}
	
	public function view()
	{
		$data = $this->master_model->general();
		if($this->session->userdata('m_success')){
			$data['m_success'] = $this->session->userdata('m_success');
			$this->session->unset_userdata('m_success');
		}
		$data['program_list'] = $this->master_model->get_program();
		
		$this->load->view('master/program_view',$data);
	}
	
	public function add()
	{
		$data = $this->master_model->general();
		$this->load->library('form_validation');
		
		$this->form_validation->set_rules('kd_program', 'Kode Program', 'trim|required|min_length[1]|max_length[3]|xss_clean');
		$this->form_validation->set_rules('nm_program', 'Nama Program', 'trim|required|min_length[4]|max_length[35]|xss_clean');
		
		if ($this->form_validation->run() == FALSE)
		{
			$this->load->view('master/program',$data);
		}else{
			$this->master_model->add_program();
			$this->session->set_userdata('m_success',"Program berhasil disimpan");
			redirect(base_url().'index.php/master/program');
		}
	}
	
	public function edit()
	{
		$data = $this->master_model->general();
		$data['program_list'] = $this->master_model->get_program();
		$this->load->library('form_validation');
				
		$this->form_validation->set_rules('nm_program', 'Nama Program', 'trim|required|min_length[4]|max_length[35]|xss_clean');
		
		if ($this->form_validation->run() == FALSE)
		{
			$this->load->view('master/program',$data);
		}else{
			$this->master_model->update_program();
			$this->session->set_userdata('m_success',"Program berhasil disimpan");
			redirect(base_url().'index.php/master/program');
		}
	}
	
	public function delete($kode){
		$this->master_model->delete_program($kode);
		$this->session->set_userdata('m_success',"Program berhasil dihapus");
		redirect(base_url().'index.php/master/program');
	}
	
	function disp_program($kode){
		$d_program = $this->master_model->get_program($kode);
		
		$data = '<form method="post" action="'.base_url().'index.php/master/program/edit">
				<div class="modal-header label-default">
	                <a type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</a>
	                <h4 class="modal-title"><span class="fa fa-user fa-3x"></span> Edit Barang</h4>
	            </div>';
		$data.= '<div class="modal-body">
					<div class="row">					  
                      <div class="col-md-5"><label for="kd_program">Kode program</label></div>
                      <div class="col-md-7">
                          <input autofocus type="text" readonly class="form-control disabled" value="'.$d_program['kd_program'].'" name="kd_program">                          
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-5"><label for="nm_program">Nama program</label></div>
                      <div class="col-md-7">
                          <input type="text" required class="form-control" name="nm_program" value="'.$d_program['nm_program'].'">                          
                      </div>
                    </div>
                </div>';
         $data.= '<div class="modal-footer">
	            	<input type="submit" class="btn btn-success" value="Simpan">
	                <input type="reset" class="btn btn-danger" data-dismiss="modal" value="Batal">
	            </div>
	            </form>';
		echo $data;
	}

	public function program_datatable(){
		$this->load->library('datatables');
		//$this->load->library('jquery');
		//$this->jquery->

		$this->datatables->select('kd_program, nm_program')->from('program');				
		$this->datatables->add_column('edit', '<a id="$1" data-toggle="modal" data-target="#edit_modal" data-remote="'.base_url().'index.php/master/program/disp_program/$1" href="#edit-modal" class="tb-edit btn btn-info btn-embossed"><i class="fa fa-edit"></i></a>
											   <a href="'.base_url().'index.php/master/program/delete/$1" class="btn btn-danger btn-embossed"><i class="fa fa-trash-o"></i></a></td>','kd_program');
		
    	echo $this->datatables->generate();		
	}
}