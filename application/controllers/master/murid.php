<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class murid extends CI_Controller {
	function __construct(){
		parent::__construct();		
		$this->load->helper(array('form','url'));
		$this->load->library('session');
		$this->load->model('master_model');
		$this->check_isvalidated();
	}
	
	private function check_isvalidated(){
		if(! $this->session->userdata('validated'))
			redirect('login');
		else
			if($this->session->userdata('level') === 'user')
					redirect('user/main');						
	}
	
	public function logout(){
		$this->session->sess_destroy();
		redirect('login');
	}
		
	public function index()
	{
		$data = $this->master_model->general();
		$data['murid_baru'] = $this->master_model->count_new_murid()->num_rows();

		if($this->session->userdata('m_success')){
			$data['m_success'] = $this->session->userdata('m_success');
			$this->session->unset_userdata('m_success');
		}
		if($this->session->userdata('m_fail')){
			$data['m_fail'] = $this->session->userdata('m_fail');
			$this->session->unset_userdata('m_fail');
		}
				
		$this->load->view('master/murid',$data);
	}
	
	public function view()
	{
		$data = $this->master_model->general();
		if($this->session->userdata('m_success')){
			$data['m_success'] = $this->session->userdata('m_success');
			$this->session->unset_userdata('m_success');
		}
		$data['murid_list'] = $this->master_model->get_murid();
		
		$this->load->view('master/murid_view',$data);
	}
	
	public function add()
	{
		$data = $this->master_model->general();
		$this->load->library('form_validation');
		
		$this->form_validation->set_rules('nim', 'NIM', 'trim|required|min_length[2]|max_length[10]|xss_clean|alpha_dash');
		$this->form_validation->set_rules('nm_murid', 'Nama Murid', 'trim|required|min_length[2]|max_length[50]|xss_clean');
		$this->form_validation->set_rules('ket', 'Keterangan', 'trim|max_length[100]');
		
		if ($this->form_validation->run() == FALSE)
		{
			$this->load->view('master/murid',$data);
		}else{			
			$cek_kode = $this->input->post('nim');
			
			if($this->master_model->get_murid($cek_kode)){				
				$this->session->set_userdata('m_fail',"Murid dengan nim tersebut telah ada");
			}else{
				if($this->master_model->add_murid())
					$this->session->set_userdata('m_success',"Murid berhasil disimpan");				
				else
					$this->session->set_userdata('m_fail',"Murid tidak berhasil disimpan");
			}

			redirect(base_url().'index.php/master/murid');
		}
	}
	
	public function edit()
	{
		$data = $this->master_model->general();
		$data['murid_list'] = $this->master_model->get_murid();
		$this->load->library('form_validation');
				
		$this->form_validation->set_rules('nm_murid', 'Nama Murid', 'trim|required|min_length[2]|max_length[50]|xss_clean');
		$this->form_validation->set_rules('ket', 'Keterangan', 'trim|max_length[100]');
		
		if ($this->form_validation->run() == FALSE)
		{
			$this->load->view('master/murid/view',$data);
		}else{
			$this->master_model->update_murid();
			$this->session->set_userdata('m_success',"Murid berhasil diubah");
			redirect(base_url().'index.php/master/murid/view');
		}
	}
	
	public function delete($nim){
		$this->master_model->delete_murid($nim);
		$this->session->set_userdata('m_success',"Murid berhasil dihapus");
		redirect(base_url().'index.php/master/murid/view');
	}

	function disp_murid($nim){
		$d_murid = $this->master_model->get_murid($nim);
		$diskon = $this->master_model->get_diskon();
		//$diskon_murid = $this->master_model->get_diskon_murid($nim);
		
		$table = "'ListTable'";
		$kdval = '"+kd_barang.value+"';
		$delete = "deleteRow('".$kdval."',".$table.")";
		
		$data = '<form method="post" action="'.base_url().'index.php/master/murid/edit">
				<div class="modal-header label-default">
	                <a type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</a>
	                <h4 class="modal-title"><span class="fa fa-user fa-3x"></span> Edit Barang</h4>
	            </div>';

		$data.= '<div class="modal-body">
					<div class="row">					  
                      <div class="col-md-5"><label for="kd_murid">NIM</label></div>
                      <div class="col-md-7">
                          <input autofocus type="text" readonly class="form-control disabled" value="'.$d_murid['nim'].'" name="nim">                          
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-5"><label for="nm_murid">Nama murid</label></div>
                      <div class="col-md-7">
                          <input type="text" required class="form-control" name="nm_murid" value="'.$d_murid['nm_murid'].'">                          
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-5"><label for="harga">Keterangan</label></div>
                      <div class="col-md-7">
                          <textarea class="form-control" name="ket">'.$d_murid['ket'].'</textarea>
                      </div>
                    </div>
				</div>';
		$data.= '<div class="modal-footer">
	            	<input type="submit" class="btn btn-success" value="Simpan">
	                <input type="reset" class="btn btn-danger" data-dismiss="modal" value="Batal">
	            </div>
	            </form>';
		echo $data;
	}

	public function murid_datatable(){
		$this->load->library('datatables');
		//$this->load->library('jquery');
		//$this->jquery->

		$this->datatables->select('nim, nm_murid, ket')->from('murid');				
		$this->datatables->add_column('edit', '<a id="$1" data-toggle="modal" data-target="#edit_modal" data-remote="'.base_url().'index.php/master/murid/disp_murid/$1" href="#edit-modal" class="tb-edit btn btn-info btn-embossed"><i class="fa fa-edit"></i></a>
											   <a href="'.base_url().'index.php/master/murid/delete/$1" class="btn btn-danger btn-embossed"><i class="fa fa-trash-o"></i></a></td>','nim');
		
    	echo $this->datatables->generate();		
	}

	public function murid_baru(){
		$data = $this->master_model->general();
		
		if($this->session->userdata('m_success')){
			$data['m_success'] = $this->session->userdata('m_success');
			$this->session->unset_userdata('m_success');
		}
		if($this->session->userdata('m_failed')){
			$data['m_failed'] = $this->session->userdata('m_failed');
			$this->session->unset_userdata('m_failed');
		}
		$data['murid_baru'] = $this->master_model->count_new_murid();
		
		$this->load->view('master/murid_baru_view',$data);
	}

	public function disp_murid_baru($nim){
		$murid_baru = $this->master_model->view_new_murid($nim);		

		$data = '<div class="row">					  
                  <div class="col-md-5"><label for="kd_murid">NIM</label></div>
                  <div class="col-md-7">
                      <input autofocus type="text" readonly class="form-control disabled" value="'.$murid_baru['nim'].'" name="nim">                          
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-5"><label for="nim_baru">NIM baru</label></div>
                  <div class="col-md-7">
                      <input type="text" required class="form-control" name="nim_baru" value="'.$murid_baru['nim_baru'].'">                          
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-5"><label for="nm_murid">Nama murid</label></div>
                  <div class="col-md-7">
                      <input type="text" required class="form-control" name="nm_murid" value="'.$murid_baru['nm_murid'].'">                          
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-5"><label for="harga">Keterangan</label></div>
                  <div class="col-md-7">
                      <textarea class="form-control" name="ket">'.$murid_baru['ket'].'</textarea>
                  </div>
                </div>
				';		
		echo $data;
		$this->show_affected_kwitansi($nim);
	}

	public function show_affected_kwitansi($nim){
		$related_kwitansi = $this->master_model->murid_kwitansipljr_relation($nim);
		$data ='';
		$data.= '<div class="row">
					<h5>Affected Kwitansi Pelajaran</h5>';
		if($related_kwitansi != NULL)
			foreach ($related_kwitansi->result_array() as $row) {
				$data.='<span class="label label-primary">'.$row['no_kwitansi'].'</span>';	
			}
		$data.= '</div>';

		$related_kwitansi = $this->master_model->murid_kwitansibrg_relation($nim);
		$data.= '<div class="row">
					<h5>Affected Kwitansi Barang</h5>';
		if($related_kwitansi != NULL)
			foreach ($related_kwitansi->result_array() as $row) {
				$data.='<span class="label label-primary">'.$row['no_kwitansi'].'</span>';	
			}
		$data.= '</div>';

		echo $data;
	}

	public function move_murid_baru(){
		$data = $this->master_model->general();
		$data['murid_baru'] = $this->master_model->count_new_murid();

		$this->load->library('form_validation');
		$this->form_validation->set_rules('nim_baru', 'NIM Baru', 'trim|required|min_length[2]|max_length[10]|xss_clean|alpha_dash');
		$this->form_validation->set_rules('nm_murid', 'Nama Murid', 'trim|required|min_length[2]|max_length[50]|xss_clean');
		$this->form_validation->set_rules('ket', 'Keterangan', 'trim|max_length[100]');
		
		if ($this->form_validation->run() == FALSE)
		{
			$this->load->view('master/murid_baru_view',$data);
		}else{			
			if($this->master_model->move_murid_baru())
				$this->session->set_userdata('m_success',"Murid Baru berhasil dipindahkan ke murid");				
			else
				$this->session->set_userdata('m_fail',"Murid baru tidak berhasil dipindahkan");					
			redirect(base_url().'index.php/master/murid/murid_baru');
		}
	}
	public function delete_murid_baru($nim){
		$update = $this->master_model->delete_murid_baru($nim);
		($update) ? $this->session->set_userdata('m_success',"Murid berhasil dihapus") : $this->session->set_userdata('m_failed',"Murid gagal dihapus");
		redirect(base_url().'index.php/master/murid/murid_baru');
	}
}