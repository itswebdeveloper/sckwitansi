<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class biaya extends CI_Controller {
	function __construct(){
		parent::__construct();		
		$this->load->helper(array('form','url'));
		$this->load->library('session');
		$this->load->model('master_model');
		$this->check_isvalidated();
	}

	private function check_isvalidated(){
		if(! $this->session->userdata('validated'))
			redirect('login');
		else
			if($this->session->userdata('level') === 'user')
					redirect('user/main');						
	}
	
	public function logout(){
		$this->session->sess_destroy();
		redirect('login');
	}
		
	public function index()
	{
		$data = $this->master_model->general();
		if($this->session->userdata('m_success')){
			$data['m_success'] = $this->session->userdata('m_success');
			$this->session->unset_userdata('m_success');
		}
		
		$data['pelajaran'] = $this->master_model->get_jenis();
		$data['program'] = $this->master_model->get_program();
		//$data['tingkat'] = $this->master_model->get_tingkat();
		
		$this->load->view('master/biaya',$data);
	}
	
	public function view()
	{
		$data = $this->master_model->general();
		if($this->session->userdata('m_success')){
			$data['m_success'] = $this->session->userdata('m_success');
			$this->session->unset_userdata('m_success');
		}
		$biaya_list = $this->master_model->get_biaya();
		/*$data['list'] ='';
		$color = array('USS'=>'success','USK'=>'success','Sem1'=>'success','Sem2'=>'success',
		'adm'=>'warning','daftar'=>'info','test'=>'danger');
		foreach($biaya_list->result_array() as $row){						
			$tingkat = $this->master_model->get_tingkat($row['kd_tingkat']);
			$program = $this->master_model->get_program($row['kd_program']);
			$pelajaran = $this->master_model->get_pelajaran($row['kd_pelajaran']);
			$jenis = $this->master_model->get_jenis($row['kd_jenis']);			
	
			$data['list'].= "<tr class='".$color[$row['jenis_biaya']]."'>";
			$data['list'].= "	<td class=''>$row[jenis_biaya]</td>";
			if($row['kd_program'] == NULL)
				$data['list'].= "	<td></td>";
			else
				$data['list'].= "	<td>$program[nm_program]</td>";
			$data['list'].= "	<td>$row[nm_biaya]</td>	";
			if($row['kd_jenis'] == NULL)
				$data['list'].= "	<td></td>";
			else
				$data['list'].= "	<td>$jenis[nm_jenis]</td>";	
						
			$data['list'].= "	<td>$row[kd_tingkat]</td>";
			$data['list'].= "	<td>Rp. ".number_format($row['biaya'])."</td>";
			$data['list'].= "	<td width=14%><a id='$row[kd_biaya]' href='#edit_modal' data-toggle='modal' class='tb-edit btn btn-hg btn-info btn-embossed'><i class='fa fa-edit'></i></a> ";
			$data['list'].= "	<a href='".base_url()."index.php/master/biaya/delete/$row[kd_biaya]' class='btn btn-danger btn-hg btn-embossed'><i class='fa fa-trash-o'></i></a></td>";
			$data['list'].="</tr>";
		}*/
		$data['biaya_list'] = $biaya_list;
		
		$this->load->view('master/biaya_view',$data);
	}
	
	public function add()
	{
		$data = $this->master_model->general();
		$this->load->library('form_validation');
		$data['pelajaran'] = $this->master_model->get_jenis();
		$data['program'] = $this->master_model->get_program();
		
		//$this->form_validation->set_rules('kd_biaya', 'Kode Biaya', 'trim|required|min_length[2]|max_length[3]|xss_clean');
		$this->form_validation->set_rules('nm_biaya', 'Nama Biaya', 'trim|required|min_length[4]|max_length[50]|xss_clean');
		$this->form_validation->set_rules('biaya', 'Biaya', 'trim|required|numeric');
		$this->form_validation->set_rules('jenis_biaya', 'Jenis Biaya', 'trim|required');
		if($this->input->post('jenis_biaya') == 'adm' || $this->input->post('jenis_biaya') == 'daftar'){
			$this->form_validation->set_rules('kd_program', 'Program', 'trim|required');
		}elseif($this->input->post('jenis_biaya') == 'test'){
			$this->form_validation->set_rules('kd_jenis', 'Jenis Pelajaran', 'trim|required');
		}
		
		if ($this->form_validation->run() == FALSE)
		{
			$this->load->view('master/biaya',$data);
		}else{
			$this->master_model->add_biaya();
			$this->session->set_userdata('m_success',"Biaya berhasil disimpan");
			redirect(base_url().'index.php/master/biaya');
		}
	}
	
	public function edit()
	{
		$data = $this->master_model->general();
		$data['biaya_list'] = $this->master_model->get_biaya();
		$this->load->library('form_validation');
				
		$this->form_validation->set_rules('nm_biaya', 'Nama Biaya', 'trim|required|min_length[4]|max_length[50]|xss_clean');
		$this->form_validation->set_rules('biaya', 'Biaya', 'trim|required|numeric');
		
		if ($this->form_validation->run() == FALSE)
		{
			$this->load->view('master/biaya',$data);
		}else{
			$this->master_model->update_biaya();
			$this->session->set_userdata('m_success',"Biaya berhasil disimpan");
			redirect(base_url().'index.php/master/biaya');
		}
	}
	
	public function delete($kode){
		$this->master_model->delete_biaya($kode);
		$this->session->set_userdata('m_success',"Biaya berhasil dihapus");
		redirect(base_url().'index.php/master/biaya');
	}
	
	function disp_biaya($kode){
		$d_biaya = $this->master_model->get_biaya($kode);
		
		$data = '<form method="post" action="'.base_url().'index.php/master/biaya/edit">
				<div class="modal-header label-default">
	                <a type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</a>
	                <h4 class="modal-title"><span class="fa fa-user fa-3x"></span> Edit Biaya</h4>
	            </div>';
		$data.= '<div class="modal-body">
					<div class="row">					  
                      <div class="col-md-5"><label for="kd_biaya">Kode biaya</label></div>
                      <div class="col-md-7">
                          <input autofocus type="text" readonly class="form-control disabled" value="'.$d_biaya['kd_biaya'].'" name="kd_biaya">                          
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-5"><label for="nm_biaya">Nama biaya</label></div>
                      <div class="col-md-7">
                          <input type="text" required class="form-control" name="nm_biaya" value="'.$d_biaya['nm_biaya'].'">                          
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-5"><label for="biaya">Biaya</label></div>
                      <div class="col-md-7">
                          <input autofocus type="number" required class="form-control" name="biaya" value="'.$d_biaya['biaya'].'">
                      </div>
                    </div>
                </div>';
		$data.= '<div class="modal-footer">
	            	<input type="submit" class="btn btn-success" value="Simpan">
	                <input type="reset" class="btn btn-danger" data-dismiss="modal" value="Batal">
	            </div>
	            </form>';
		echo $data;

	}

	public function biaya_datatable(){
		$this->load->library('datatables');
		//$this->load->library('jquery');
		//$this->jquery->

		$this->datatables->select('kd_biaya, jenis_biaya, kd_pelajaran, nm_biaya, kd_program, kd_tingkat, kd_jenis, biaya')->from('biaya');				
		$this->datatables->edit_column('biaya','Rp. $1,-','number_format(biaya)');
		$this->datatables->add_column('edit', '<a id="$1" data-toggle="modal" data-target="#edit_modal" data-remote="'.base_url().'index.php/master/biaya/disp_biaya/$1" href="#edit-modal" class="tb-edit btn btn-info btn-embossed"><i class="fa fa-edit"></i></a></td>','kd_biaya');
		
    	echo $this->datatables->generate();		
	}
}