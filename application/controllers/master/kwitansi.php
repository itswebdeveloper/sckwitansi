<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class kwitansi extends CI_Controller {
	function __construct(){
		parent::__construct();		
		$this->load->helper(array('form','url'));
		$this->load->library('session');
		$this->load->database();
		$this->load->model('master_model');
		$this->check_isvalidated();
	}
	
	private function check_isvalidated(){
		if(! $this->session->userdata('validated'))
			redirect('login');
		else
			if($this->session->userdata('level') === 'user')
					redirect('user/main');						
	}
	
	public function logout(){
		$this->session->sess_destroy();
		redirect('login');
	}
		
	public function index()
	{		
		redirect('master/main');
	}

	public function barang(){
		$data = $this->master_model->general();
		if($this->session->userdata('m_success')){
			$data['m_success'] = $this->session->userdata('m_success');
			$this->session->unset_userdata('m_success');
		}
		$data['tag'] = 'Barang';
		$data['ajax_source'] = base_url().'index.php/master/kwitansi/kwitansi_datatable/m_kwitansibrg';
		$this->load->view('master/kwitansi',$data);
	}
	public function pelajaran(){
		$data = $this->master_model->general();
		if($this->session->userdata('m_success')){
			$data['m_success'] = $this->session->userdata('m_success');
			$this->session->unset_userdata('m_success');
		}
		$data['tag'] = 'Barang';
		$data['ajax_source'] = base_url().'index.php/master/kwitansi/kwitansi_datatable/m_kwitansipljr';
		$this->load->view('master/kwitansi',$data);
	}	
	
	public function change_status(){
		$no = $this->input->get('no');
		(substr($no, 0, 1) == 'B') ? $kwitansi = "barang" : $kwitansi = "pelajaran" ;
		$status = $this->input->get('status');

		$update = $this->master_model->set_kwitansi_status($no, $status);
		if($update){
			$this->session->set_userdata('m_success',"Kwitansi berhasil disimpan $no");
			redirect(base_url().'index.php/master/kwitansi/'.$kwitansi);
		}else{
			redirect(base_url().'index.php/master/kwitansi/'.$kwitansi);
		}
	}

	function disp_kwitansi(){		
		$this->load->model('user_model');
		$no_kwitansi = $this->input->get('no');
		(substr($no_kwitansi, 0, 1) == 'B') ? $kwitansi = "barang" : $kwitansi = "pelajaran" ;
		
		$m_kwitansi = $this->user_model->get_selected_mkwitansi($no_kwitansi, $kwitansi);
		
		$data = '<div class="modal-header label-default">
	                <a type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</a>
	                <h4 class="modal-title"><span class="fa fa-user fa-3x"></span> Kwitansi '.$no_kwitansi.'</h4>
	            </div>';

		$data.= '<div class="modal-body">
					<h4>Pilih Aksi</h4>
					<a class="btn btn-info" target="_blank" href="'.base_url().'index.php/user/pdfprint/'.$kwitansi.'/?nomor='.$no_kwitansi.'"><i class="fa fa-eye fa-2x"></i> Lihat Kwitansi</a> ';
	    if($m_kwitansi['status'] == '' || $m_kwitansi['status'] == "REQBATAL")
			$data.= '	<a class="btn btn-danger" href="change_status?no='.$no_kwitansi.'&status=BATAL"><i class="fa fa-edit fa-2x"></i> SET To BATAL</a> ';
		else
			$data.= '	<a class="btn btn-info" href="change_status?no='.$no_kwitansi.'&status="><i class="fa fa-edit fa-2x"></i> Hapus Status</a> ';

        $data.= '</div>';

        $data.= '<div class="modal-footer">	            	
	                <input type="reset" class="btn btn-default" data-dismiss="modal" value="Close">
	            </div>';
		echo $data;
	}		

	public function kwitansi_datatable($kwitansi){
		$this->load->library('datatables');
		//$this->load->library('jquery');
		//$this->jquery->

		$this->datatables->select('no_kwitansi, tgl_kwitansi, nim, nama, total, status')->from($kwitansi);		
		$this->datatables->edit_column('no_kwitansi','<a data-toggle="modal" data-target="#edit_modal" data-remote="'.base_url().'index.php/master/kwitansi/disp_kwitansi/?no=$1" href="#edit-modal" class="btn btn-info">$1</a>','no_kwitansi');
		$this->datatables->edit_column('total','Rp. $1,-','number_format(total)');
		$this->datatables->edit_column('status','<a class="$1 btn">$1</a>','status');		
		
    	echo $this->datatables->generate();		
	}
}