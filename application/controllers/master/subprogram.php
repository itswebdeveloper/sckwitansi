<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class subprogram extends CI_Controller {
	function __construct(){
		parent::__construct();		
		$this->load->helper(array('form','url'));
		$this->load->library('session');
		$this->load->model('master_model');
		$this->check_isvalidated();
	}

	private function check_isvalidated(){
		if(! $this->session->userdata('validated'))
			redirect('login');
		else
			if($this->session->userdata('level') === 'user')
					redirect('user/main');						
	}
	
	public function logout(){
		$this->session->sess_destroy();
		redirect('login');
	}
		
	public function index()
	{
		$data = $this->master_model->general();
		if($this->session->userdata('m_success')){
			$data['m_success'] = $this->session->userdata('m_success');
			$this->session->unset_userdata('m_success');
		}
		
		$this->load->view('master/subprogram',$data);
	}
	
	public function view()
	{
		$data = $this->master_model->general();
		if($this->session->userdata('m_success')){
			$data['m_success'] = $this->session->userdata('m_success');
			$this->session->unset_userdata('m_success');
		}
		$data['subprogram_list'] = $this->master_model->get_subprogram();
		
		$this->load->view('master/subprogram_view',$data);
	}
	
	public function add()
	{
		$data = $this->master_model->general();
		$this->load->library('form_validation');
		
		$this->form_validation->set_rules('kd_subprogram', 'Kode Subprogram', 'trim|required|min_length[1]|max_length[3]|xss_clean');
		$this->form_validation->set_rules('nm_subprogram', 'Nama Subprogram', 'trim|required|min_length[2]|max_length[35]|xss_clean');
		
		if ($this->form_validation->run() == FALSE)
		{
			$this->load->view('master/subprogram',$data);
		}else{
			$this->master_model->add_subprogram();
			$this->session->set_userdata('m_success',"Subprogram berhasil disimpan");
			redirect(base_url().'index.php/master/subprogram');
		}
	}
	
	public function edit()
	{
		$data = $this->master_model->general();
		$data['subprogram_list'] = $this->master_model->get_subprogram();
		$this->load->library('form_validation');
				
		$this->form_validation->set_rules('nm_subprogram', 'Nama Subprogram', 'trim|required|min_length[4]|max_length[35]|xss_clean');
		
		if ($this->form_validation->run() == FALSE)
		{
			$this->load->view('master/subprogram',$data);
		}else{
			$this->master_model->update_subprogram();
			$this->session->set_userdata('m_success',"Subprogram berhasil disimpan");
			redirect(base_url().'index.php/master/subprogram');
		}
	}
	
	public function delete($kode){
		$this->master_model->delete_subprogram($kode);
		$this->session->set_userdata('m_success',"Subprogram berhasil dihapus");
		redirect(base_url().'index.php/master/subprogram');
	}
	
	function disp_subprogram($kode){
		$d_subprogram = $this->master_model->get_subprogram($kode);
		
		$data = '<form method="post" action="'.base_url().'index.php/master/subprogram/edit">
				<div class="modal-header label-default">
	                <a type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</a>
	                <h4 class="modal-title"><span class="fa fa-user fa-3x"></span> Edit Subprogram</h4>
	            </div>';
		$data.= '<div class="modal-body">
					<div class="row">					  
                      <div class="col-md-5"><label for="kd_subprogram">Kode subprogram</label></div>
                      <div class="col-md-7">
                          <input autofocus type="text" readonly class="form-control disabled" value="'.$d_subprogram['kd_subprogram'].'" name="kd_subprogram">                          
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-5"><label for="nm_subprogram">Nama subprogram</label></div>
                      <div class="col-md-7">
                          <input type="text" required class="form-control" name="nm_subprogram" value="'.$d_subprogram['nm_subprogram'].'">                          
                      </div>
                    </div>
                </div>';
        $data.= '<div class="modal-footer">
	            	<input type="submit" class="btn btn-success" value="Simpan">
	                <input type="reset" class="btn btn-danger" data-dismiss="modal" value="Batal">
	            </div>
	            </form>';
		echo $data;
	}

	public function subprogram_datatable(){
		$this->load->library('datatables');
		//$this->load->library('jquery');
		//$this->jquery->

		$this->datatables->select('kd_subprogram, nm_subprogram')->from('subprogram');				
		$this->datatables->add_column('edit', '<a id="$1" data-toggle="modal" data-target="#edit_modal" data-remote="'.base_url().'index.php/master/subprogram/disp_subprogram/$1" href="#edit-modal" class="tb-edit btn btn-info btn-embossed"><i class="fa fa-edit"></i></a>
											   <a href="'.base_url().'index.php/master/subprogram/delete/$1" class="btn btn-danger btn-embossed"><i class="fa fa-trash-o"></i></a></td>','kd_subprogram');
		
    	echo $this->datatables->generate();		
	}
}