<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class diskon extends CI_Controller {
	function __construct(){
		parent::__construct();		
		$this->load->helper(array('form','url'));
		$this->load->library('session');
		$this->load->model('master_model');
		$this->check_isvalidated();
	}
	
	private function check_isvalidated(){
		if(! $this->session->userdata('validated'))
			redirect('login');
		else
			if($this->session->userdata('level') === 'user')
					redirect('user/main');						
	}
	
	public function logout(){
		$this->session->sess_destroy();
		redirect('login');
	}
		
	public function index()
	{
		$data = $this->master_model->general();
		if($this->session->userdata('m_success')){
			$data['m_success'] = $this->session->userdata('m_success');
			$this->session->unset_userdata('m_success');
		}
		
		$this->load->view('master/diskon',$data);
	}
	
	public function view()
	{
		$data = $this->master_model->general();
		if($this->session->userdata('m_success')){
			$data['m_success'] = $this->session->userdata('m_success');
			$this->session->unset_userdata('m_success');
		}
		$data['diskon_list'] = $this->master_model->get_diskon();
		
		$this->load->view('master/diskon_view',$data);
	}
	
	public function add()
	{
		$data = $this->master_model->general();
		$this->load->library('form_validation');
		
		$this->form_validation->set_rules('kd_diskon', 'Kode Diskon', 'trim|required|min_length[2]|max_length[10]|xss_clean');
		$this->form_validation->set_rules('nm_diskon', 'Nama Diskon', 'trim|required|min_length[4]|max_length[50]|xss_clean');
		$this->form_validation->set_rules('persen', 'Persen', 'trim|required|numeric');
		$this->form_validation->set_rules('ket_diskon', 'Keterangan Diskon', 'trim|max_length[100]');
		
		if ($this->form_validation->run() == FALSE)
		{
			$this->load->view('master/diskon',$data);
		}else{
			$this->master_model->add_diskon();
			$this->session->set_userdata('m_success',"Diskon berhasil disimpan");
			redirect(base_url().'index.php/master/diskon');
		}
	}
	
	public function edit()
	{
		$data = $this->master_model->general();
		$data['diskon_list'] = $this->master_model->get_diskon();
		$this->load->library('form_validation');
				
		$this->form_validation->set_rules('kd_diskon', 'Kode Diskon', 'trim|required|min_length[2]|max_length[10]|xss_clean');
		$this->form_validation->set_rules('nm_diskon', 'Nama Diskon', 'trim|required|min_length[4]|max_length[50]|xss_clean');
		$this->form_validation->set_rules('persen', 'Persen', 'trim|required|numeric');
		$this->form_validation->set_rules('ket_diskon', 'Keterangan Diskon', 'trim');
		
		if ($this->form_validation->run() == FALSE)
		{
			$this->load->view('master/diskon',$data);
		}else{
			$this->master_model->update_diskon();
			$this->session->set_userdata('m_success',"Diskon berhasil disimpan");
			redirect(base_url().'index.php/master/diskon');
		}
	}
	
	public function delete($kode){
		$this->master_model->delete_diskon($kode);
		$this->session->set_userdata('m_success',"Diskon berhasil dihapus");
		redirect(base_url().'index.php/master/diskon');
	}
	
	function disp_diskon($kode){
		$d_diskon = $this->master_model->get_diskon($kode);
		
		$data = '<form method="post" action="'.base_url().'index.php/master/diskon/edit">
				<div class="modal-header label-default">
	                <a type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</a>
	                <h4 class="modal-title"><span class="fa fa-user fa-3x"></span> Edit Diskon</h4>
	            </div>';
		$data.= '<div class="modal-body">
					<div class="row">					  
                      <div class="col-md-5"><label for="kd_diskon">Kode diskon</label></div>
                      <div class="col-md-7">
                          <input autofocus type="text" readonly class="form-control disabled" value="'.$d_diskon['kd_diskon'].'" name="kd_diskon">                          
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-5"><label for="nm_diskon">Nama diskon</label></div>
                      <div class="col-md-7">
                          <input type="text" required class="form-control" name="nm_diskon" value="'.$d_diskon['nm_diskon'].'">                          
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-5"><label for="persen">Persen</label></div>
                      <div class="col-md-7">
                          <input autofocus type="number" required class="form-control" name="persen" value="'.$d_diskon['persen'].'">
                      </div>
                    </div>
					<div class="row">
                      <div class="col-md-5"><label for="persen">Keterangan</label></div>
                      <div class="col-md-7">
                          <input class="form-control" name="ket_diskon" value="'.$d_diskon['ket_diskon'].'">
                      </div>
                    </div>
                </div>';
        $data.= '<div class="modal-footer">
	            	<input type="submit" class="btn btn-success" value="Simpan">
	                <input type="reset" class="btn btn-danger" data-dismiss="modal" value="Batal">
	            </div>
	            </form>';
		echo $data;
	}

	public function diskon_datatable(){
		$this->load->library('datatables');
		//$this->load->library('jquery');
		//$this->jquery->

		$this->datatables->select('kd_diskon, nm_diskon, persen, ket_diskon')->from('diskon');		
		$this->datatables->edit_column('persen','$1 %','persen');
		$this->datatables->add_column('edit', '<a id="$1" data-toggle="modal" data-target="#edit_modal" data-remote="'.base_url().'index.php/master/diskon/disp_diskon/$1" href="#edit-modal" class="tb-edit btn btn-info btn-embossed"><i class="fa fa-edit"></i></a>
											   <a href="'.base_url().'index.php/master/diskon/delete/$1" class="btn btn-danger btn-embossed"><i class="fa fa-trash-o"></i></a></td>','kd_diskon');
		
    	echo $this->datatables->generate();		
	}
}