<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Barang extends CI_Controller {
	function __construct(){
		parent::__construct();		
		$this->load->helper(array('form','url'));
		$this->load->library('session');
		$this->load->model('user_model');
		$this->check_isvalidated();
	}

	private function check_isvalidated(){
		if(! $this->session->userdata('validated'))
			redirect('login');
		else
			if($this->session->userdata('level') === 'admin')
					redirect('master/main');						
	}
	
	public function logout(){
		$this->session->sess_destroy();
		redirect('login');
	}	
	
	public function index()
	{
		$data = $this->user_model->general();
		
		$data['murid'] = $this->user_model->get_murid();
		$data['barang'] = $this->user_model->get_barang();
		
		$this->load->view('user/barang_form',$data);
	}

	public function view($page=0){		
		$data = $this->user_model->general();					

		$data['kwitansi'] = $this->user_model->get_mkwitansi_barang($page,20);
		$count = $this->user_model->get_count_mkwitansi_barang();
		$data['pagination'] = $this->pagination($count,20,5);
		
		$this->load->view('user/barang_view',$data);
	}
	
	//pagination settings
	function pagination($row_total,$row_per_page,$link_count){
		$this->load->library(array('form_validation','pagination'));
		$config['base_url'] = base_url().'index.php/user/barang/view';
		$config['uri_segment'] = 4;
		$config['total_rows'] = $row_total;
		$config['per_page'] = $row_per_page;
		$config['num_links'] = $link_count;
		$config['use_page_numbers'] = FALSE;
		$config['full_tag_open'] = '<ul class="pagination">';
		$config['full_tag_close'] = '</ul>';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		$config['next_link'] = 'Older &rarr;';
		$config['next_tag_open'] = '<li class="next">';
		$config['next_tag_close'] = '</li>';
		$config['prev_link'] = '&larr; Newer';
		$config['prev_tag_open'] = '<li class="previous">';
		$config['prev_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="active"><a>';
		$config['cur_tag_close'] = '<span class="sr-only">(current)</span></a></li>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		$data = $this->pagination->create_links();
		return $data;
	}
	
	public function batal(){
		$no_kwitansi = $this->input->get('no');
		$this->user_model->change_kwitansi_status('REQBATAL', 'barang', $no_kwitansi);
		redirect(base_url().'index.php/user/barang/view');
	}

	public function add(){
		$data = $this->user_model->general();
		$this->load->library('form_validation');
		$data['murid'] = $this->user_model->get_murid();		
		$data['barang'] = $this->user_model->get_barang();
		
		if($this->input->post('murid')){
			$this->form_validation->set_rules('nim','NIM','required');
			$this->form_validation->set_rules('nama','Nama','required');						
		}else{
			$this->form_validation->set_rules('nama','Nama','trim|min_length[2]|max_length[50]|required');
		}
		
		if($this->form_validation->run() == FALSE){						
			$this->load->view('user/barang_form',$data);		
		}else{
			if( count($this->input->post('kd_barang')) <= 0){
				$this->load->view('user/barang_form',$data);		
			}else{
				$data['tanggal'] = date('d-m-Y');
				if($this->input->post('nim')){
					$murid = explode('|',$this->input->post('nim'));
					$data['nim'] = $this->input->post('nim');
					$data['nama'] = $this->input->post('nama');
				}else{
					$data['nim'] = NULL;
					$data['nama'] = $this->input->post('nama');
				}
				$kd_barang = $this->input->post('kd_barang');
				$quantity = $this->input->post('quantity');
				$harga = $this->input->post('harga');
				$stotal = $this->input->post('stotal');
				
				$data['uraian'] = array();
				$data['total']=0;
				for($i=0;$i< count($kd_barang); $i++){					
					$barang = $this->user_model->get_barang($kd_barang[$i]);
					$u_barang = array(
						'kd_barang'=>$kd_barang[$i],
						'nm_barang'=>$barang['nm_barang'],
						'harga'=>$harga[$i],
						'quantity'=>$quantity[$i],
						'stotal'=>$stotal[$i]
					);
					$data['total'] += round($stotal[$i]);
					array_push($data['uraian'],$u_barang);
				}
				$data['terbilang'] = $this->terbilang($data['total']);
				
				$buydata = array(
					'tanggal'=>$data['tanggal'],
					'nim'=>$data['nim'],
					'nama'=>$data['nama'],
					'total'=>$data['total'],
					'uraian'=>$data['uraian']
				);
				$this->session->set_flashdata('buy',$buydata);
				
				$this->load->view('user/barang_check',$data);
			}
		}
	}	
	
	public function save(){
		$pembelian = $this->session->flashdata('buy');
		
		$kwitansi = $this->user_model->set_kwitansi_barang($pembelian);
		
		$this->session->set_flashdata('message','Kwitansi telah berhasil disimpan. 
		<a target="_blank" href="'.base_url().'index.php/user/pdfprint/barang/?nomor='.$kwitansi.'">Klik Di sini untuk menampilkan</a>');
		redirect(base_url().'index.php/user/barang');
	}
	
	function terbilang($satuan){
		$huruf = array("","satu","dua","tiga","empat","lima","enam","tujuh","delapan","sembilan","sepuluh","sebelas");
		if($satuan < 12)
			return " ".$huruf[$satuan];
		elseif($satuan<20)
			return $this->terbilang($satuan - 10)." belas";
		elseif($satuan<100)
			return $this->terbilang($satuan/10)." puluh".$this->terbilang($satuan% 10);
		elseif($satuan<200)
			return "seratus".$this->terbilang($satuan-100);
		elseif($satuan<1000)
			return $this->terbilang($satuan /100)." ratus".$this->terbilang($satuan %100);
		elseif($satuan<2000)
			return "seribu".$this->terbilang($satuan-1000);
		elseif($satuan<1000000)
			return $this->terbilang($satuan/1000)." ribu".$this->terbilang($satuan%1000);
		elseif($satuan < 1000000000)
			return $this->terbilang($satuan/1000000)." juta".$this->terbilang($satuan % 1000000);
		elseif($satuan >=1000000000)
			return "Angka terlalu Besar";
	}
}