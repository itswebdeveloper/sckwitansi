<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class PDFPrint extends CI_Controller {
	function __construct(){
		parent::__construct();		
		$this->load->helper(array('form','url'));
		$this->load->library('session');
		$this->load->model('user_model');
		$this->load->library('fpdf');
		//$this->check_isvalidated();
	}

	private function check_isvalidated(){
		if(! $this->session->userdata('validated'))
			redirect('login');
		else
			if($this->session->userdata('level') === 'admin')
					redirect('master/main');						
	}

	public function logout(){
		$this->session->sess_destroy();
		redirect('login');
	}
	
	public function index($jenis_kwitansi)
	{
		redirect('user/main');
	}

	public function pelajaran()
	{
		$nomor = $this->input->get('nomor',TRUE);
		$kwitansi = $this->user_model->get_selected_mkwitansi($nomor,'pelajaran');
		$uraian = $this->user_model->get_selected_pkwitansi($nomor,'pelajaran');
		$diskon = $this->user_model->get_ket_diskon($nomor);		
		$ket_diskon='';
		$temp = '';
        foreach ($diskon->result_array() as $row) {

            if($temp != '' && $row['kd_biaya'] != $temp)
                $ket_diskon.= " , ";

            if($row['kd_biaya'] != $temp){
            	if($row['kd_pelajaran'] == NULL)
					$kd_subprogram = '';
				else{
					$split = explode('-', $row['kd_pelajaran']);
					$kd_subprogram = $split[1];
				}            	
            	$kd_pembayaran = "[".$row['kd_program']."] $row[nm_biaya] $kd_subprogram $row[kd_tingkat]";
                $ket_diskon.= "($kd_pembayaran) $row[ket] $row[persen_diskon]%";
            }


            if($row['kd_biaya'] != $temp){
                //$ket_diskon.= $row['persen_diskon']."%";
            }else{
                $ket_diskon.= "+ $row[ket] $row[persen_diskon]%";
            }                                        

            $temp = $row['kd_biaya'];                                        
         }

        $denda = $this->user_model->get_ket_denda($nomor);
        $ket_denda='';
        //$temp = '';
        foreach ($denda->result_array() as $row) {
            /*if($temp != '' && $row['kd_denda'] != $temp)
                $ket_denda.= " , ";*/
            $total =0;
            if($row['status'] == "semester"){
				$total = $row['harga'] * $row['periode'];
			}else{
				for($j=1; $j<= $row['periode']; $j++){
					$total+=(($row['harga']*$row['periode'])-(($j-1)*$row['harga']));
				}
			}

            $ket_denda.="($row[status])$row[ket] ($row[periode] periode= $total), "; 

            /*$temp = $row['kd_denda'];*/
         }
        $this->fpdf = new FPDF('L','mm',array(210,148.5));
		$this->fpdf->AddPage();
		$this->KwitansiPelajaran($kwitansi,$uraian, $ket_diskon, $ket_denda);
		$this->fpdf->Output();		
	}
	
	function KwitansiPelajaran($kwitansi,$uraian, $ket_diskon, $ket_denda)
	{
		$this->fpdf->SetFillColor(255,255,255);
		$this->fpdf->SetTextColor(0,0,0);
		$this->fpdf->SetDrawColor(0,0,0);
		$this->fpdf->SetFont('Arial','B',18);		
		$this->fpdf->Cell(60,4,"",'',0,'L',TRUE);
		$this->fpdf->Image("".base_url()."assets/images/sctext.JPG",7,7,50);
		$this->fpdf->Cell(70,4,"Kwitansi",'',0,'C',TRUE);
		$this->fpdf->SetFont('Arial','',9);
		$this->fpdf->Cell(60,4,"e-mail   : sumatraconservatoire@yahoo.com",'',0,'L',TRUE);
		$this->fpdf->Ln();
		$this->fpdf->Cell(60,4,"Jl. Mahoni No.12 Medan",'',0,'L',TRUE);		
		$this->fpdf->SetFont('Arial','B',11);
		$split_kwitansi = explode('/', $kwitansi['no_kwitansi']);
		$this->fpdf->Cell(70,4,"No. ".$split_kwitansi[0],'',0,'C',TRUE);
		$this->fpdf->SetFont('Arial','',9);		
		$this->fpdf->Cell(60,4,"Website: www.sumatraconservatoire.com",'',0,'L',TRUE);
		$this->fpdf->Ln();
		$this->fpdf->Cell(190,4,"Telp. (061)4525712",'',0,'L',TRUE);
		$this->fpdf->Ln();
		$this->fpdf->Ln();
		$this->fpdf->SetFont('Arial','B',10);
		$this->fpdf->Cell(35,5,"Telah diterima dari :",'',0,'L',TRUE);		
		$dr = $kwitansi['nim'].' | '.strtoupper($kwitansi['nama']);
		$this->fpdf->Cell(125,5,$dr,'',0,'L',TRUE);
		$this->fpdf->Cell(10,5,"Tgl. ",'',0,'L',TRUE);
		$tgl = date('d M Y',strtotime($kwitansi['tgl_kwitansi']));
		$this->fpdf->Cell(45,5,$tgl,'',0,'L',TRUE);		
		$this->fpdf->Ln();
		$w = array(7,61,12,12,12,30,8,25,25);
		$this->fpdf->Cell($w[0],5,"No",'BTLR',0,'C',TRUE);
		$this->fpdf->Cell($w[1],5,"Pembayaran",'BTLR',0,'C',TRUE);
		$this->fpdf->Cell($w[2],5,"Prog.",'BTLR',0,'C',TRUE);
		$this->fpdf->Cell($w[3],5,"S.Prog.",'BTLR',0,'C',TRUE);
		$this->fpdf->Cell($w[4],5,"Tgkt",'BTLR',0,'C',TRUE);
		$this->fpdf->Cell($w[5],5,"Bulan",'BTLR',0,'C',TRUE);
		$this->fpdf->Cell($w[6],5,"P.",'BTLR',0,'C',TRUE);
		$this->fpdf->Cell($w[7],5,"@",'BTLR',0,'C',TRUE);
		$this->fpdf->Cell($w[8],5,"Jumlah",'BTLR',0,'C',TRUE);
		$this->fpdf->Ln();		
		$i=1;
		foreach($uraian->result_array() as $row){
			$this->fpdf->SetFont('Arial','I',10);
			$this->fpdf->Cell($w[0],6,$i,'BTLR',0,'C',TRUE);			
			$this->fpdf->Cell($w[1],6,$row['nm_biaya'],'BTLR',0,'L',TRUE);
			$this->fpdf->Cell($w[2],6,$row['kd_program'],'BTLR',0,'C',TRUE);
			if($row['kd_pelajaran'] == NULL)
				$kd_subprogram = '';
			else{
				$split = explode('-', $row['kd_pelajaran']);
				$kd_subprogram = $split[1];
			}
			$this->fpdf->Cell($w[3],6,$kd_subprogram,'BTLR',0,'C',TRUE);
			$this->fpdf->Cell($w[4],6,$row['kd_tingkat'],'BTLR',0,'C',TRUE);
			$mth = array('','Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec');                                                
            if($row['jlh_bln'] > 1){
                $split_bln = explode(',', $row['bulan']);
                $this->fpdf->Cell($w[5],6,$mth[$split_bln[0]].' - '.$mth[$split_bln[(($row['jlh_bln'])-1)]].' '.$row['tahun'],'BTLR',0,'C',TRUE);
            }else{
                if($row['bulan']!='')
                	$this->fpdf->Cell($w[5],6,$mth[$row['bulan']].' '.$row['tahun'],'BTLR',0,'C',TRUE);
                else
                	$this->fpdf->Cell($w[5],6,'','BTLR',0,'C',TRUE);
            }
            $this->fpdf->Cell($w[6],6,$row['jlh_bln'],'BTLR',0,'C',TRUE);
			$this->fpdf->Cell($w[7],6,number_format($row['pkpbiaya']),'BTLR',0,'R',TRUE);
			$this->fpdf->Cell($w[8],6,number_format($row['jumlah']),'BTLR',0,'R',TRUE);
			$i++;
			$this->fpdf->Ln();
		}

		for($i=1; $i<=(4-(count($uraian->result_array()))); $i++){
			$this->fpdf->Cell($w[0],6,'','BTLR',0,'C',TRUE);			
			$this->fpdf->Cell($w[1],6,'','BTLR',0,'L',TRUE);
			$this->fpdf->Cell($w[2],6,'','BTLR',0,'L',TRUE);
			$this->fpdf->Cell($w[3],6,'','BTLR',0,'L',TRUE);
			$this->fpdf->Cell($w[4],6,'','BTLR',0,'L',TRUE);			
            $this->fpdf->Cell($w[5],6,'','BTLR',0,'L',TRUE);            
            $this->fpdf->Cell($w[6],6,'','BTLR',0,'R',TRUE);
			$this->fpdf->Cell($w[7],6,'','BTLR',0,'R',TRUE);
			$this->fpdf->Cell($w[8],6,'','BTLR',0,'R',TRUE);			
			$this->fpdf->Ln();	
		}
		$this->fpdf->SetLineWidth(.4);		
		$this->fpdf->cell($w[0]+$w[1]+$w[2]+$w[3]+$w[4]+$w[5]+$w[6]+$w[7],6,'Subtotal','BTLR',0,'R',TRUE);	
		$this->fpdf->cell($w[8],6,number_format($kwitansi['subtotal']),'BTLR',0,'R',TRUE);
		$this->fpdf->Ln();
		$this->fpdf->cell($w[0]+$w[1]+$w[2]+$w[3]+$w[4]+$w[5]+$w[6]+$w[7],6,'Nilai Diskon','BLR',0,'R',TRUE);	
		$this->fpdf->cell($w[8],6,number_format($kwitansi['nilai_diskon']),'BLR',0,'R',TRUE);
		$this->fpdf->Ln();
		$this->fpdf->cell($w[0]+$w[1]+$w[2]+$w[3]+$w[4]+$w[5]+$w[6]+$w[7],6,'Nilai Denda','BLR',0,'R',TRUE);	
		$this->fpdf->cell($w[8],6,number_format($kwitansi['nilai_denda']),'BLR',0,'R',TRUE);
		$this->fpdf->Ln();
		$this->fpdf->cell($w[0]+$w[1]+$w[2]+$w[3]+$w[4]+$w[5]+$w[6]+$w[7],6,'Lebih / Kurang Bayar','BLR',0,'R',TRUE);
		if($kwitansi['lebih_bayar'] != 0){
			$this->fpdf->cell($w[8],6,"-".number_format($kwitansi['lebih_bayar']),'BLR',0,'R',TRUE);
		}else{
			$this->fpdf->cell($w[8],6,number_format($kwitansi['kurang_bayar']),'BLR',0,'R',TRUE);
		}
		$this->fpdf->Ln();
		$this->fpdf->cell($w[0]+$w[1]+$w[2]+$w[3]+$w[4]+$w[5]+$w[6]+$w[7],6,'TOTAL','BLR',0,'R',TRUE);	
		$this->fpdf->cell($w[8],6,number_format($kwitansi['total']),'BLR',0,'R',TRUE);
		$this->fpdf->Ln();
		$this->fpdf->SetLineWidth(.3);
		$this->fpdf->SetFont('Arial','B',11);
		$this->fpdf->Cell(22,5,"Terbilang :",'T',0,'L',TRUE);
		$this->fpdf->SetFont('Arial','I',10);
		$this->fpdf->Cell(143,5,$this->terbilang($kwitansi['total'])." rupiah",'T',0,'L',TRUE);
		$this->fpdf->SetFont('Arial','B',10);		
		$this->fpdf->cell(25,6,"Kasir",'T',0,'C',TRUE);
		$this->fpdf->Ln();
		$this->fpdf->SetFont('Arial','',11);
		$this->fpdf->Cell(25,3,"Keterangan :",'',0,'L',TRUE);
		$this->fpdf->DrawTextBox("$kwitansi[keterangan]",135,0,'J','T',0);
		$this->fpdf->SetFont('Arial','',11);
		$this->fpdf->Cell(25,5,"Ket Diskon :",'',0,'L',TRUE);
		$this->fpdf->DrawTextBox("$ket_diskon",135,0,'J','T',0);
		$this->fpdf->Ln();
		$this->fpdf->SetFont('Arial','',11);
		$this->fpdf->Cell(25,5,"Ket Denda :",'',0,'L',TRUE);
		$this->fpdf->DrawTextBox("$ket_denda",135,0,'J','T',0);
		
		
	}	

	public function barang()
	{
		$nomor = $this->input->get('nomor',TRUE);
		$kwitansi = $this->user_model->get_selected_mkwitansi($nomor,'barang');
		$uraian = $this->user_model->get_selected_pkwitansi($nomor,'barang');
		$this->fpdf = new FPDF('L','mm',array(210,148.5));
		$this->fpdf->AddPage();
		$this->KwitansiBarang($kwitansi,$uraian);
		$this->fpdf->Output();
	}
	
	function KwitansiBarang($kwitansi,$uraian)
	{
		$this->fpdf->SetFillColor(255,255,255);
		$this->fpdf->SetTextColor(0,0,0);
		$this->fpdf->SetDrawColor(0,0,0);
		$this->fpdf->SetFont('Arial','',9);
		$this->fpdf->Cell(190,4,"",'',0,'L',TRUE);
		$this->fpdf->Ln();
		
		$this->fpdf->Cell(130,4,"",'',0,'L',TRUE);
		$this->fpdf->Image("".base_url()."assets/images/sctext.JPG",7,10,50);
		$this->fpdf->Cell(60,4,"e-mail   : sumatraconservatoire@yahoo.com",'',0,'L',TRUE);
		$this->fpdf->Ln();
		$this->fpdf->Cell(60,4,"Jl. Mahoni No.12 Medan",'',0,'L',TRUE);
		$this->fpdf->SetFont('Arial','B',11);
		$split_kwitansi = explode('/', $kwitansi['no_kwitansi']);
		$this->fpdf->Cell(70,4,"No. ".$split_kwitansi[0],'',0,'C',TRUE);
		$this->fpdf->SetFont('Arial','',9);
		$this->fpdf->Cell(60,4,"Website: www.sumatraconservatoire.com",'',0,'L',TRUE);
		$this->fpdf->Ln();
		$this->fpdf->Cell(190,4,"Telp. (061)4525712",'',0,'L',TRUE);
		$this->fpdf->Ln();
		$this->fpdf->SetLineWidth(.3);
		$this->fpdf->SetFont('Arial','B',18);
		$this->fpdf->Cell(190,7,"Kwitansi",'',0,'C',TRUE);
		$this->fpdf->Ln();
		$this->fpdf->Ln();
		$this->fpdf->SetFont('Arial','B',10);
		$this->fpdf->Cell(35,5,"Telah diterima dari :",'',0,'L',TRUE);
		if($kwitansi['nim']==NULL)
			$dr = strtoupper($kwitansi['nama']);
		else
			$dr = $kwitansi['nim'].' | '.strtoupper($kwitansi['nama']);
		$this->fpdf->Cell(100,5,$dr,'B',0,'L',TRUE);
		$this->fpdf->Cell(10,5,"Tgl. ",'',0,'L',TRUE);
		$tgl = date('d M Y',strtotime($kwitansi['tgl_kwitansi']));
		$this->fpdf->Cell(45,5,$tgl,'B',0,'L',TRUE);
		$this->fpdf->Ln();
		$this->fpdf->Ln();
		$this->fpdf->Cell(190,5,"Untuk pembelian : ",'',0,'L',TRUE);
		$this->fpdf->Ln();
		$w = array(8,97,35,10,40);
		$this->fpdf->Cell($w[0],5,"No.",'BTLR',0,'C',TRUE);
		$this->fpdf->Cell($w[1],5,"Uraian",'BTLR',0,'C',TRUE);
		$this->fpdf->Cell($w[2],5,"Harga",'BTLR',0,'C',TRUE);
		$this->fpdf->Cell($w[3],5,"Qty",'BTLR',0,'C',TRUE);
		$this->fpdf->Cell($w[4],5,"Sub Total",'BTLR',0,'C',TRUE);
		$this->fpdf->Ln();
		$i=1;
		foreach($uraian->result_array() as $row){
			$this->fpdf->SetFont('Arial','I',10);
			$this->fpdf->Cell($w[0],6,$i,'TL',0,'C',TRUE);			
			$this->fpdf->Cell($w[1],6,$row['nm_barang'],'TLR',0,'L',TRUE);
			$this->fpdf->SetFont('Arial','',10);
			$this->fpdf->Cell(7,6,"Rp. ",'TL',0,'L',TRUE);
			$this->fpdf->SetFont('Arial','I',10);
			$this->fpdf->Cell($w[2]-7,6,number_format($row['harga']),'TR',0,'R',TRUE);
			$this->fpdf->Cell($w[3],6,$row['qty'],'TLR',0,'C',TRUE);
			$this->fpdf->SetFont('Arial','',10);
			$this->fpdf->Cell(7,6,"Rp. ",'TL',0,'L',TRUE);
			$this->fpdf->SetFont('Arial','I',10);
			$this->fpdf->Cell($w[4]-7,6,number_format($row['jumlah']),'TR',0,'R',TRUE);
			$this->fpdf->SetFont('Arial','',10);
			$i++;
			$this->fpdf->Ln();
		}
		for($i=1; $i<=(7-(count($uraian->result_array()))); $i++){			
			$this->fpdf->Cell($w[0],6,'','TL',0,'C',TRUE);			
			$this->fpdf->Cell($w[1],6,'','TLR',0,'L',TRUE);			
			$this->fpdf->Cell(7,6,"",'TL',0,'L',TRUE);			
			$this->fpdf->Cell($w[2]-7,6,'','TR',0,'R',TRUE);
			$this->fpdf->Cell($w[3],6,'','TLR',0,'C',TRUE);			
			$this->fpdf->Cell(7,6,"",'TL',0,'L',TRUE);			
			$this->fpdf->Cell($w[4]-7,6,'','TR',0,'R',TRUE);					
			$this->fpdf->Ln();
		}
		$this->fpdf->SetLineWidth(.3);
		$this->fpdf->cell($w[0]+$w[1],6,'','T',0,'C',TRUE);
		$this->fpdf->cell($w[2],6,'TOTAL','BTLR',0,'C',TRUE);
		$this->fpdf->cell($w[3],6,'Rp.','BTL',0,'C',TRUE);
		$this->fpdf->cell($w[4],6,number_format($kwitansi['total']),'BTR',0,'R',TRUE);
		$this->fpdf->Ln();
		$this->fpdf->Ln();
		$this->fpdf->SetLineWidth(.3);
		$this->fpdf->SetFont('Helvetica','B',12);
		$this->fpdf->Cell(22,5,"Terbilang :",'',0,'L',TRUE);
		$this->fpdf->SetFont('Arial','',10);
		$this->fpdf->Cell(168,5,$this->terbilang($kwitansi['total'])." rupiah",'B',0,'L',TRUE);
		$this->fpdf->SetFont('Arial','B',10);
		$this->fpdf->Ln();
		$this->fpdf->cell($w[0]+$w[1]+$w[2],6,"",'',0,'C',TRUE);
		$this->fpdf->cell($w[3]+$w[4],6,"Kasir",'',0,'C',TRUE);
	}
	
	function terbilang($satuan){
		$huruf = array("","satu","dua","tiga","empat","lima","enam","tujuh","delapan","sembilan","sepuluh","sebelas");
		if($satuan < 12)
			return " ".$huruf[$satuan];
		elseif($satuan<20)
			return $this->terbilang($satuan - 10)." belas";
		elseif($satuan<100)
			return $this->terbilang($satuan/10)." puluh".$this->terbilang($satuan% 10);
		elseif($satuan<200) 
			return "seratus".$this->terbilang($satuan-100);
		elseif($satuan<1000)
			return $this->terbilang($satuan /100)." ratus".$this->terbilang($satuan %100);
		elseif($satuan<2000)
			return "seribu".$this->terbilang($satuan-1000);
		elseif($satuan<1000000)
			return $this->terbilang($satuan/1000)." ribu".$this->terbilang($satuan%1000);
		elseif($satuan < 1000000000)
			return $this->terbilang($satuan/1000000)." juta".$this->terbilang($satuan % 1000000);
		elseif($satuan >=1000000000)
			return "Angka terlalu Besar";
	}

	public function tes()
	{
		$nomor = $this->input->get('nomor',TRUE);
		$kwitansi = $this->user_model->get_selected_mkwitansi($nomor,'barang');
		$uraian = $this->user_model->get_selected_pkwitansi($nomor,'barang');
		$this->fpdf->AddPage();
		$this->fpdf->SetMargins(10,10,1);
		$this->KwitansiTes($kwitansi,$uraian);
		$this->fpdf->Output();
	}
	function KwitansiTes($kwitansi,$uraian)
	{

		$this->fpdf->SetFillColor(255,255,255);
		$this->fpdf->SetTextColor(0,0,0);
		$this->fpdf->SetDrawColor(0,0,0);
		$this->fpdf->SetFont('Arial','',9);		
		$this->fpdf->Ln();
		
		$this->fpdf->Cell(130,4,"",'',0,'L',TRUE);
		$this->fpdf->Image("".base_url()."assets/images/sctext.JPG",7,6,50);
		$this->fpdf->Cell(60,4,"e-mail   : sumatraconservatoire@yahoo.com",'',0,'L',TRUE);
		$this->fpdf->Ln();
		$this->fpdf->Cell(60,4,"Jl. Mahoni No.12 Medan",'',0,'L',TRUE);
		$this->fpdf->SetFont('Arial','B',11);
		$this->fpdf->Cell(70,4,"No. ".$kwitansi['no_kwitansi'],'',0,'C',TRUE);
		$this->fpdf->SetFont('Arial','',9);
		$this->fpdf->Cell(60,4,"Website: www.sumatraconservatoire.com",'',0,'L',TRUE);
		$this->fpdf->Ln();
		$this->fpdf->Cell(190,4,"Telp. (061)4525712",'',0,'L',TRUE);
		$this->fpdf->Ln();
		$this->fpdf->SetLineWidth(.3);
		$this->fpdf->SetFont('Arial','B',18);
		$this->fpdf->Cell(190,7,"Kwitansi",'',0,'C',TRUE);
		$this->fpdf->Ln();		
		$this->fpdf->SetFont('Arial','B',10);
		$this->fpdf->Cell(35,5,"Telah diterima dari :",'',0,'L',TRUE);
		if($kwitansi['nim']==NULL)
			$dr = strtoupper($kwitansi['nama']);
		else
			$dr = $kwitansi['nim'].' | '.strtoupper($kwitansi['nama']);
		$this->fpdf->Cell(100,5,$dr,'B',0,'L',TRUE);
		$this->fpdf->Cell(10,5,"Tgl. ",'',0,'L',TRUE);
		$tgl = date('d M Y',strtotime($kwitansi['tgl_kwitansi']));
		$this->fpdf->Cell(45,5,$tgl,'B',0,'L',TRUE);
		$this->fpdf->Ln();
		$this->fpdf->Cell(190,5,"Untuk pembelian : ",'',0,'L',TRUE);
		$this->fpdf->Ln();
		$w = array(8,97,35,10,40);
		$this->fpdf->Cell($w[0],5,"No.",'BTLR',0,'C',TRUE);
		$this->fpdf->Cell($w[1],5,"Uraian",'BTLR',0,'C',TRUE);
		$this->fpdf->Cell($w[2],5,"Harga",'BTLR',0,'C',TRUE);
		$this->fpdf->Cell($w[3],5,"Qty",'BTLR',0,'C',TRUE);
		$this->fpdf->Cell($w[4],5,"Jumlah",'BTLR',0,'C',TRUE);
		$this->fpdf->Ln();
		$i=1;
		foreach($uraian->result_array() as $row){
			$this->fpdf->SetFont('Arial','I',10);
			$this->fpdf->Cell($w[0],6,$i,'TL',0,'C',TRUE);			
			$this->fpdf->Cell($w[1],6,$row['nm_barang'],'TLR',0,'L',TRUE);
			$this->fpdf->SetFont('Arial','',10);
			$this->fpdf->Cell(7,6,"Rp. ",'TL',0,'L',TRUE);
			$this->fpdf->SetFont('Arial','I',10);
			$this->fpdf->Cell($w[2]-7,6,number_format($row['harga']),'TR',0,'R',TRUE);
			$this->fpdf->Cell($w[3],6,$row['qty'],'TLR',0,'C',TRUE);
			$this->fpdf->SetFont('Arial','',10);
			$this->fpdf->Cell(7,6,"Rp. ",'TL',0,'L',TRUE);
			$this->fpdf->SetFont('Arial','I',10);
			$this->fpdf->Cell($w[4]-7,6,number_format($row['jumlah']),'TR',0,'R',TRUE);
			$this->fpdf->SetFont('Arial','',10);
			$i++;
			$this->fpdf->Ln();
		}

		$this->fpdf->SetFont('Arial','I',10);
			$this->fpdf->Cell($w[0],6,$i,'TL',0,'C',TRUE);			
			$this->fpdf->Cell($w[1],6,$row['nm_barang'],'TLR',0,'L',TRUE);
			$this->fpdf->SetFont('Arial','',10);
			$this->fpdf->Cell(7,6,"Rp. ",'TL',0,'L',TRUE);
			$this->fpdf->SetFont('Arial','I',10);
			$this->fpdf->Cell($w[2]-7,6,number_format($row['harga']),'TR',0,'R',TRUE);
			$this->fpdf->Cell($w[3],6,$row['qty'],'TLR',0,'C',TRUE);
			$this->fpdf->SetFont('Arial','',10);
			$this->fpdf->Cell(7,6,"Rp. ",'TL',0,'L',TRUE);
			$this->fpdf->SetFont('Arial','I',10);
			$this->fpdf->Cell($w[4]-7,6,number_format($row['jumlah']),'TR',0,'R',TRUE);
			$this->fpdf->SetFont('Arial','',10);
			$i++;
			$this->fpdf->Ln();
		$this->fpdf->SetFont('Arial','I',10);
			$this->fpdf->Cell($w[0],6,$i,'TL',0,'C',TRUE);			
			$this->fpdf->Cell($w[1],6,$row['nm_barang'],'TLR',0,'L',TRUE);
			$this->fpdf->SetFont('Arial','',10);
			$this->fpdf->Cell(7,6,"Rp. ",'TL',0,'L',TRUE);
			$this->fpdf->SetFont('Arial','I',10);
			$this->fpdf->Cell($w[2]-7,6,number_format($row['harga']),'TR',0,'R',TRUE);
			$this->fpdf->Cell($w[3],6,$row['qty'],'TLR',0,'C',TRUE);
			$this->fpdf->SetFont('Arial','',10);
			$this->fpdf->Cell(7,6,"Rp. ",'TL',0,'L',TRUE);
			$this->fpdf->SetFont('Arial','I',10);
			$this->fpdf->Cell($w[4]-7,6,number_format($row['jumlah']),'TR',0,'R',TRUE);
			$this->fpdf->SetFont('Arial','',10);
			$i++;
			$this->fpdf->Ln();
		$this->fpdf->SetFont('Arial','I',10);
			$this->fpdf->Cell($w[0],6,$i,'TL',0,'C',TRUE);			
			$this->fpdf->Cell($w[1],6,$row['nm_barang'],'TLR',0,'L',TRUE);
			$this->fpdf->SetFont('Arial','',10);
			$this->fpdf->Cell(7,6,"Rp. ",'TL',0,'L',TRUE);
			$this->fpdf->SetFont('Arial','I',10);
			$this->fpdf->Cell($w[2]-7,6,number_format($row['harga']),'TR',0,'R',TRUE);
			$this->fpdf->Cell($w[3],6,$row['qty'],'TLR',0,'C',TRUE);
			$this->fpdf->SetFont('Arial','',10);
			$this->fpdf->Cell(7,6,"Rp. ",'TL',0,'L',TRUE);
			$this->fpdf->SetFont('Arial','I',10);
			$this->fpdf->Cell($w[4]-7,6,number_format($row['jumlah']),'TR',0,'R',TRUE);
			$this->fpdf->SetFont('Arial','',10);
			$i++;
			$this->fpdf->Ln();						

		$this->fpdf->SetLineWidth(.3);
		$this->fpdf->cell($w[0]+$w[1],6,'','T',0,'C',TRUE);
		$this->fpdf->cell($w[2],6,'TOTAL','BTLR',0,'C',TRUE);
		$this->fpdf->cell($w[3],6,'Rp.','BTL',0,'C',TRUE);
		$this->fpdf->cell($w[4],6,number_format($kwitansi['total']),'BTR',0,'R',TRUE);
		$this->fpdf->Ln();
		$this->fpdf->Ln();
		$this->fpdf->SetLineWidth(.3);
		$this->fpdf->SetFont('Helvetica','B',12);
		$this->fpdf->Cell(22,5,"Terbilang :",'',0,'L',TRUE);
		$this->fpdf->SetFont('Arial','',10);
		$this->fpdf->Cell(168,5,$this->terbilang($kwitansi['total']),'B',0,'L',TRUE);
		$this->fpdf->SetFont('Arial','B',10);
		$this->fpdf->Ln();
		$this->fpdf->Ln();
		$this->fpdf->cell($w[0]+$w[1]+$w[2],6,"",'',0,'C',TRUE);
		$this->fpdf->cell($w[3]+$w[4],6,"Penerima",'',0,'C',TRUE);
	}

	public function tespelajaran()
	{
		$nomor = $this->input->get('nomor',TRUE);
		$kwitansi = $this->user_model->get_selected_mkwitansi($nomor,'pelajaran');
		$uraian = $this->user_model->get_selected_pkwitansi($nomor,'pelajaran');
		$this->fpdf->AddPage();
		$this->fpdf->SetMargins(10,10,1);
		$this->KwitansitesPelajaran($kwitansi,$uraian);
		$this->fpdf->Output();
	}
	
	function KwitansitesPelajaran($kwitansi,$uraian)
	{
		$this->fpdf->SetFillColor(255,255,255);
		$this->fpdf->SetTextColor(0,0,0);
		$this->fpdf->SetDrawColor(0,0,0);
		$this->fpdf->SetFont('Arial','',9);
		$this->fpdf->Cell(130,5,"",'',0,'L',TRUE);
		$this->fpdf->Image("".base_url()."assets/images/sctext.JPG",7,5,50);
		$this->fpdf->Cell(60,4,"e-mail   : sumatraconservatoire@yahoo.com",'',0,'L',TRUE);
		$this->fpdf->Ln();
		$this->fpdf->Cell(130,4,"Jl. Mahoni No.12 Medan",'',0,'L',TRUE);		
		$this->fpdf->Cell(60,4,"Website: www.sumatraconservatoire.com",'',0,'L',TRUE);
		$this->fpdf->Ln();
		$this->fpdf->Cell(81,4,"Telp. (061)4525712",'',0,'L',TRUE);
		$this->fpdf->SetLineWidth(.3);
		$this->fpdf->SetFont('Arial','B',18);
		$this->fpdf->Cell(109,7,"Kwitansi",'',0,'L',TRUE);
		/*$this->fpdf->Ln();
		$this->fpdf->SetLineWidth(.3);
		$this->fpdf->SetFont('Arial','B',18);
		$this->fpdf->Cell(190,7,"Kwitansi",'',0,'C',TRUE);*/
		$this->fpdf->ln();
		$this->fpdf->SetFont('Arial','B',11);
		$this->fpdf->Cell(190,4,"No. ".$kwitansi['no_kwitansi'],'',0,'C',TRUE);
		$this->fpdf->SetFont('Arial','',9);
		$this->fpdf->Ln();
		$this->fpdf->Ln();
		$this->fpdf->SetFont('Arial','B',10);
		$this->fpdf->Cell(35,5,"Telah diterima dari :",'',0,'L',TRUE);
		if($kwitansi['nim']==NULL)
			$dr = strtoupper($kwitansi['nm_murid']);
		else
			$dr = $kwitansi['nim'].' | '.strtoupper($kwitansi['nm_murid']);
		$this->fpdf->Cell(125,5,$dr,'',0,'L',TRUE);
		$this->fpdf->Cell(10,5,"Tgl. ",'',0,'L',TRUE);
		$tgl = date('d M Y',strtotime($kwitansi['tgl_kwitansi']));
		$this->fpdf->Cell(45,5,$tgl,'',0,'L',TRUE);
		$this->fpdf->Ln();
		$w = array(8,60,18,18,30,8,25,25);
		$this->fpdf->Cell($w[0],5,"No.",'BTLR',0,'C',TRUE);
		$this->fpdf->Cell($w[1],5,"Pembayaran",'BTLR',0,'C',TRUE);
		$this->fpdf->Cell($w[2],5,"Program",'BTLR',0,'C',TRUE);
		$this->fpdf->Cell($w[3],5,"Tingkat",'BTLR',0,'C',TRUE);
		$this->fpdf->Cell($w[4],5,"Bulan",'BTLR',0,'C',TRUE);
		$this->fpdf->Cell($w[5],5,"P.",'BTLR',0,'C',TRUE);
		$this->fpdf->Cell($w[6],5,"@",'BTLR',0,'C',TRUE);
		$this->fpdf->Cell($w[7],5,"Jumlah",'BTLR',0,'C',TRUE);
		$this->fpdf->Ln();		
		$i=1;
		foreach($uraian->result_array() as $row){
			$this->fpdf->SetFont('Arial','I',10);
			$this->fpdf->Cell($w[0],6,$i,'BTLR',0,'C',TRUE);			
			$this->fpdf->Cell($w[1],6,$row['nm_biaya'],'BTLR',0,'L',TRUE);
			$this->fpdf->Cell($w[2],6,$row['kd_program'],'BTLR',0,'L',TRUE);
			$this->fpdf->Cell($w[3],6,$row['kd_tingkat'],'BTLR',0,'L',TRUE);
			$mth = array('','Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec');                                                
            if($row['jlh_bln'] > 1){
                $split_bln = explode(',', $row['bulan']);
                $this->fpdf->Cell($w[4],6,$mth[$split_bln[0]].' - '.$mth[$split_bln[(($row['jlh_bln'])-1)]].' '.$row['tahun'],'BTLR',0,'L',TRUE);
            }else{
                if($row['bulan']!='')
                	$this->fpdf->Cell($w[4],6,$mth[$row['bulan']].' '.$row['tahun'],'BTLR',0,'L',TRUE);
                else
                	$this->fpdf->Cell($w[4],6,'','BTLR',0,'L',TRUE);
            }
            $this->fpdf->Cell($w[5],6,$row['jlh_bln'],'BTLR',0,'R',TRUE);
			$this->fpdf->Cell($w[6],6,number_format($row['biaya']),'BTLR',0,'R',TRUE);
			$this->fpdf->Cell($w[7],6,number_format($row['jumlah']),'BTLR',0,'R',TRUE);
			$i++;
			$this->fpdf->Ln();
		}

		$this->fpdf->SetFont('Arial','I',10);
			$this->fpdf->Cell($w[0],6,$i,'BTLR',0,'C',TRUE);			
			$this->fpdf->Cell($w[1],6,$row['nm_biaya'],'BTLR',0,'L',TRUE);
			$this->fpdf->Cell($w[2],6,$row['kd_program'],'BTLR',0,'L',TRUE);
			$this->fpdf->Cell($w[3],6,$row['kd_tingkat'],'BTLR',0,'L',TRUE);
			$mth = array('','Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec');                                                
            if($row['jlh_bln'] > 1){
                $split_bln = explode(',', $row['bulan']);
                $this->fpdf->Cell($w[4],6,$mth[$split_bln[0]].' - '.$mth[$split_bln[(($row['jlh_bln'])-1)]].' '.$row['tahun'],'BTLR',0,'L',TRUE);
            }else{
                if($row['bulan']!='')
                	$this->fpdf->Cell($w[4],6,$mth[$row['bulan']].' '.$row['tahun'],'BTLR',0,'L',TRUE);
                else
                	$this->fpdf->Cell($w[4],6,'','BTLR',0,'L',TRUE);
            }
            $this->fpdf->Cell($w[5],6,$row['jlh_bln'],'BTLR',0,'R',TRUE);
			$this->fpdf->Cell($w[6],6,number_format($row['biaya']),'BTLR',0,'R',TRUE);
			$this->fpdf->Cell($w[7],6,number_format($row['jumlah']),'BTLR',0,'R',TRUE);
			$i++;
			$this->fpdf->Ln();

		$this->fpdf->SetFont('Arial','I',10);
			$this->fpdf->Cell($w[0],6,$i,'BTLR',0,'C',TRUE);			
			$this->fpdf->Cell($w[1],6,$row['nm_biaya'],'BTLR',0,'L',TRUE);
			$this->fpdf->Cell($w[2],6,$row['kd_program'],'BTLR',0,'L',TRUE);
			$this->fpdf->Cell($w[3],6,$row['kd_tingkat'],'BTLR',0,'L',TRUE);
			$mth = array('','Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec');                                                
            if($row['jlh_bln'] > 1){
                $split_bln = explode(',', $row['bulan']);
                $this->fpdf->Cell($w[4],6,$mth[$split_bln[0]].' - '.$mth[$split_bln[(($row['jlh_bln'])-1)]].' '.$row['tahun'],'BTLR',0,'L',TRUE);
            }else{
                if($row['bulan']!='')
                	$this->fpdf->Cell($w[4],6,$mth[$row['bulan']].' '.$row['tahun'],'BTLR',0,'L',TRUE);
                else
                	$this->fpdf->Cell($w[4],6,'','BTLR',0,'L',TRUE);
            }
            $this->fpdf->Cell($w[5],6,$row['jlh_bln'],'BTLR',0,'R',TRUE);
			$this->fpdf->Cell($w[6],6,number_format($row['biaya']),'BTLR',0,'R',TRUE);
			$this->fpdf->Cell($w[7],6,number_format($row['jumlah']),'BTLR',0,'R',TRUE);
			$i++;
			$this->fpdf->Ln();

		$this->fpdf->SetFont('Arial','I',10);
			$this->fpdf->Cell($w[0],6,$i,'BTLR',0,'C',TRUE);			
			$this->fpdf->Cell($w[1],6,$row['nm_biaya'],'BTLR',0,'L',TRUE);
			$this->fpdf->Cell($w[2],6,$row['kd_program'],'BTLR',0,'L',TRUE);
			$this->fpdf->Cell($w[3],6,$row['kd_tingkat'],'BTLR',0,'L',TRUE);
			$mth = array('','Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec');                                                
            if($row['jlh_bln'] > 1){
                $split_bln = explode(',', $row['bulan']);
                $this->fpdf->Cell($w[4],6,$mth[$split_bln[0]].' - '.$mth[$split_bln[(($row['jlh_bln'])-1)]].' '.$row['tahun'],'BTLR',0,'L',TRUE);
            }else{
                if($row['bulan']!='')
                	$this->fpdf->Cell($w[4],6,$mth[$row['bulan']].' '.$row['tahun'],'BTLR',0,'L',TRUE);
                else
                	$this->fpdf->Cell($w[4],6,'','BTLR',0,'L',TRUE);
            }
            $this->fpdf->Cell($w[5],6,$row['jlh_bln'],'BTLR',0,'R',TRUE);
			$this->fpdf->Cell($w[6],6,number_format($row['biaya']),'BTLR',0,'R',TRUE);
			$this->fpdf->Cell($w[7],6,number_format($row['jumlah']),'BTLR',0,'R',TRUE);
			$i++;
			$this->fpdf->Ln();		

		$this->fpdf->SetFont('Arial','I',10);
			$this->fpdf->Cell($w[0],6,$i,'BTLR',0,'C',TRUE);			
			$this->fpdf->Cell($w[1],6,$row['nm_biaya'],'BTLR',0,'L',TRUE);
			$this->fpdf->Cell($w[2],6,$row['kd_program'],'BTLR',0,'L',TRUE);
			$this->fpdf->Cell($w[3],6,$row['kd_tingkat'],'BTLR',0,'L',TRUE);
			$mth = array('','Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec');                                                
            if($row['jlh_bln'] > 1){
                $split_bln = explode(',', $row['bulan']);
                $this->fpdf->Cell($w[4],6,$mth[$split_bln[0]].' - '.$mth[$split_bln[(($row['jlh_bln'])-1)]].' '.$row['tahun'],'BTLR',0,'L',TRUE);
            }else{
                if($row['bulan']!='')
                	$this->fpdf->Cell($w[4],6,$mth[$row['bulan']].' '.$row['tahun'],'BTLR',0,'L',TRUE);
                else
                	$this->fpdf->Cell($w[4],6,'','BTLR',0,'L',TRUE);
            }
            $this->fpdf->Cell($w[5],6,$row['jlh_bln'],'BTLR',0,'R',TRUE);
			$this->fpdf->Cell($w[6],6,number_format($row['biaya']),'BTLR',0,'R',TRUE);
			$this->fpdf->Cell($w[7],6,number_format($row['jumlah']),'BTLR',0,'R',TRUE);
			$i++;
			$this->fpdf->Ln();

		$this->fpdf->SetLineWidth(.4);		
		$this->fpdf->cell($w[0]+$w[1]+$w[2]+$w[3]+$w[4],6,'','T',0,'R',TRUE);
		$this->fpdf->cell($w[5]+$w[6],6,'Subtotal','BTLR',0,'R',TRUE);	
		$this->fpdf->cell($w[7],6,number_format($kwitansi['subtotal']),'BTLR',0,'R',TRUE);
		$this->fpdf->Ln();
		$this->fpdf->cell($w[0]+$w[1]+$w[2]+$w[3]+$w[4],6,'','',0,'R',TRUE);
		$this->fpdf->cell($w[5]+$w[6],6,'Diskon murid','BTLR',0,'R',TRUE);	
		$this->fpdf->cell($w[7],6,'( '.number_format($kwitansi['nilai_diskon']).')','BTLR',0,'R',TRUE);
		$this->fpdf->Ln();
		$this->fpdf->cell($w[0]+$w[1]+$w[2]+$w[3]+$w[4],6,'','',0,'R',TRUE);
		$this->fpdf->cell($w[5]+$w[6],6,'Denda','BTLR',0,'R',TRUE);	
		$this->fpdf->cell($w[7],6,number_format($kwitansi['nilai_diskon']),'BTLR',0,'R',TRUE);
		$this->fpdf->Ln();
		$this->fpdf->SetLineWidth(.4);
		$this->fpdf->cell($w[0]+$w[1]+$w[2]+$w[3]+$w[4],6,'','',0,'R',TRUE);
		$this->fpdf->cell($w[5]+$w[6],6,'Total','BTLR',0,'R',TRUE);	
		$this->fpdf->cell($w[7],6,number_format($kwitansi['total']),'BTLR',0,'R',TRUE);
		$this->fpdf->Ln();
		$this->fpdf->SetLineWidth(.3);
		$this->fpdf->SetFont('Arial','B',11);
		$this->fpdf->Cell(22,5,"Terbilang :",'T',0,'L',TRUE);
		$this->fpdf->SetFont('Arial','I',10);
		$this->fpdf->Cell(168,5,$this->terbilang($kwitansi['total']),'T',0,'L',TRUE);		
		$this->fpdf->Ln();
		$this->fpdf->SetFont('Arial','B',11);
		$this->fpdf->Cell(25,5,"Keterangan :",'',0,'L',TRUE);		
		$this->fpdf->SetFont('Arial','',10);
		$this->fpdf->cell($w[0]+$w[1]+$w[2]+$w[3]+$w[4]+$w[5]+$w[6] - 25,6,"tes tes tes tes",'',0,'L',TRUE);		
		$this->fpdf->SetFont('Arial','B',11);
		$this->fpdf->cell($w[7],6,"Kasir",'',0,'C',TRUE);
	}
}