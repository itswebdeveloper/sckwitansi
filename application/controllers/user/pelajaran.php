<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Pelajaran extends CI_Controller {
	function __construct(){
		parent::__construct();		
		$this->load->helper(array('form','url'));
		$this->load->library('session');
		$this->load->model('user_model');
		$this->check_isvalidated();
	}

	private function check_isvalidated(){
		if(! $this->session->userdata('validated'))
			redirect('login');
		else
			if($this->session->userdata('level') === 'admin')
					redirect('master/main');						
	}
	
	public function logout(){
		$this->session->sess_destroy();
		redirect('login');
	}		
	
	public function index()
	{
		$data = $this->user_model->general();
		
		$data['biaya'] 		= $this->user_model->get_biaya();
		$data['murid'] 		= $this->user_model->get_murid();
		$data['pelajaran'] 	= $this->user_model->get_pelajaran();
		$data['program'] 	= $this->user_model->get_program();
		$data['jenis'] 		= $this->user_model->get_jenis();
		$data['diskon'] 	= $this->user_model->get_diskon();
		$data['denda'] 		= $this->user_model->get_denda();
		
		$this->load->view('user/pelajaran_form',$data);
	}

	function suggestion(){
		//search term from jQuery
		$term = $this->input->post('term',TRUE);
		
		//if(strlen($term) < 2) break;
		
		$rows = $this->user_model->get_suggest_murid(array('keyword'=>$term));
		
		$json_array = array();
		foreach ($rows as $row)
			array_push($json_array, $row->nim);
		
		//return data
		echo json_encode($json_array);
	}

	function current_year(){
		$data = array('cyear' => date('Y'));
		$this->output
		    ->set_content_type('application/json')
		    ->set_output(json_encode($data));
	}

	public function view($page=0){		
		$data = $this->user_model->general();
		$this->load->model('master_model');
		
		$data['kwitansi'] 	= $this->user_model->get_mkwitansi_pelajaran($page,20);
		$count 				= $this->user_model->get_count_mkwitansi_pelajaran();
		$data['pagination'] = $this->pagination($count,20,5);
		
		$this->load->view('user/pelajaran_view',$data);
	}
	
	//pagination settings
	function pagination($row_total,$row_per_page,$link_count){
		$this->load->library(array('form_validation','pagination'));
		$config['base_url'] 		= base_url().'index.php/user/pelajaran/view';
		$config['uri_segment'] 		= 4;
		$config['total_rows'] 		= $row_total;
		$config['per_page'] 		= $row_per_page;
		$config['num_links']	 	= $link_count;
		$config['use_page_numbers'] = FALSE;
		$config['full_tag_open'] 	= '<ul class="pagination">';
		$config['full_tag_close'] 	= '</ul>';
		$config['first_tag_open'] 	= '<li>';
		$config['first_tag_close'] 	= '</li>';
		$config['last_tag_open'] 	= '<li>';
		$config['last_tag_close'] 	= '</li>';
		$config['next_link'] 		= 'Older &rarr;';
		$config['next_tag_open'] 	= '<li class="next">';
		$config['next_tag_close'] 	= '</li>';
		$config['prev_link'] 		= '&larr; Newer';
		$config['prev_tag_open'] 	= '<li class="previous">';
		$config['prev_tag_close'] 	= '</li>';
		$config['cur_tag_open'] 	= '<li class="active"><a>';
		$config['cur_tag_close'] 	= '<span class="sr-only">(current)</span></a></li>';
		$config['num_tag_open'] 	= '<li>';
		$config['num_tag_close'] 	= '</li>';
		$this->pagination->initialize($config);
		$data = $this->pagination->create_links();
		return $data;
	}
	
	public function batal(){
		$no_kwitansi = $this->input->get('no');
		$this->user_model->change_kwitansi_status('REQBATAL', 'pelajaran', $no_kwitansi);
		redirect(base_url().'index.php/user/pelajaran/view');
	}
	
	public function add(){
		$data = $this->user_model->general();
		$this->load->library('form_validation');
		$data['murid'] = $this->user_model->get_murid();			
				
		$this->form_validation->set_rules('nama','NAMA','required');						
		
		if($this->form_validation->run() == FALSE){
			$data['biaya'] 		= $this->user_model->get_biaya();
			$data['murid'] 		= $this->user_model->get_murid();
			$data['pelajaran'] 	= $this->user_model->get_pelajaran();
			$data['program'] 	= $this->user_model->get_program();
			$data['jenis'] 		= $this->user_model->get_jenis();
			$data['diskon'] 	= $this->user_model->get_diskon();
			$data['denda'] 		= $this->user_model->get_denda();	
			$this->load->view('user/pelajaran_form',$data);		
		}else{
			if( count($this->input->post('kd_biaya')) <= 0){
				$this->load->view('user/pelajaran_form',$data);		
			}else{
				$data['nim'] 			= $this->input->post('nim');
				$data['nama'] 			= $this->input->post('nama');
				$data['tanggal'] 		= date("d-m-Y");
				$data['uraian'] 		= array();
				$data['diskon'] 		= array();
				$data['denda'] 			= array();
				$data['stotal'] 		= 0;
				$data['nilai_diskon'] 	= 0;
				$data['nilai_denda'] 	= 0;
				$data['total'] 			= 0;
				$data['lk_status'] 		= $this->input->post('lkstatus');
				$data['keterangan'] 	= $this->input->post('keterangan');
				
				if($data['lk_status'] == "lebih"){
					$data['kurang_bayar'] 	= 0;
					$data['lebih_bayar'] 	= $this->input->post('lkbayar');
				}else{
					$data['kurang_bayar'] 	= $this->input->post('lkbayar');
					$data['lebih_bayar'] 	= 0;
				}
			
				//List Pembayaran
				$kd_biaya 	= $this->input->post('kd_biaya');
				$bulan 		= $this->input->post('bulan');				
				$tahun 		= $this->input->post('tahun');
				$stotal 	= $this->input->post('stotal');
				$periode 	= $this->input->post('periode');

				//List Diskon
				$kd_biaya_diskon 	= $this->input->post('kd_biaya_diskon');
				$kd_diskon 			= $this->input->post('kd_diskon');
				$ket_diskon 		= $this->input->post('ket_diskon');
				$p_diskon 			= $this->input->post('p_diskon');
				$kd_pembayaran 		= $this->input->post('kd_pembayaran');
								
				//List Denda
				$kd_denda 		= $this->input->post('kd_denda');
				$bulan_denda 	= $this->input->post('bulan_denda');
				$periode_denda 	= $this->input->post('periode_denda');
				$status 		= $this->input->post('status');
				$ket_denda 		= $this->input->post('ket_denda');
				$n_denda 		= $this->input->post('n_denda');
				
				for($i=0;$i< count($kd_biaya)&& $kd_biaya != NULL; $i++){					
					$biaya = $this->user_model->get_biaya($kd_biaya[$i]);

					$persen_diskon=0;										/*mengambil persen diskon dan nilai diskon per biaya*/
					for($j=0; $j < count($kd_biaya_diskon) && $kd_biaya_diskon != NULL; $j++){
						if($kd_biaya[$i] == $kd_biaya_diskon[$j]){
							$persen_diskon = ($persen_diskon + $p_diskon[$j]) - (($persen_diskon/10)*($p_diskon[$j]/10));
						}else{
							continue;
						}
						$u_diskon = array(
							'kd_biaya'		=>$kd_biaya_diskon[$j],
							'kd_diskon'		=>$kd_diskon[$j],
							'persen_diskon'	=>$p_diskon[$j],
							'ket_diskon'	=>$ket_diskon[$j],
							'kd_pembayaran' => $kd_pembayaran[$j]
						);

						array_push($data['diskon'],$u_diskon);
					}

					$ybiaya = $biaya['biaya'];
					if($biaya['jenis_biaya'] == "USS" || $biaya['jenis_biaya'] == "USK") //cek tersedia pilihan tahun
						if($tahun[$i] < round(date("Y")))
							$ybiaya = $biaya['biaya_lama'];

					$subtotal = round($ybiaya*$periode[$i]);
					$n_diskon = $subtotal * ($persen_diskon/100);

					if($biaya['kd_pelajaran'] == NULL)
						$kd_subprogram = '';
					else{
						$split = explode('-', $biaya['kd_pelajaran']);
						$kd_subprogram = $split[1];
					}
					$u_biaya = array(
						'kd_biaya'		=>$kd_biaya[$i],											
						'kd_tingkat'	=>$biaya['kd_tingkat'],
						'kd_program'	=>$biaya['kd_program'],
						'kd_subprogram' => $kd_subprogram,						
						'nm_biaya'		=>$biaya['nm_biaya'],
						'biaya'			=>$ybiaya,
						'bulan'			=>$bulan[$i],
						'tahun'			=>$tahun[$i],
						'periode' 		=> $periode[$i],
						'n_diskon' 		=> $n_diskon
					);
					
					$data['nilai_diskon'] += $n_diskon;
					$data['stotal'] += $subtotal;
					array_push($data['uraian'],$u_biaya);
				}								
				for($i=0;$i<count($kd_denda)&& $kd_denda != NULL;$i++){					
					$total =0;
					if($status[$i] == "semester"){
						$total = $n_denda[$i] * $periode_denda[$i];
					}else{
						for($j=1; $j<= $periode_denda[$i]; $j++){
							$total+=(($n_denda[$i]*$periode_denda[$i])-(($j-1)*$n_denda[$i]));
						}
					}

					$data['nilai_denda']+=$total;

					$u_denda = array(
						'kd_denda'	=>$kd_denda[$i],
						'bulan'		=>$bulan_denda[$i],
						'periode'	=>$periode_denda[$i],
						'status'	=>$status[$i],
						'harga'		=>$n_denda[$i],
						'keterangan'=>$ket_denda[$i],
						'total'		=>$total
					);

					array_push($data['denda'], $u_denda);
				}
							
				$data['total'] = ($data['stotal'] - $data['nilai_diskon']) + $data['nilai_denda'] - $data['lebih_bayar'] + $data['kurang_bayar'];
				
				$data['terbilang'] = $this->terbilang(round($data['total']));

				$buydata = array(
					'tanggal'=>$data['tanggal'],
					'nim'=>$data['nim'],
					'nama'=>$data['nama'],
					'subtotal'=>$data['stotal'],
					'nilai_diskon'=>$data['nilai_diskon'],
					'nilai_denda'=>$data['nilai_denda'],
					'kurang_bayar' => $data['kurang_bayar'],
					'lebih_bayar' => $data['lebih_bayar'],
					'total'=>$data['total'],
					'keterangan'=>$data['keterangan'],
					'uraian'=>$data['uraian'],
					'diskon'=>$data['diskon'],
					'denda'=>$data['denda']		
				);
				$this->session->set_flashdata('buy',$buydata);
				
				$this->load->view('user/pelajaran_check',$data);
			}
		}
	}	
	
	public function save(){
		$pembelian = $this->session->flashdata('buy');
		
		$kwitansi = $this->user_model->set_kwitansi_pelajaran($pembelian);
		
		$this->session->set_flashdata('message','Kwitansi telah berhasil disimpan. 
		<a target="_blank" href="'.base_url().'index.php/user/pdfprint/pelajaran/?nomor='.$kwitansi.'">Klik Di sini untuk menampilkan</a>');
		redirect(base_url().'index.php/user/pelajaran');
	}
	
	function terbilang($satuan){
		$huruf = array("","satu","dua","tiga","empat","lima","enam","tujuh","delapan","sembilan","sepuluh","sebelas");
		if($satuan < 12)
			return " ".$huruf[$satuan];
		elseif($satuan<20)
			return $this->terbilang($satuan - 10)." belas";
		elseif($satuan<100)
			return $this->terbilang($satuan/10)." puluh".$this->terbilang($satuan% 10);
		elseif($satuan<200)
			return " seratus".$this->terbilang($satuan-100);
		elseif($satuan<1000)
			return $this->terbilang($satuan /100)." ratus".$this->terbilang($satuan %100);
		elseif($satuan<2000)
			return " seribu".$this->terbilang($satuan-1000);
		elseif($satuan<1000000)
			return $this->terbilang($satuan/1000)." ribu".$this->terbilang($satuan%1000);
		elseif($satuan < 1000000000)
			return $this->terbilang($satuan/1000000)." juta".$this->terbilang($satuan % 1000000);
		elseif($satuan >=1000000000)
			return "Angka terlalu Besar";
	}	
}