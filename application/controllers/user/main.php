<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class main extends CI_Controller {
	function __construct(){
		parent::__construct();		
		$this->load->helper(array('form','url'));
		$this->load->library('session');
		$this->load->model('user_model');
		$this->check_isvalidated();
	}

	private function check_isvalidated(){
		if(! $this->session->userdata('validated'))
			redirect('login');
		else
			if($this->session->userdata('level') === 'admin')
					redirect('master/main');						
	}
	
	public function logout(){
		$this->session->sess_destroy();
		redirect('login');
	}		
	
	public function index()
	{
		$data = $this->user_model->general();
		
		$this->load->view('user/index',$data);
	}	
}