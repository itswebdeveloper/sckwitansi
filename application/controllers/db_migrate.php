<?php if( ! defined('BASEPATH')) exit('No direct script access allowed');

class db_migrate extends CI_Controller{
	
	function __construct(){
		parent:: __construct();		
		$this->load->helper('url');
		$this->load->library('session');
		$this->load->dbforge();
		$this->load->model('migrate_model');
	}
	
	public function index()
	{			
		
	}

	public function create_tb_murid_baru(){
		$field = array(
			'id'		=> array(
							'type' => 'int',
							'constraint' => 5,
							'unsigned' => TRUE,
							'auto_increment' => TRUE
						),
			'nim'		=> array(
							'type' => 'varchar',
							'constraint' => 10						
						),
			'nim_baru'	=> array(
							'type' => 'varchar',
							'constraint' => 10,
							'null' => TRUE,
							'default' => NULL
						),
			'nm_murid'	=> array(
							'type' => 'varchar',
							'constraint' => 50
						),
			'ket'		=> array(
							'type' => 'varchar',
							'constraint' => 100
						),
			'status'	=> array(
							'type' => 'varchar',
							'constraint' => 20
						)
		);
		$this->dbforge->add_field($field);
		$this->dbforge->add_key('id',TRUE);
		$this->dbforge->create_table('murid_baru', TRUE);

		return "<p>Table Murid Baru Generated Succesful</p>";
	}	

	public function upgrade_database(){
		$message = '';
		//add murid baru table
		$message.= $this->create_tb_murid_baru();

		//restructure table kwitansi barang
		$field = array(
				'status' => array(
							'type' => 'varchar',
							'constraint' => 10
						)
				);
		if(($this->dbforge->add_column('m_kwitansibrg', $field)) == TRUE){
			$message.="<p>Kwitansi Barang Column added Succesful</p>";
			$field_modify = array(
				'nama'  => array(
							'name' => 'nama',
							'type' => 'varchar',
							'constraint' => 35							 
				),
				'keterangan'	=> array(
							'name' => 'keterangan',
							'type' => 'varchar',
							'constraint' => 255
				)
			);	
		}else{
			$message.="<p>Kwitansi Barang Column adding Failed. Trying to modify column ...</p>";
			$field_modify = array(
				'nama'  => array(
							'name' => 'nama',
							'type' => 'varchar',
							'constraint' => 35							 
				),
				'keterangan'	=> array(
							'name' => 'keterangan',
							'type' => 'varchar',
							'constraint' => 255
				),
				'status' => array(
							'name' => 'status',
							'type' => 'varchar',
							'constraint' => 10
				)
			);
		}
		($this->dbforge->modify_column('m_kwitansibrg', $field_modify)) ? $message.="<p>Kwitansi Barang Column modified Succesful</p>":$message.="<p>Kwitansi Barang Column modifying Failed. Halt trying..</p>";
		$message.="<p>Continuing to Kwitansi Pelajaran ...</p>";

		// restructure table kwitansi pelajaran
		$fieldnama = array(
				'nama'  => array(
							'type' => 'varchar',
							'constraint' => 35							 
				)
		);
		$fieldstatus = array(				
				'status' => array(
							'type' => 'varchar',
							'constraint' => 10
				)
		);
		if($this->dbforge->add_column('m_kwitansipljr', $fieldnama, 'nim') == TRUE && ($this->dbforge->add_column('m_kwitansipljr', $fieldstatus) == TRUE)){
			$message.="<p>Kwitansi Pelajaran Column added Succesful</p>";
			$field_modify = array(				
					'keterangan'	=> array(
								'name' => 'keterangan',
								'type' => 'varchar',
								'constraint' => 255
					)
			);
		}else{
			$message.="<p>Kwitansi Pelajaran Column adding Failed. Trying to modify Column ...</p>";
			$field_modify = array(				
					'keterangan'	=> array(
								'name' => 'keterangan',
								'type' => 'varchar',
								'constraint' => 255
					),
					'nama'  => array(
							'name' => 'nama',
							'type' => 'varchar',
							'constraint' => 35							 
					),
					'status' => array(
								'status' => 'status',
								'type' => 'varchar',
								'constraint' => 10
					)
			);
		}

		($this->dbforge->modify_column('m_kwitansipljr', $field_modify)) ? $message.="<p>Kwitansi Pelajaran Column modified Succesful</p>":$message.="<p>Kwitansi Pelajaran Column modifying Failed. Halt trying..</p>";		

		$message.="<p>Continuing to repair Kwitansi Pelajaran records</p>";
		($this->migrate_model->sync_m_kwitansipljr()) ? $message.="<p>Records repaired Succesful</p>" : $message.="<p>Records Failed to repair</p>";
		
		//restructure table biaya
		$field = array(
				'biaya_lama'  => array(
							'type' => 'int',							
							'constraint' => 11,
							'null' => TRUE,						 
				)
		);
		if($this->dbforge->add_column('biaya', $field, 'biaya') == TRUE){
			$message.="<p>Biaya lama Column added Succesful</p>";
			($this->migrate_model->sync_biaya_lama()) ? $message.="<p>Records for biaya lama was copied</p>" : $message.='<p>Records for biaya lama failed</p>';
		}else{
			$message.="<p>Biaya lama Column adding Failed.</p>";
		}
		$fieldmodifydate = array(
				'modified_date'  => array(
							'type' => 'date',							
				)
		);
		if($this->dbforge->add_column('biaya', $fieldmodifydate) == TRUE){
			$message.="<p>Modified_date column added</p>";
			($this->migrate_model->sync_biaya_modifieddate()) ? $message.="<p>Records for biaya modified date fixed</p>" : $message.='<p>Records for biaya modified date failed</p>';
		}else{
			$message.="<p>Modified_date column failed.</p>";
		}		

		$message.="<p>Migrating Stop ...</p>";

		$this->session->set_flashdata('Upgrade_message',$message);
		redirect('master/main');
	}
}