<?php if( ! defined('BASEPATH')) exit('No direct script access allowed');

class login extends CI_Controller{
	
	function __construct(){
		parent:: __construct();
		$this->load->database();
		$this->load->helper('url');
		$this->load->library('session');
	}
	
	public function index()
	{		
		$this->load->library('form_validation');
		$this->form_validation->set_rules('name', 'Name', 'trim|required|xss_clean');
		$this->form_validation->set_rules('pass', 'Password', 'trim|required');
		$this->load->model('general_model');
		$data = $this->general_model->general();
		
		if ($this->form_validation->run() == FALSE)
		{				
			$this->load->view('login',$data);
		}
		else
		{
			//load model
			$this->load->model('login_model');
			//validate admin can login
			$result = $this->login_model->validate();
			if($result == true){
				if($this->session->userdata('level') === 'admin')
					redirect('master/main');
				else
					redirect('user/main');
					
			}elseif($result == false){
				$data['msg'] = "Name atau Password tidak cocok, silahkan ulangi kembali";
				$this->load->view('login',$data);
			}
		}
		
	}
}